//
//  ConceirgeDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 17/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox


class ConceirgeDetailsVC: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var LBL_contactDetails: UILabel!

    public  var outletId  : Int?
    var navTitle : String?
    private var imageList : [LightboxImage]? = []
    var ownerContactNo = ""
    var conceirgeDetails : ConceirgeServiceDetails?
    private var menuCardURLArray : [LightboxImage] = []
    

    let cellIdentifier = "MenuCollectionViewCell"
   // public  var outletId  : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navTitle
        backImageView.layer.cornerRadius = 4.0
        self.getConceirgeDetails()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = navTitle
    }
    
    @IBAction func onClickViewButton(_ sender: Any) {
        showMenuCard()
    }
}

extension ConceirgeDetailsVC {
    
    func getConceirgeDetails(){
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["ConceirgeServiceId" : self.outletId ?? ""],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getConceirgeServiceDetails, objectType: ConceirgeServiceDetailsData.self, isHud: true, controller: self, success: { [weak self] csData in
            print("***********")
            print(csData)
            self?.conceirgeDetails = csData.Data
            if (self?.conceirgeDetails) != nil {
                self?.setupData()
            }
        })
    }
    
    func setupData(){
        if let img = self.conceirgeDetails?.BannerImage {
            self.backImageView.sd_setImage(with: URL.init(string: img ), placeholderImage:  #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.continueInBackground, completed: nil)
        }
        self.nameLabel.text = self.conceirgeDetails?.Title
        self.descriptionLabel.text = self.conceirgeDetails?.Description
        self.timeLabel.text = self.conceirgeDetails?.Time
        self.LBL_contactDetails.text = self.conceirgeDetails?.ContactDetails
        let tapGestureForCall = UITapGestureRecognizer(target: self, action:#selector(self.handleTap))
        self.LBL_contactDetails.addGestureRecognizer(tapGestureForCall)

        self.setData()
        self.collectionView.reloadData()
    }
    
    private func setData(){
        self.imageList = self.conceirgeDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0)!) }) ?? []
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let contactDetails = self.conceirgeDetails?.ContactDetails
        let contactno1 = String((contactDetails?.suffix(10))!).trimmingCharacters(in: .whitespaces)
        if let url = URL(string: "tel://\(contactno1)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

}


extension ConceirgeDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.imageList?.count else {return 0}
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MenuCollectionViewCell
        
        if let image = self.conceirgeDetails?.ImageUrls?[indexPath.row] {
            cell.imgView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList ?? [], startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }

}

extension ConceirgeDetailsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}

extension ConceirgeDetailsVC {
    func showMenuCard() {
        if let menuCardURL = conceirgeDetails?.BookingProcessDocument {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    private func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Business Process Document"
        navigationController?.pushViewController(controller, animated: true)
    }
}


extension ConceirgeDetailsVC : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
}
