//
//  RUOffersVC.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUOffersVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "OffersCategoryCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    
    // MARK:- Objects
    private var offersData : OffersData?
    private var catrgoryArray : [String]?
    private var slides : [HomeScreenBannerView] = [];
    private var bannerTimer : Timer?
    private var bannerListing : [BannerListing]?
    private var offerList : [CategoryList]?
    //MARK:- Lifecycle Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupScreen()
        self.addTapToScroll()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
    @objc func scrollViewTapped(){
        let currentBanner = self.pgControl.currentPage
        let bannerID = self.offersData?.BannerList?[currentBanner].Id
        let vc = Storyboard.offerDetailVC
        vc.offerID = bannerID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension RUOffersVC {
    //MARK:- Webservice Call
    private func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getOfferList, objectType: OffersData.self, isHud: true, controller: self, success: {[weak self]
            response in
            self?.offersData = response
            self?.offerList = response.DataList
            self?.tblView.reloadData()
            self?.setupBannerSlides()
        })
    }
    //MARK:- Class Methods
    private func setupScreen(){
        self.setupNavigation()
        self.callWebService()
        
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    private func setupNavigation(){
        self.addLeftMenuButton()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Offers"
    }
    private func setupBannerSlides(){
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
//        self.pgControl.isHidden = true
    }
    private func addTapToScroll(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        self.bannerScrollView.addGestureRecognizer(tap)
    }
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        if let bannerImgArray = self.offersData?.BannerList?.map({ $0.BannerImage ?? ""}) {
            
            for imgStr in bannerImgArray {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                slide.bannerImgView.sd_setImage(with: URL.init(string: imgStr), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
                bannerVwArray.append(slide)
            }
            return bannerVwArray
        }
        return []
    }
    
    private func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
    private func loadOfferDetails(offerId : Int) {
        let offerDetailsVC = Storyboard.offerDetailVC
        offerDetailsVC.offerID = offerId
        self.navigationController?.pushViewController(offerDetailsVC, animated: true)
    }
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
}
extension RUOffersVC : UITableViewDataSource,UITableViewDelegate {
    // MARK: - TableView datsource and delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offersData?.DataList?.count ?? 0//catrgoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: OffersCategoryCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OffersCategoryCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OffersCategoryCell
        }
        let info = offerList![indexPath.row]
        cell.titleLbl.text = info.OfferTypeName?.capitalized ?? ""
        cell.dataSource = info.OfferList ?? []
        cell.offerSelected = {offerList in
            self.loadOfferDetails(offerId: offerList.OfferId ?? 0)
        }
        return cell
    }
    
}

extension RUOffersVC : UIScrollViewDelegate {
    // MARK:- ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }
}


