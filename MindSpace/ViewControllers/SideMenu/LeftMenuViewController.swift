//
//  LeftMenuViewController.swift
//  Sail Uday
//
//  Created by Webwerks on 07/08/17.
//  Copyright © 2017 webwerks1. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {

    let cellIdentifier = "menuCell"
    let user = AppDelegate.appDelegateShared.user?.UserType
   
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!

    var mainViewController: UIViewController!
    var selectedIndex: IndexPath? = [0,0]
    @IBOutlet weak var tableVw: UITableView!
    var tableContent = [
        ["Title":"Home",
         "StoryboardID" : "UserTabBarController",
         "Storyboard" : "Advance",
         "Image" : "home_SM"
        ],
        ["Title":"About Us",
         "StoryboardID" : "WebViewVC",
          "Storyboard" : "Advance",
         "Image" : "info_SM"
        ],
        ["Title":"Terms & Conditions",
         "StoryboardID" : "WebViewVC",
          "Storyboard" : "Advance",
         "Image" : "question_SM"
        ],
        ["Title":"Settings",
         "StoryboardID" : "SettingScreenVC",
          "Storyboard" : "Main",
         "Image" : "settings_SM"
        ],
        ["Title":"Logout",
//         "StoryboardID" : "",
         "Image" : "logout_SM"
        ],
    ]
    var tableContentVisitor = [
        ["Title":"Home",
         "StoryboardID" : "UserTabBarController",
         "Storyboard" : "Advance",
         "Image" : "home_SM"
        ],
        ["Title":"Settings",
         "StoryboardID" : "SettingScreenVC",
         "Storyboard" : "Main",
         "Image" : "settings_SM"
        ],
        ["Title":"Logout",
         //         "StoryboardID" : "",
            "Image" : "logout_SM"
        ],
        ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        if let currentUser = appDelegate.user {
            welcomeLabel.text = currentUser.FirstName! + " " + currentUser.LastName!
            if let imgUrl = URL(string: currentUser.ProfilePicture ?? ""){
                profileImageView?.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "profilePic_Placeholder"))
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension LeftMenuViewController : UITableViewDelegate, UITableViewDataSource{
    //MARK:- TableView Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user == "Visitor"{
            return tableContentVisitor.count
        } else {
            return tableContent.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: menuCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? menuCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? menuCell
        }
        if user == "Visitor"{
            cell.icon?.image = UIImage(named: tableContentVisitor[indexPath.row]["Image"]!)
            cell.title?.text = tableContentVisitor[indexPath.row]["Title"]
        }
        else {
            cell.icon?.image = UIImage(named: tableContent[indexPath.row]["Image"]!)
            cell.title?.text = tableContent[indexPath.row]["Title"]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let cell = tableView.cellForRow(at: indexPath)
        
        UIView.animate(withDuration: 0.3, animations: {
            cell?.contentView.backgroundColor = .lightGray
            cell?.contentView.alpha = 0.3
        }) { (finished) in
            if finished
            {
                UIView.animate(withDuration: 0.3, animations: {
                    cell?.contentView.alpha = 1.0
                    cell?.contentView.backgroundColor = .clear
                }, completion: { (finished) in
                    if finished
                    {
                        DispatchQueue.main.async {
                            // Call selection function here
                            self.sideMenuViewController?.hideMenuViewController()
                        }
                    }
                })
            }
        }
        
        let nav: UINavigationController = self.sideMenuViewController?.contentViewController as! UINavigationController;
        if(nav.viewControllers.count > 1){
            nav.popToRootViewController(animated: false)
        }
        if indexPath.row == 0 {
            selectedIndex = indexPath
            if let tabBarVC = nav.viewControllers.first as? UITabBarController{
                tabBarVC.selectedIndex = 0
            }
            return
        }
        self.sideMenuViewController?.hideMenuViewController()
        let user = AppDelegate.appDelegateShared.user?.UserType//UserDefaults.standard.string(forKey: "user")
        if user == "User"{
            //User side menu click
            if let storyboardID = self.tableContent[indexPath.row]["StoryboardID"], let storyboard = self.tableContent[indexPath.row]["Storyboard"]{
                var storyB = Constants.StoryBoard.MAIN
                if storyboard == "Advance"{
                    storyB =  Constants.StoryBoard.ADVANCE
                }
                let selectedVC = storyB.instantiateViewController(withIdentifier: storyboardID)
                selectedVC.navigationItem.title = self.tableContent[indexPath.row]["Title"]
                let webViewVC = selectedVC as? WebViewVC
                switch indexPath.row {
                case 1:
                    webViewVC?.htmlString = "About Us"
                case 2:
                    webViewVC?.htmlString = "Terms and Conditions"
                default:
                    break
                }
                
                nav.pushViewController(selectedVC, animated: false)
                //            nav.viewControllers = [selectedVC!]
            }else{
                logoutMenuSelected()
            }
        } else {
            //Visitor side menu click
            if let storyboardID = self.tableContentVisitor[indexPath.row]["StoryboardID"], let storyboard = self.tableContentVisitor[indexPath.row]["Storyboard"]{
                var storyB = Constants.StoryBoard.MAIN
                if storyboard == "Advance"{
                    storyB =  Constants.StoryBoard.ADVANCE
                }
                let selectedVC = storyB.instantiateViewController(withIdentifier: storyboardID)
                selectedVC.navigationItem.title = self.tableContentVisitor[indexPath.row]["Title"]
//                let webViewVC = selectedVC as? WebViewVC
//                switch indexPath.row {
//                case 1:
//                    webViewVC?.htmlString = "About Us"
//                case 2:
//                    webViewVC?.htmlString = "Terms and Conditions"
//                default:
//                    break
//                }
                
                nav.pushViewController(selectedVC, animated: false)
                //            nav.viewControllers = [selectedVC!]
            }else{
                logoutMenuSelected()
            }
            
        }
        
        
        
        
        
       
        
        
        
        
        
        selectedIndex = indexPath

        //dropShadow(color: .black, offSet: (cell?.contentView.frame.size)!)
        
       /*if indexPath == selectedIndex
       {
        self.sideMenuViewController?.hideMenuViewController()
        return
        }
        var selectedCell = tableView.cellForRow(at: selectedIndex!)
        if selectedCell != nil {selectedCell?.backgroundColor = UIColor.clear; selectedCell?.textLabel?.textColor = UIColor.white}
        selectedIndex = indexPath
        selectedCell = tableView.cellForRow(at: selectedIndex!)
        selectedCell?.backgroundColor = UIColor.white
        selectedCell?.textLabel?.textColor = UIColor(patternImage: UIImage(named:"Nav_bar")!)
        tableView.deselectRow(at: indexPath, animated: true)
        let nav: UINavigationController = self.sideMenuViewController?.contentViewController as! UINavigationController;
        if(nav.viewControllers.count > 1){
            nav.popToRootViewController(animated: false)
        }
        self.sideMenuViewController?.hideMenuViewController()

        if let storyboardID = self.tableContent[indexPath.row]["StoryboardID"]{
            let selectedVC = self.storyboard?.instantiateViewController(withIdentifier: storyboardID)
            selectedVC?.navigationItem.title = self.tableContent[indexPath.row]["Title"]
            
            nav.pushViewController(selectedVC!, animated: false)
            //nav.viewControllers = [selectedVC!]
        }else{
            logoutMenuSelected()
            
        }*/
        
        //self.sideMenuViewController?.hideMenuViewController()

        
    }
    func logoutMenuSelected(){
        
            let alert = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                User.clearData()
                let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
                AppDelegate.appDelegateShared.user = nil
                let navController = UINavigationController.init(rootViewController: targetVC)
                //self.sideMenuViewController?.navigationController?.pushViewController(targetVC, animated: true)
                //self.sideMenuViewController?.navigationController?.viewControllers = [targetVC]
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navController
            }))
            self.present(alert, animated: true, completion: nil)
        
//
//        User.clearData()
//        let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
//        AppDelegate.appDelegateShared.user = nil
//        let navController = UINavigationController.init(rootViewController: targetVC)
//        //self.sideMenuViewController?.navigationController?.pushViewController(targetVC, animated: true)
//        //self.sideMenuViewController?.navigationController?.viewControllers = [targetVC]
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = navController
    }
//    {
//
//        let alertVC = UIAlertController(title: CommonStrings.appName, message: CommonStrings.logoutMsg, preferredStyle: .alert)
//        let ok = UIAlertAction(title: "Yes", style: .default) { (action) in
//            self.callLogoutWS()
//        }
//        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
//            self.selectedIndex = [0,0]
//            self.tableVw.reloadData()
//        }
//
//        alertVC.addAction(ok)
//        alertVC.addAction(cancel)
//        self.present(alertVC, animated: true, completion: nil)
//    }
    
    //MARK:- Webservice calls

}

class menuCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
}
