//
//  VisitorRequestCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
struct VisitorRequest {
    var firstName : String = ""
    var lastName : String = ""
    var emailId : String = ""
    var phoneNo :String = ""
    var isBaggage : Bool = false
    var isVehicle : Bool = false
    var isLaptop : Bool = false
    var vehicleNumber : String = ""
    var laptopModelNumber : String = ""
    func getDictionary() -> [String : Any] {
        return ["FirstName" : firstName,
                "LastName" : lastName,
                "Email" : emailId,
                "MobileNumber" : phoneNo,
                "IsVehicle" : isVehicle,
                "VehicleNumber" : vehicleNumber,
                "IsBaggage" : isBaggage,
                "IsLaptop" : isLaptop,
                "LaptopModelNumber" : laptopModelNumber
        ]
    }
}
protocol VisitorRequestCellDelegate {
    func visitorDataChanged(index:Int, visitorDetails:VisitorRequest)
    func hideVehicalDetail(cell : VisitorRequestCell, hide : Bool)
    func hideLaptopDetail(cell : VisitorRequestCell, hide : Bool)
}
class VisitorRequestCell: UITableViewCell {
    @IBOutlet weak var firstNameTxtField: RoundTextField!
    @IBOutlet weak var lastNameField: RoundTextField!
    @IBOutlet weak var emailIdTxtField: RoundTextField!
    @IBOutlet weak var mobileNumberTxtField: RoundTextField!
    @IBOutlet weak var baggageYesBtn: UIButton!
    @IBOutlet weak var baggageNoBtn: UIButton!
    @IBOutlet weak var vehicleYesBtn: UIButton!
    @IBOutlet weak var vehicleNoBtn: UIButton!
    @IBOutlet weak var laptopYesBtn: UIButton!
    @IBOutlet weak var laptopNoBtn: UIButton!
    
    @IBOutlet weak var vehicleDetailView: UIView!
    @IBOutlet weak var laptopDetailView: UIView!
    @IBOutlet weak var vehicleNoTxtFld: UITextField!
    @IBOutlet weak var laptopModelNoTxtFld: UITextField!

    
    
    var visitorRequest = VisitorRequest()
    var delegate: VisitorRequestCellDelegate?
    var cellIndex : Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func selectOption(_ sender: UIButton) {
        switch sender {
        case baggageNoBtn:
            baggageYesBtn.isSelected = false
            baggageNoBtn.isSelected = true
        case baggageYesBtn:
            baggageYesBtn.isSelected = true
            baggageNoBtn.isSelected = false
        case vehicleNoBtn:
            vehicleNoBtn.isSelected = true
            vehicleYesBtn.isSelected = false
            vehicleDetailView.isHidden = true
            vehicleNoTxtFld.text = ""
            visitorRequest.vehicleNumber = ""
        case vehicleYesBtn:
            vehicleNoBtn.isSelected = false
            vehicleYesBtn.isSelected = true
            vehicleDetailView.isHidden = false
        case laptopNoBtn:
            laptopYesBtn.isSelected = false
            laptopNoBtn.isSelected = true
            laptopDetailView.isHidden = true
            laptopModelNoTxtFld.text = ""
            visitorRequest.laptopModelNumber = ""
        case laptopYesBtn:
            laptopYesBtn.isSelected = true
            laptopNoBtn.isSelected = false
            laptopDetailView?.isHidden = false
        default:
            break
        }
        delegate?.hideVehicalDetail(cell: self, hide: vehicleNoBtn.isSelected)
        delegate?.hideLaptopDetail(cell: self, hide: laptopNoBtn.isSelected)
        visitorRequest.isBaggage = baggageYesBtn.isSelected
        visitorRequest.isVehicle = vehicleYesBtn.isSelected
        visitorRequest.isLaptop = laptopYesBtn.isSelected
        delegate?.visitorDataChanged(index: cellIndex, visitorDetails: visitorRequest)
    }
}
extension VisitorRequestCell : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case firstNameTxtField:
            visitorRequest.firstName = textField.text ?? ""
        case lastNameField:
            visitorRequest.lastName = textField.text ?? ""
        case emailIdTxtField:
            visitorRequest.emailId = textField.text ?? ""
        case mobileNumberTxtField:
            visitorRequest.phoneNo = textField.text ?? ""
        case vehicleNoTxtFld:
            visitorRequest.vehicleNumber = textField.text ?? ""
        case laptopModelNoTxtFld:
            visitorRequest.laptopModelNumber = textField.text ?? ""
        default:
            break
        }
        delegate?.visitorDataChanged(index: cellIndex, visitorDetails: visitorRequest)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
