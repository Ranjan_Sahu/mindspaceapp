//
//  AnnouncementDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 16/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox
class AnnouncementDetailsVC: UIViewController {
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    public  var annoucementId : Int?
    private var announcementDetails : AnnouncementDetails?
    private var imageURL : [String]?
    private var imageList: [LightboxImage] = []
    var navTitle : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAnnouncementDetails()
        self.navigationItem.title = navTitle
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = navTitle
    }
}

//MARK:- Custom Methods
extension AnnouncementDetailsVC {
    private func getAnnouncementDetails() {
        let parameter = ["entity" : ["AnnouncementId" : annoucementId ?? 0],
                         "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.getAnouncementDetails, objectType: AnnouncementDetailsData.self, isHud: true, controller: self, success: { [weak self] announcements in
            guard let list = announcements.Data else {
                let alert = UIAlertController.init(title: "Error", message: Messages.noData, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
                    self?.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okAction)
                self?.present(alert, animated: true, completion: nil)
                self?.view.isHidden = true
                return
            }
            
            
            
            self?.announcementDetails = list
            self?.navigationItem.title = self?.announcementDetails?.AnnouncementTitle
            self?.setData()
        })
    }
    private func setData(){
        self.nameLabel.text = self.announcementDetails?.AnnouncementTitle
        self.descriptionLabel.text = self.announcementDetails?.AnnouncementDesc
        
        if let strBannerImage = self.announcementDetails?.BannerImage{
            self.backImageView.sd_setImage(with: URL.init(string: strBannerImage ), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
        }
        self.imageURL = self.announcementDetails?.ImageUrls
        self.imageList = self.imageURL!.map( { LightboxImage(imageURL: URL.init(string: $0)!) })
        self.collectionView.reloadData()
    }
}
extension AnnouncementDetailsVC : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.imageURL?.count else { return 0 }
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MenuCollectionViewCell = collectionView.dequeueReusableCellFor(indexPath)
        cell.imgView.sd_setImage(with: URL.init(string: imageURL?[indexPath.row] ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        return cell
    }
}
extension AnnouncementDetailsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}
extension AnnouncementDetailsVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
}
extension AnnouncementDetailsVC : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
}
