//
//  RUBuyVC.swift
//  MindSpace
//
//  Created by webwerks on 10/16/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit



class RUBuyVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    // MARK:- Constants
    let cellIdentifier = "RentingListCell"
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    // MARK:- Objects
    //var productBuyList : [ProductBuyList]? = nil
    let refreshControl = UIRefreshControl()
    private var pageInfo = PageInfo()
    private var ProductListData : ProductBuyListData?
    
    @IBOutlet weak var noDataView: UIView!
    
    var productBuyList : [ProductBuyList] = [] {
        didSet {
            tblView.reloadData()
            if productBuyList.count == 0{
                tblView.tableFooterView = noDataView
            }else {
                tblView.tableFooterView = nil
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.addRefreshControlToTableView()
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        self.getProductsData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Products on sell"
    }
//    {
//    "deviceInfo":{    "DeviceTypeId":"1",
//    "UDID":"2ftygifhuidhgiofjhitj","Timestamp":"1245012102","UserId":"171"},
//    "pageInfo":{"PageIndex":"1","PageSize":"10"}
//    }
    // MARK:- Service Calls
    func getProductsData() {
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)", "PageSize" : "\(pageInfo.pageSize)"],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetProductList, objectType: ProductBuyListData.self, isHud: true, controller: self, success:{ [weak self] response in
    
            print(response)
            
            self?.ProductListData = response
            if self?.pageInfo.pageIndex == 1  {
                self?.productBuyList = response.DateList ?? []
            }else {
                if let productBuyList = response.DateList {
                    self?.productBuyList.append(contentsOf: productBuyList)
                }
            }
            
            self?.tblView.reloadData()
        })
    }
    
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.productBuyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: RentingListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RentingListCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RentingListCell
        }
        
        //assignihg the data to Ui compnenets
        let productBuyListDetails = self.productBuyList[indexPath.row]
        cell.rentBannerImgView.sd_setImage(with: URL.init(string: productBuyListDetails.ImageName ), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        cell.LBL_title.text = productBuyListDetails.Title
        cell.LBL_subText.text = productBuyListDetails.Category
        cell.LBL_description.text = productBuyListDetails.Description
        cell.LBL_peopleCount.text = String(describing: productBuyListDetails.ClassifiedId)
        cell.LBL_price.text = "₹ " + String(describing: productBuyListDetails.Price)
        
        cell.peopleCountSW.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.productDetailsVC
        targetVC.productID = self.productBuyList[indexPath.row].ClassifiedId
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
    
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
}

extension RUBuyVC {
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        getProductsData()
        refreshControl.endRefreshing()
    }
}
