//
//  TicketsListTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class TicketsListTableViewCell: UITableViewCell {

    @IBOutlet weak var LBL_TktType: UILabel!
    @IBOutlet weak var LBL_Subject: UILabel!
    @IBOutlet weak var LBL_Status: UILabel!
    @IBOutlet weak var LBL_Priority: UILabel!
    @IBOutlet weak var LBL_TktTypeValue: UILabel!
    @IBOutlet weak var LBL_SubjectValue: UILabel!
    @IBOutlet weak var LBL_StatusValue: UILabel!
    @IBOutlet weak var LBL_PriorityValue: UILabel!
    
    @IBOutlet weak var LBL_AddedOn: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
