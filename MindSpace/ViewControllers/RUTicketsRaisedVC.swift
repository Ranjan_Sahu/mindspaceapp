//
//  RUTicketsRaisedVC.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUTicketsRaisedVC: UIViewController, UIScrollViewDelegate {
    
    
    // MARK:- Constants
    let cellIdentifier = "TicketsListTableViewCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    var visitorReqList : [VisitorRequestList]? = nil
    var dummyImgArray : [String]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        
        self.getTicketsList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Tickets Raised"
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
}
extension RUTicketsRaisedVC {
    // MARK:- Service Calls
    func getTicketsList() {
        
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "20"],
                         "entity" : ["ComplexId" : "1"],
                         "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetVisitorRequestHistory, objectType: VisitorRequestData.self, isHud: true, controller: self, success: { visitorData in
            self.visitorReqList = visitorData.DataList
            print(self.visitorReqList!)
            self.tblView.reloadData()
            
        })
    }
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
}
extension RUTicketsRaisedVC :  UITableViewDataSource{
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if let vList = self.visitorReqList {
//            return (vList.count)
//        }
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: TicketsListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TicketsListTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TicketsListTableViewCell
        }
        
        //cell.LBL_visitorInfo.text = "1 Visitor, Malad\nFriday 25/05/2018 3 p.m\nAccenture India Pvt Ltd."
        
        return cell
    }
}
extension RUTicketsRaisedVC :  UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
