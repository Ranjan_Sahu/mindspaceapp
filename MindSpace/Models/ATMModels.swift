//
//  ATMModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct GetATMLists: Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [ATMlist]?
    let TotalRecords : Int?
}
/*
 "AtmId": 1,
 "AtmName": "ICICI Bank",
 "LocationId": 1,
 "ComplexId": 1,
 "LocationName": "Navi Mumbai",
 "ComplexName": "Mindspace Business Parks Pvt.Ltd.SEZ .Divn.",
 "BuildingName": "Building No.12"
 */

struct ATMlist: Codable {
    var AtmId : Int?
    var LocationId : Int?
    var ComplexId : Int?
    var AtmName: String?
    var LocationName : String?
    var ComplexName : String?
    var BuildingName : String?
}
struct GetAmbulanceLists: Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [Ambulancelist]?
    let TotalRecords : Int?
}
struct Ambulancelist: Codable {
    var ContactNumber : String?
    var LocationId : Int?
    var LocationName : String?
    var ComplexId : Int?
    var ComplexName : String?
    
}
