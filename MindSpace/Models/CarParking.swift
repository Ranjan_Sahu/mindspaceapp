//
//  CarParking.swift
//  MindSpace
//
//  Created by Zeeshan  on 02/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class CarParkingData : Codable {
    var DataList : [CarPArkingDetails]?
    var Status : Bool?
    var Message : String?
}
class CarPArkingDetails : Codable {
    var BuildingName : String?
    var ParkingMAP : String?
}
