//
//  RUAboutCommanSpaceDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/15/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class RUAboutCommanSpaceDetailsVC: UIViewController {
    
    
    // MARK:- Constants
    let cellIdentifier = "ACommonSpaceDetailsCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerImageView: UIImageView!

    // MARK:- Objects
    private var catrgoryArray = [String]()
    private var slides:[HomeScreenBannerView] = []
    private var bannerTimer:Timer? = nil
    private var aCommonSpaceDetails : CommonSpacesDetails? = nil
    private var dummyImgArray : [String]? = nil
    public  var bookingSpaceId : String?
    var ownerContactNo = ""
    private var menuCardURLArray : [LightboxImage] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigation()
        self.setupScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
}
extension RUAboutCommanSpaceDetailsVC {
    
    // MARK:- Custom methods
    private func setupScreen(){
        self.view.layoutIfNeeded()
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.callWebService()
    }
    private func setupNavigation(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = ""
    }
   
    
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    
  
}
extension RUAboutCommanSpaceDetailsVC {
    // MARK:- Service Calls
    
    private func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["BookingSpaceId" : bookingSpaceId ?? ""],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetBookingSpaceDetails, objectType: CommonSpacesDetailsData.self, isHud: true, controller: self, success: {
            response in
            guard let list = response.Data else {return}
            self.aCommonSpaceDetails = list
            if let imageUrl = self.aCommonSpaceDetails?.ImageUrls?.first{
                 self.bannerImageView?.sd_setImage(with: URL.init(string: imageUrl), placeholderImage:  #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.continueInBackground, completed: nil)
            }
            self.navigationItem.title = self.aCommonSpaceDetails?.BookingSpaceName?.capitalized
            self.tblView.reloadData()
        })
    }
  
}
    // MARK: - TableView datsource and delegates
extension RUAboutCommanSpaceDetailsVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if aCommonSpaceDetails != nil{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ACommonSpaceDetailsCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ACommonSpaceDetailsCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ACommonSpaceDetailsCell
        }
        if let data = self.aCommonSpaceDetails {
            cell.LBL_description.text = data.BookingSpaceDesc?.capitalized
            cell.LBL_title.text = data.BookingSpaceName?.capitalized ?? ""
            cell.LBL_timing.text = data.Time
            cell.LBL_peopleCount.text = data.BookingSpaceCapacity
            cell.parent = self
            cell.dataSource = data.ImageUrls ?? []
            
            cell.LBL_contactPerson.text = data.ContactDetails
            let tapGestureForCall = UITapGestureRecognizer(target: self, action:#selector(self.handleTap))
            cell.LBL_contactPerson.addGestureRecognizer(tapGestureForCall)
            cell.viewButton.addTarget(self, action:#selector(handlePresentAction), for: .touchUpInside)

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let contactDetails = self.aCommonSpaceDetails?.ContactDetails
        let contactno1 = String((contactDetails?.suffix(10))!).trimmingCharacters(in: .whitespaces)
        if let url = URL(string: "tel://\(contactno1)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension RUAboutCommanSpaceDetailsVC {
    @objc func handlePresentAction() {
        if let menuCardURL = self.aCommonSpaceDetails?.BookingProcessDocument {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Business Process Document"
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension RUAboutCommanSpaceDetailsVC : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
}

