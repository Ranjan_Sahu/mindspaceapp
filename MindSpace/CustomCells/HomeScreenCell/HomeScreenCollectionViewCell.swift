//
//  HomeScreenCollectionViewCell.swift
//  MindSpace
//
//  Created by webwerks on 9/27/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class HomeScreenCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backImgView: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var LBL_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
