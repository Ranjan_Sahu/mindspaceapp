//
//  PollTableViewCell.swift
//  MindSpace
//
//  Created by Webwerks on 12/14/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var resultTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCell()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension PollTableViewCell {
    private func registerCell(){
        resultTableView.register(UINib(nibName: "PollCell", bundle: nil), forCellReuseIdentifier: "AnnouncmentAnSCell")
        optionsTableView.register(UINib(nibName: "PollOptionCell", bundle: nil), forCellReuseIdentifier: "PollOptionCell")
    }
}
extension PollTableViewCell : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == optionsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PollOptionCell") as! PollOptionCell
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncmentAnSCell") as! AnnouncmentAnSCell
            return cell
        }
    }
}
extension PollTableViewCell : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (5*63)
    }
}
