//
//  Date.swift
//  MindSpace
//
//  Created by Webwerks on 11/6/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

extension Date {
    
    //period -> .WeekOfYear, .Day
    func rangeOfPeriod(period: Calendar.Component) -> (Date, Date) {
        
        var startDate = Date()
        var interval : TimeInterval = 0
        let _ = Calendar.current.dateInterval(of: period, start: &startDate, interval: &interval, for: self)
        let endDate = startDate.addingTimeInterval(interval - 1)
        
        return (startDate, endDate)
    }
    
    func calcStartAndEndOfDay() -> (Date, Date) {
        return rangeOfPeriod(period: .day)
    }
    
    func calcStartAndEndOfWeek() -> (Date, Date) {
        return rangeOfPeriod(period: .weekday)
    }
    
    func calcStartAndEndOfMonth() -> (Date, Date) {
        return rangeOfPeriod(period: .month)
    }
    
    func getSpecificDate(interval: Int) -> Date {
        var timeInterval = DateComponents()
        timeInterval.day = interval
        return Calendar.current.date(byAdding: timeInterval, to: self)!
    }
    
    func getStart() -> Date {
        let (start, _) = calcStartAndEndOfDay()
        return start
    }
    
    func getEnd() -> Date {
        let (_, end) = calcStartAndEndOfDay()
        return end
    }
    
    func isBigger(to: Date) -> Bool {
        return Calendar.current.compare(self, to: to, toGranularity: .day) == .orderedDescending ? true : false
    }
    
    func isSmaller(to: Date) -> Bool {
        return Calendar.current.compare(self, to: to, toGranularity: .day) == .orderedAscending ? true : false
    }
    
    func isEqual(to: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs: to)
    }
    
    func isElement(of: [Date]) -> Bool {
        for element in of {
            if self.isEqual(to: element) {
                return true
            }
        }
        return false
    }
    
    func getElement(of: [Date]) -> Date {
        for element in of {
            if self.isEqual(to: element) {
                return element
            }
        }
        return Date()
    }
    func getNextMonth() -> String?{
        let calDate =  NSCalendar.current.date(byAdding: .month, value: 1, to: self)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM"
        if let dDate = calDate {
            let month = dateFormat.string(from: dDate)
            return month
        }
        return ""
    }
    func getCurrentMonth() -> String?{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM"
            let month = dateFormat.string(from: self)
            return month
    }
    func getPreviousMonth() -> String?{
        let calDate =  NSCalendar.current.date(byAdding: .month, value: -1, to: self)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM"
        if let dDate = calDate {
            let month = dateFormat.string(from: dDate)
            return month
        }
        return ""
    }
}
class AnimationClass {
    
    class func BounceEffect() -> (UIView, @escaping (Bool) -> Void) -> () {
        
        return {
            view, completion in
            view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            UIView.animate(
                withDuration: 0.5,
                delay: 0, usingSpringWithDamping: 0.3,
                initialSpringVelocity: 0.1,
                options: UIViewAnimationOptions.beginFromCurrentState,
                animations: {
                    view.transform = CGAffineTransform(scaleX: 1, y: 1)
            },
                completion: completion
            )
        }
        
    }
    
    class func fadeOutEffect() -> (UIView, @escaping (Bool) -> Void) -> () {
        
        return {
            view, completion in
            UIView.animate(withDuration: 0.6,
                           delay: 0, usingSpringWithDamping: 0.6,
                           initialSpringVelocity: 0,
                           options: [],
                           animations: {
                            view.alpha = 0
            },
                           completion: completion)
        }
        
    }
    
    fileprivate class func get3DTransformation(_ angle: Double) ->
        CATransform3D {
            var transform = CATransform3DIdentity
            transform.m34 = -1.0 / 500.0
            transform = CATransform3DRotate(transform,
                                            CGFloat(angle * Double.pi / 180.0), 0, 1, 0.0)
            return transform
    }
    
    class func flipAnimation(_ view: UIView, completion: (() -> Void)?) {
        let angle = 180.0
        view.layer.transform = get3DTransformation(angle)
        UIView.animate(withDuration: 1,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: [],
                       animations: { () -> Void in
                        view.layer.transform = CATransform3DIdentity
        }) { (finished) -> Void in
            completion?()
        }
    }
    
}

class AnimationView: UIView {
    func animateWithFlipEffect(withCompletionHandler completionHandler:(() -> Void)?) {
        AnimationClass.flipAnimation(self, completion: completionHandler)
    }
    func animateWithBounceEffect(withCompletionHandler completionHandler:(() -> Void)?) {
        let viewAnimation = AnimationClass.BounceEffect()
        viewAnimation(self) { _ in
            completionHandler?()
        }
    }
    func animateWithFadeEffect(withCompletionHandler completionHandler:(() -> Void)?) {
        let viewAnimation = AnimationClass.fadeOutEffect()
        viewAnimation(self) { _ in
            completionHandler?()
        }
    }
}
