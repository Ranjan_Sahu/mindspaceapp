//
//  AnnouncementListCell.swift
//  MindSpace
//
//  Created by webwerks on 10/15/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class AnnouncementListCell: UITableViewCell {

    @IBOutlet weak var LBL_title: UILabel!
    @IBOutlet weak var LBL_description: UILabel!
    @IBOutlet weak var LBL_timing: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
