//
//  FakeModelBuilder.h
//  Example
//
//  Created by Robert Nash on 19/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

#import "MenuSection.h"

@interface FakeModelBuilder : NSObject

+(NSMutableArray *)buildMenuCompany;
+(NSMutableArray *)buildMenuProject;
+(NSMutableArray *)buildMenuContact;
+(NSMutableArray *)buildMenuCalendar;
+(NSMutableArray *)buildMenuPurchase;
+(NSMutableArray *)buildMenuToolEdit;
+(NSMutableArray *)buildMenuCallEdit;

+(NSMutableArray *)buildMenuAccountEdit;

+(NSMutableArray *)buildMenuViewCompany;
+(NSMutableArray *)buildMenuViewCalendar;
+(NSMutableArray *)buildMenuViewContact;
+(NSMutableArray *)buildMenuViewPurchase;
+(NSMutableArray *)buildMenuViewProject;
+(NSMutableArray *)buildMenuViewCall;
+(NSMutableArray *)buildMenuViewTransactionDetail;
+(NSMutableArray *)buildMenuAgreementDetails;
+(NSMutableArray *)buildMenuAgreementEdit;
+(NSMutableArray *)buildMenuBillingEdit;
+(NSMutableArray *)buildMenuViewTools;

@end