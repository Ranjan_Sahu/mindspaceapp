//
//  EntertainmentDetialsTableViewCell.swift
//  MindSpace
//
//  Created by webwerks1 on 11/30/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class EntertainmentDetialsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var LBL_gymTitle: UILabel!
    @IBOutlet weak var LBL_contactPerson: UILabel!
    @IBOutlet weak var LBL_bookThrough: UILabel!
    @IBOutlet weak var LBL_buildingNo: UILabel!
    @IBOutlet weak var LBL_peopleCount: UILabel!
    @IBOutlet weak var LBL_gymTiming: UILabel!
    @IBOutlet weak var LBL_gymDesc: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var viewButton: RoundButton!
    var imageList: [LightboxImage] = []
    var parentVC : UIViewController?
    
    public var imageURL : [String]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView?.register(UINib(nibName: "MenuCollectionViewCel", bundle: nil), forCellWithReuseIdentifier: "MenuCollectionViewCel")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
extension EntertainmentDetialsTableViewCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = imageURL?.count else { return 0 }
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCel", for: indexPath) as! MenuCollectionViewCel
        cell.imgView.sd_setImage(with: URL.init(string: self.imageURL?[indexPath.row] ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        //        if let image = self.eapDetails?.ImageUrls?[indexPath.row].thumbnail {
        //            cell.imgView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        //        }
        return cell
    }
}
/*
extension EntertainmentDetialsTableViewCell : UICollectionViewDelegateFlowLayout {
    
}*/

extension EntertainmentDetialsTableViewCell : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        parentVC?.present(controller, animated: true, completion: nil)
    }
}

extension EntertainmentDetialsTableViewCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}

extension EntertainmentDetialsTableViewCell : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    
}
