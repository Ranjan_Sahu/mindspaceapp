//
//  User.swift
//  MindSpace
//
//  Created by Zeeshan  on 16/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct  UserData : Codable {
    var Status : Bool?
    var Message : String?
    var Data : User?
}

struct  Configuration : Codable {
    let OlaLink : String?
    let UberLink : String?
    let ConfigurationId : Int?
    let OlaIOSAppLink : String?
    let OlaIOSStoreLink : String?
    let OlaAndroidAppLink : String?
    let OlaAndroidStoreLink : String?
    let UberIOSAppLink : String?
    let UberIOSStoreLink : String?
    let UberAndroidAppLink : String?
    let UberAndroidStoreLink : String?
    let BannerSlideTime : Int? = 5
}

struct  User : Codable {
    var UserId : Int?
    var FirstName : String?
    var LastName : String?
    var VisitorId : Int?
    var VisitorRequestId : Int?
    var ComplexId : Int?
    var TenantId : Int?
    var EmailAddress : String?
    var MobileNumber : String?
    var UserType : String?
    var ProfilePicture : String?
    var OTP : String?
    var Configutarion:Configuration?
    
    static func saveData(user : User){
        UserDefaults.standard.set(user.UserId, forKey: "UserId")
        UserDefaults.standard.set(user.FirstName, forKey: "FirstName")
        UserDefaults.standard.set(user.LastName, forKey: "LastName")
        UserDefaults.standard.set(user.VisitorId, forKey: "VisitorId")
        UserDefaults.standard.set(user.VisitorRequestId, forKey: "VisitorRequestId")
        UserDefaults.standard.set(user.ComplexId, forKey: "ComplexId")
        UserDefaults.standard.set(user.TenantId, forKey: "TenantId")
        UserDefaults.standard.set(user.EmailAddress, forKey: "EmailAddress")
        UserDefaults.standard.set(user.MobileNumber, forKey: "MobileNumber")
        UserDefaults.standard.set(user.UserType, forKey: "UserType")
        UserDefaults.standard.set(user.ProfilePicture, forKey: "ProfilePicture")
        //UserDefaults.standard.set(user.Configutarion?.BannerSlideTime, forKey: "BannerSlide_Time")

    }
    static func clearData(){
        UserDefaults.standard.removeObject(forKey: "UserId")
        UserDefaults.standard.removeObject(forKey: "FirstName")
        UserDefaults.standard.removeObject(forKey: "LastName")
        UserDefaults.standard.removeObject(forKey: "VisitorId")
        UserDefaults.standard.removeObject(forKey: "VisitorRequestId")
        UserDefaults.standard.removeObject(forKey: "ComplexId")
        UserDefaults.standard.removeObject(forKey: "TenantId")
        UserDefaults.standard.removeObject(forKey: "EmailAddress")
        UserDefaults.standard.removeObject(forKey: "MobileNumber")
        UserDefaults.standard.removeObject(forKey: "UserType")
        UserDefaults.standard.removeObject(forKey: "ProfilePicture")
        //UserDefaults.standard.removeObject(forKey: "BannerSlide_Time")

    }
    init() {
       self.UserId = UserDefaults.standard.object(forKey: "UserId") as? Int
        self.FirstName = UserDefaults.standard.object(forKey: "FirstName") as? String
        self.LastName = UserDefaults.standard.object(forKey: "LastName") as? String
        self.TenantId = UserDefaults.standard.object(forKey: "TenantId") as? Int
        self.VisitorId = UserDefaults.standard.object(forKey: "VisitorId") as? Int
        self.VisitorRequestId = UserDefaults.standard.object(forKey: "VisitorRequestId") as? Int
        self.ComplexId = UserDefaults.standard.object(forKey: "ComplexId") as? Int
        self.EmailAddress = UserDefaults.standard.object(forKey: "EmailAddress") as? String
        self.MobileNumber = UserDefaults.standard.object(forKey: "MobileNumber") as? String
        self.UserType = UserDefaults.standard.object(forKey: "UserType") as? String
        self.ProfilePicture = UserDefaults.standard.object(forKey: "ProfilePicture") as? String

    }
    
    
    
//    (userID : Int, firstName:String, lastName:String, visitorID:Int, visitorRequestID: Int, complexID : Int, tenantID : Int, email: String, number : String, userType:String){
//        self.UserId = userID
//        self.FirstName = firstName
//        self.LastName = lastName
//        self.VisitorId = visitorID
//        self.VisitorRequestId = visitorRequestID
//        self.ComplexId = complexID
//        self.TenantId = tenantID
//        self.EmailAddress = email
//        self.MobileNumber = number
//        self.UserType = userType
    
    
//    }
//    required convenience init?(code decoder : NSCoder) {
//        guard let title = decoder.decodeObject(forKey: "UserId") as? String, let FirstName = decoder.decodeObject(forKey: "UserId") as? String, let title = decoder.decodeObject(forKey: "UserId") as? String, let title = decoder.decodeObject(forKey: "UserId") as? String, let title = decoder.decodeObject(forKey: "UserId") as? String else { return nil}
//    }
}
