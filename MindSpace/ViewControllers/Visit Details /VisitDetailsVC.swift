//
//  VisitDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/8/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitDetailsVC: UIViewController {
    // MARK:- Constants
    let cellIdentifier = "VisitDetailsListTableViewCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    var visitorReqList : [VisitorRequestList] = []
    var dummyImgArray : [String]? = nil
    let refreshControl = UIRefreshControl()
    var pageInfo = PageInfo()
    var totalRecords : Int?
    //MARK:- Lifecyle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupScreen()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
}
// MARK:- Custom methods
extension VisitDetailsVC {
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    private func setupScreen() {
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.addRefreshControlToTableView()
        self.getVisitorList()
        self.setupNavigation()
    }
    private func setupNavigation(){
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Visit Details"
    }
    
    // MARK:- Service Calls
    private func getVisitorList() {
        
        let user = AppDelegate.appDelegateShared.user?.UserType
        var parameters = [String:Any]()
        var serviceMethod = ""
        
        if user == "Visitor"{
            
            parameters  = ["entity" : ["ComplexId" : AppDelegate.appDelegateShared.user?.ComplexId ?? "",
                                       "VisitorId": AppDelegate.appDelegateShared.user?.VisitorId ?? ""],
                           "pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
                            "PageSize" : "\(pageInfo.pageSize)"],
                           "deviceInfo" : CommonHelper.getDeviceInfo()] as [String : Any]
            
            serviceMethod = WebserviceURLs.Service_GetVisitorHistory
        }else{
            parameters  = ["entity" : ["ComplexId" : AppDelegate.appDelegateShared.user?.ComplexId ?? "",
                                       "CreatedBy": AppDelegate.appDelegateShared.user?.UserId ?? ""],
                           "pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
                            "PageSize" : "\(pageInfo.pageSize)"],
                           "deviceInfo" : CommonHelper.getDeviceInfoWithUser()] as [String : Any]
            
            serviceMethod = WebserviceURLs.Service_GetVisitorRequestHistory
        }
        
        Webservice.callPostWebservice(parameters: parameters, url: serviceMethod, objectType: VisitorRequestData.self, isHud: true, controller: self, success: { [weak self] visitors in
            self?.totalRecords = visitors.TotalRecords
            
            if self?.totalRecords == 0{
                self?.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            
            guard let list = visitors.DataList else {return}
            if self?.pageInfo.pageIndex == 1  {
                self?.visitorReqList = list
            }else {
                self?.visitorReqList.append(contentsOf: list)
            }
            self?.tblView.reloadData()
        })
        print("******************")
    }
    
}
// MARK: - Refresh Control
extension VisitDetailsVC {
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        getVisitorList()
        refreshControl.endRefreshing()
    }
}
// MARK: - TableView datsource and delegates
extension VisitDetailsVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visitorReqList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: VisitDetailsListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? VisitDetailsListTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? VisitDetailsListTableViewCell
        }
        
        let detail = self.visitorReqList[indexPath.row]
        cell.imgQRCode.sd_setImage(with:  URL.init(string: detail.QRCode ?? ""), placeholderImage: #imageLiteral(resourceName: "icon_qrcode"), options: SDWebImageOptions.highPriority, completed: nil)
        //cell.LBL_visitorInfo.text = "1 Visitor, Malad\nFriday 25/05/2018 3 p.m\nAccenture India Pvt Ltd."
        cell.LBL_visitorInfo.text = "\(detail.NumberOfVisitor ?? 0) visitor, \(detail.ComplexName ?? "") \(detail.StartTime ?? "") , \(detail.TenantName ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = self.visitorReqList[indexPath.row]
        let vc = Storyboard.qrCodeViewController
        vc.qrCodeImageName =  detail.QRCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK:- ScrollView Delegate
extension VisitDetailsVC : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
            if (endScrolling >= Int(scrollView.contentSize.height)){
                if let count = totalRecords, count > visitorReqList.count{
                    pageInfo.pageIndex += 1
                    getVisitorList()
                }else if totalRecords == nil{
                    getVisitorList()
                }
                
            }
        }
        
    }
}


