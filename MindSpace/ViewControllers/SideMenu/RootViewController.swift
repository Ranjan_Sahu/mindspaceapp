//
//  RootViewController.swift
//  Sail Uday
//
//  Created by Webwerks on 07/08/17.
//  Copyright © 2017 webwerks1. All rights reserved.
//

import UIKit

class RootViewController: SideMenu {

    override func awakeFromNib() {
        
        self.menuPreferredStatusBarStyle = UIStatusBarStyle.lightContent
        self.contentViewShadowEnabled = true
        self.scaleMenuView = false
        self.scaleContentView = false
        self.scaleBackgroundImageView = false
        self.contentViewInPortraitOffsetCenterX = 80
        self.contentViewInLandscapeOffsetCenterX = 240
        self.parallaxEnabled = false
        self.delegate = self
        let user = AppDelegate.appDelegateShared.user?.UserType//UserDefaults.standard.string(forKey: "user")

        if user == "Visitor"{
            let advanceSB = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "navVisitor")
            self.contentViewController = advanceSB
        } else {
            let mainSB =  Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "nav")
            //UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav")
            self.contentViewController = mainSB
        }
        
        
        // Side Menu
        self.leftMenuViewController = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "LeftMenuViewController")
        self.rightMenuViewController = nil
    }
}

extension RootViewController: SideMenuDelegate {
    
    func sideMenu(sideMenu: SideMenu, willShowMenuViewController menuViewController: UIViewController) {
        print("willShowMenuViewController")
    }
    
    func sideMenu(sideMenu: SideMenu, didShowMenuViewController menuViewController: UIViewController) {
        print("didShowMenuViewController")
    }
    
    func sideMenu(sideMenu: SideMenu, willHideMenuViewController menuViewController: UIViewController) {
        print("willHideMenuViewController")
    }
    
    func sideMenu(sideMenu: SideMenu, didHideMenuViewController menuViewController: UIViewController) {
        print("didHideMenuViewController")
    }
}

