//
//  TicketModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct GetTicketData : Codable {
    var Status : Bool?
    var Message : String?
    var Data : GetTicketDetails?
}

struct GetTicketDetails : Codable {
    var ComplaintTicketsId : Int?
    var ComplaintTicketNo: String?
    var Priority : String?
    var StatusType : String?
    var CategoryId : Int?
    var Subject : String?
    var ComplaintTypeName : String?
    var Description : String?
    var UserComments : String?
    var Attachment : String?
}
