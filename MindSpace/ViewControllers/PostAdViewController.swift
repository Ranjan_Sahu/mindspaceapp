//
//  PostAdViewController.swift
//  MindSpace
//
//  Created by webwerks on 12/6/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
struct PostAdd {
    var category : String = ""
    var title : String = ""
    var descriptions : String = ""
    var price : String = ""
    var categoryId : String = ""
}

class PostAdViewController: UIViewController {

    @IBOutlet weak var categoryTxtField: RoundTextField!
    @IBOutlet weak var productTitleTxtField: RoundTextField!
    @IBOutlet weak var descriptionTxtFeild: RoundTextField!
    @IBOutlet weak var priceTxtField: RoundTextField!
    
    @IBOutlet weak var imgVw1: UIImageView!
    @IBOutlet weak var imgVw2: UIImageView!
    @IBOutlet weak var imgVw3: UIImageView!
    @IBOutlet weak var imgVw4: UIImageView!
    @IBOutlet weak var imgVw5: UIImageView!
    
    @IBOutlet weak var closeBtn1: UIButton!
    @IBOutlet weak var closeBtn2: UIButton!
    @IBOutlet weak var closeBtn3: UIButton!
    @IBOutlet weak var closeBtn4: UIButton!
    @IBOutlet weak var closeBtn5: UIButton!
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var commonFields = PostAdd()
    var productCategoryList : [ProductCategoryList] = []
    var activeTextField :UITextField?
    var activeimageView :UIImageView?
    var imagePicker = UIImagePickerController()
    var selectedImagesArray = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Post Ad"
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.GetProductCategoryList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .highlighted)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func submitRequest() {

        let param = ["ClassifiedTypeId" : String(describing: commonFields.categoryId),
                     "Title" : commonFields.title,
                     "Description" : commonFields.descriptions,
                     "Price" : String(describing: commonFields.price),
                     "UserId" : String(describing: AppDelegate.appDelegateShared.user?.UserId ?? 0),
                     ]
        
        let imagesKeys = ["Image1", "Image2", "Image3", "Image4", "Image5"]
        var images = [String: UIImage]()
        
        if selectedImagesArray.count > 0 {
            for i in 0 ..< selectedImagesArray.count {
                let key = imagesKeys[i]
                images[key] = selectedImagesArray[i].image
            }
        }
        
        Webservice.uploadFiles(imageArray: images, parameters: param, url: WebserviceURLs.Service_AddProduct, objectType: UploadReqData.self, controller: self, success: { response in
            print(response.Message as Any)
            
            Constants.showAlertWithMessage(message: response.Message ?? "", presentingVC: self, okBlock: { _ in
                if response.Status == true{
                    self.navigationController?.popViewController(animated: true)
                }
            })
//            if response.Status ?? false{
//                CommonHelper.showAlert(vc: self, title: "", msg: "Ad Posted Successfully")
//            } else {
//                CommonHelper.showAlert(vc: self, title: "", msg: "Something went wrong, please try again")
//            }
        })
        
    }
    
    @IBAction func resignEditing(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func imageViewAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera(sender: sender)
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery(sender: sender)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(sender:UIButton) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            if (sender.tag == 1) {
                activeimageView = imgVw1
            }else if (sender.tag == 2) {
                activeimageView = imgVw2
            }else if (sender.tag == 3) {
                activeimageView = imgVw3
            }else if (sender.tag == 4) {
                activeimageView = imgVw4
            }else if (sender.tag == 5) {
                activeimageView = imgVw5
            }
            
            self.imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType    = .camera
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            present(imagePicker, animated: true, completion: nil)
        }else {
            CommonHelper.showAlert(vc: self, title:"WARNING", msg: "Can not access Camera !")
        }
    }
    
    func openGallery(sender:UIButton) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)){
            if (sender.tag == 1) {
                activeimageView = imgVw1
            }else if (sender.tag == 2) {
                activeimageView = imgVw2
            }else if (sender.tag == 3) {
                activeimageView = imgVw3
            }else if (sender.tag == 4) {
                activeimageView = imgVw4
            }else if (sender.tag == 5) {
                activeimageView = imgVw5
            }
            
            self.imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType    = .photoLibrary
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(imagePicker, animated: true, completion: nil)
        }else {
            CommonHelper.showAlert(vc: self, title:"WARNING", msg: "Can not access Gallery !")
        }
    }
    
    @IBAction func imgDeleteAction(_ sender: UIButton) {
        
        if (sender.tag == 201) {
            imgVw1.image = UIImage(named: "bannerPlaceholder")
            closeBtn1.isHidden = true
        }else if (sender.tag == 202) {
            imgVw2.image = UIImage(named: "bannerPlaceholder")
            closeBtn2.isHidden = true
        }else if (sender.tag == 203) {
            imgVw3.image = UIImage(named: "bannerPlaceholder")
            closeBtn3.isHidden = true
        }else if (sender.tag == 204) {
            imgVw4.image = UIImage(named: "bannerPlaceholder")
            closeBtn4.isHidden = true
        }else if (sender.tag == 205) {
            imgVw5.image = UIImage(named: "bannerPlaceholder")
            closeBtn5.isHidden = true
        }

    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: categoryTxtField.text!, controller: self,fieldName :FieldNames.Category.rawValue)){
            return;
        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: productTitleTxtField.text!, controller: self,fieldName :FieldNames.Title.rawValue)){
            return;
        }
//        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: descriptionTxtFeild.text!, controller: self,fieldName :FieldNames.Description.rawValue)){
//            return;
//        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: priceTxtField.text!, controller: self,fieldName :FieldNames.Price.rawValue)){
            return;
        }
        
        if checkImageSelection(imageView: imgVw1) && checkImageSelection(imageView: imgVw2) && checkImageSelection(imageView: imgVw3) && checkImageSelection(imageView: imgVw4) && checkImageSelection(imageView: imgVw5) {
            CommonHelper.showAlert(vc: self, title: "WARNING", msg: "Please select at least one image")
            return
        }
        
        if !checkImageSelection(imageView: imgVw1) {
            selectedImagesArray.append(imgVw1)
        }
        if !checkImageSelection(imageView: imgVw2) {
             selectedImagesArray.append(imgVw2)
        }
        if !checkImageSelection(imageView: imgVw3) {
             selectedImagesArray.append(imgVw3)
        }
        if !checkImageSelection(imageView: imgVw4) {
             selectedImagesArray.append(imgVw4)
        }
        if !checkImageSelection(imageView: imgVw5) {
             selectedImagesArray.append(imgVw5)
        }
        
        self.submitRequest()
    }
}

extension PostAdViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            switch self.activeimageView?.tag {
            case 101 : do {
                self.imgVw1.image = pickedImage
                closeBtn1.isHidden = false
            }
                break
            case 102 : do {
                self.imgVw2.image = pickedImage
                closeBtn2.isHidden = false
            }
                break
            case 103 : do {
                self.imgVw3.image = pickedImage
                closeBtn3.isHidden = false
            }
                break
            case 104 : do {
                self.imgVw4.image = pickedImage
                closeBtn4.isHidden = false
            }
                break
            case 105 : do {
                self.imgVw5.image = pickedImage
                closeBtn5.isHidden = false
            }
                break
            default :  break
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension PostAdViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView?.autoresizingMask = .flexibleHeight
        activeTextField = textField
        if textField == categoryTxtField {
            textField.inputView = pickerView
            pickerView?.reloadAllComponents()
        }
        textField.inputAccessoryView = toolBar
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case categoryTxtField:
            if let pickView = pickerView {
                selectRow(row: pickView.selectedRow(inComponent: 0))
            }
        case productTitleTxtField:
            commonFields.title = textField.text ?? ""
            break
        case descriptionTxtFeild:
            commonFields.descriptions = textField.text ?? ""
            break
        case priceTxtField:
            commonFields.price = textField.text ?? "0"
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeTextField = nil
        return true
    }
    
}

extension PostAdViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if activeTextField == categoryTxtField {
            return productCategoryList.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if activeTextField == categoryTxtField {
            return productCategoryList[row].ProductCategoryName ?? ""
        }
        return  ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
    }
    
    func selectRow(row : Int) {
        if activeTextField == categoryTxtField {
            if row < productCategoryList.count {
                activeTextField?.text = productCategoryList[row].ProductCategoryName
                commonFields.category =  productCategoryList[row].ProductCategoryName!
                commonFields.categoryId = "\(productCategoryList[row].ProductCategoryId ?? 0)"
            }else {
                return
            }
        }
    }
}

// MARK: - Webservices
extension PostAdViewController {
    func GetProductCategoryList() {
        let parameters = ["deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetProductCategoryList, objectType: ProductCategoryListData.self, isHud: true, controller: self, success: {
            response in
            guard let list = response.DateList else {return}
            self.productCategoryList = list
        })
    }
}
extension PostAdViewController {
    func checkImageSelection(imageView: UIImageView) -> Bool {
        if imageView.image == UIImage(named: "bannerPlaceholder") {
            return true
        }
        return false
    }
}
