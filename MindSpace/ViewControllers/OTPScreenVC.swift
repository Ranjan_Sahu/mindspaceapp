//
//  OTPScreenVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class OTPScreenVC: UIViewController, UITextFieldDelegate {

    // Constants
    
    // Outlets
    @IBOutlet weak var view_OTP: UIView!
    @IBOutlet weak var BTN_Submit: UIButton!
    @IBOutlet weak var TF_OTP1: UITextField!
    @IBOutlet weak var TF_OTP2: UITextField!
    @IBOutlet weak var TF_OTP3: UITextField!
    @IBOutlet weak var TF_OTP4: UITextField!
    @IBOutlet weak var TF_OTP5: UITextField!
    @IBOutlet weak var TF_OTP6: UITextField!
    @IBOutlet weak var TF_OTP_Main: UITextField!
    @IBOutlet weak var LBL_EnterOTP: UILabel!
    
    // Objects
    var txtFieldsArray:[UITextField]? = nil
    var otpStrArray:[String]? = nil
    var otpText:String?
    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(sender:)))
        self.view.addGestureRecognizer(tap)
        
        self.LBL_EnterOTP.attributedText = self.callMethodForAttributedString()
        TF_OTP_Main.delegate = self
        self.txtFieldsArray = [UITextField]()
        txtFieldsArray?.append(self.TF_OTP1)
        txtFieldsArray?.append(self.TF_OTP2)
        txtFieldsArray?.append(self.TF_OTP3)
        txtFieldsArray?.append(self.TF_OTP4)
        txtFieldsArray?.append(self.TF_OTP5)
        txtFieldsArray?.append(self.TF_OTP6)
        self.otpStrArray = [String]()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }

    // Selector methods
    @objc func didTapOnView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // IBActions
    @IBAction func submitAction(_ sender: Any) {
        
//        if(!CommonHelper.validateField(type: FieldTypes.OtpValidation, val: self.TF_OTP_Main.text!, controller: self, expectedVal: self.otpText)){
//            return;
//        }

        let targetVC = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
            self.navigationController?.pushViewController(targetVC, animated: true)
            self.navigationController?.viewControllers = [targetVC]
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOTPAction(_ sender: Any) {
        
    }
    
    // MARK:- UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! + (string.count - range.length)) > 6 {
            return false
        }
        
        if (string == "") {
            self.otpStrArray?.removeLast()
            let index = (self.otpStrArray?.count)!
            self.txtFieldsArray?[index].text = string
        }else {
            self.otpStrArray?.append(string)
        }
        
        if ((self.otpStrArray?.count)! > 0) {
            for i in 0 ..< (self.otpStrArray?.count)! {
                let char = self.otpStrArray![i]
                let charStr = char
                self.txtFieldsArray![i].text = charStr
            }
        }
        
        return true
        
    }
    
    // MARK:- Custom Method
    func callMethodForAttributedString()->(NSMutableAttributedString) {
        
        let font1:UIFont = UIFont.init(name: "HelveticaNeue", size: 21.0)!
        let font2:UIFont = UIFont.init(name: "HelveticaNeue", size: 18.0)!
//        let font1:UIFont = UIFont.systemFont(ofSize: 20)
//        let font2:UIFont = UIFont.systemFont(ofSize: 18)
        let color1:UIColor = UIColor.white
        
        let dict1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor : color1] as [NSAttributedStringKey : Any]
        let dict2 = [NSAttributedStringKey.font: font2, NSAttributedStringKey.foregroundColor : color1] as [NSAttributedStringKey : Any]

        let attString = NSMutableAttributedString()
        attString.append(NSAttributedString.init(string: "Enter Your Code \n\n", attributes: dict1))
        attString.append(NSAttributedString.init(string: "An OTP has been sent to your mobile number,\nplease enter below", attributes: dict2))
        
        return attString
    }
    
}
