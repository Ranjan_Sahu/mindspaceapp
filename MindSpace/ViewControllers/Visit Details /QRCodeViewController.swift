//
//  QRCodeViewController.swift
//  MindSpace
//
//  Created by Webwerks on 11/9/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var imgQRCode: UIImageView!
    
    //MARK:- Variables
    var qrCodeImageName: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupScreen()
        imgQRCode.contentMode = .scaleAspectFit
        imgQRCode.sd_setImage(with:  URL.init(string: qrCodeImageName ?? ""), placeholderImage: #imageLiteral(resourceName: "icon_qrcode"), options: SDWebImageOptions.highPriority, completed: nil)
        
    }
}
extension QRCodeViewController {
    private func setupScreen(){
        self.setupNavigation()
    }
    private func setupNavigation(){
        self.navigationController?.navigationBar.isHidden = false
        self.title = "QR Code"
    }
}
