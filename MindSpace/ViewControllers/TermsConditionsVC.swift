//
//  TermsConditionsVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
//import PDFReader

class TermsConditionsVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var BTN_Agree: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    // Objects
    public var sysDataInfo : SystemDataInfo?
    
    //MARK:- Controllers methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupScreen()
    }
}
//MARK:- Actions
extension TermsConditionsVC {
    
    // IBActions
    @IBAction func agreeAction(_ sender: Any) {
        self.navigationController?.pushViewController(Storyboard.selectPremiseVC, animated: true)
    }    
}

extension TermsConditionsVC {
    private func setupScreen(){
        self.webView.delegate = self
        let termsText = UserDefaults.standard.value(forKey: "Terms_String") as? String
        self.webView.loadHTMLString(termsText ?? "" , baseURL: nil)
        self.setupNavigation()
    }
    private func setupNavigation(){
        navigationItem.title = "Terms & Conditions"
    }
}
extension TermsConditionsVC : UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
