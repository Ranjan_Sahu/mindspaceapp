//
//  RUAnnouncementsVC.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUAnnouncementsVC: UIViewController{
    
    // MARK:- Constants
    let cellIdentifier = "AnnouncementListCell"
    let cellIdentifier2 = "AnnouncementResultCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var janBtn: UIButton!
    @IBOutlet weak var febBtn: UIButton!
    @IBOutlet weak var calenderBtn: UIButton!
    @IBOutlet weak var noDataView: UIView!

    // MARK:- Objects
    var announcementList : [AnnouncementList] = [] {
        didSet {
            tblView.reloadData()
            if announcementList.count == 0{
                self.view.makeToast("No data available", duration: 1.0, position: .center)
            }else {
                tblView.tableFooterView = nil
            }
        }
    }
    let refreshControl = UIRefreshControl()
    var pageInfo = PageInfo()
    var totalRecords : Int?
    var checkData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefreshControlToTableView()
        addLeftMenuButton()
        self.registerNibWithIdentifier(identifier: cellIdentifier)
//        self.registerNibWithIdentifier(identifier: cellIdentifier2)
      //  self.getAnnouncementList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let currentDate = Date()
        janBtn.setTitle(currentDate.getCurrentMonth(), for: .normal)
        febBtn.setTitle(currentDate.getNextMonth(), for: .normal)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Announcements"
        self.getAnnouncementList()
    }
}
// MARK: - Button Action
extension RUAnnouncementsVC {
    
    @IBAction func allBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: true, janBtn: false, febBtn: false, calenderbtn: false)
    }
    @IBAction func janBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: true, febBtn: false, calenderbtn: false)
    }
    @IBAction func febBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: false, febBtn: true, calenderbtn: false)
    }
    @IBAction func calenderBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: false, febBtn: false, calenderbtn: true)
    }
    
}
extension RUAnnouncementsVC {
    // MARK:- Service Calls
    func getAnnouncementList() {

        let month = getMonthButtontitle()
        
        let parameter = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)", "Month": "\(month)"],
                         "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetAnnouncementList, objectType: AnnouncementListData.self, isHud: true, controller: self, success: { [weak self] announcements in
            self?.totalRecords = announcements.TotalRecords
            guard let list = announcements.DataList else {
                if !self!.announcementList.isEmpty && !(self?.checkData)! {
                    self?.announcementList.removeAll()
                }
                return
            }
            self?.checkData = true
            if self?.pageInfo.pageIndex == 1  {
                self?.announcementList = list
            }else {
                self?.announcementList.append(contentsOf: list)
            }
            print(self?.announcementList)
            self?.tblView.reloadData()
        })
    }
    // MARK:- Custom methods
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    private func buttonSelection(allBtn : Bool, janBtn : Bool, febBtn: Bool, calenderbtn  : Bool) {
        self.allBtn.isSelected = allBtn
        self.janBtn.isSelected = janBtn
        self.febBtn.isSelected = febBtn
        //calenderBtn.isSelected = calenderbtn
        checkData = false
        handleRefresh()
    }
}
// MARK: - Refresh Control
extension RUAnnouncementsVC {
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        getAnnouncementList()
        refreshControl.endRefreshing()
    }
}
extension RUAnnouncementsVC : UITableViewDataSource, UITableViewDelegate  {
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return announcementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if (indexPath.row == 0){
//            let cell: AnnouncementResultCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier2) as? AnnouncementResultCell
//            cell.dataSource = [["Ans" : "Option", "Result" : 50.0],["Ans" : "Option", "Result" : 30.0],["Ans" : "Option", "Result" : 20.0],]
//            cell.tablHeightConstraint.constant = CGFloat(63 * cell.dataSource.count)
//
//            cell.selectionStyle = .none
//            return cell
//        }
//        else {
            var cell: AnnouncementListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? AnnouncementListCell
            if cell == nil {
                tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? AnnouncementListCell
            }
            
        cell.LBL_title?.text = self.announcementList[indexPath.row].AnnouncementTitle
        cell.LBL_description?.text = self.announcementList[indexPath.row].AnnouncementDesc
        cell.LBL_timing?.text = "Added \(self.announcementList[indexPath.row].Date ?? "")"
            
            return cell
//        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.announcementDetailsVC
        targetVC.annoucementId = self.announcementList[indexPath.row].AnnouncementId ?? 0
        targetVC.navTitle = announcementList[indexPath.row].AnnouncementTitle
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
// MARK:- ScrollView Delegate
extension RUAnnouncementsVC : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
            if (endScrolling >= Int(scrollView.contentSize.height)){
                pageInfo.pageIndex += 1
                if let count = totalRecords, count > announcementList.count{
                    pageInfo.pageIndex += 1
                    getAnnouncementList()
                }else if totalRecords == nil{
                    getAnnouncementList()
                }
            }
        }
    }
}
extension RUAnnouncementsVC {
    func getMonthButtontitle() -> String {
        if allBtn.isSelected {
            let title = allBtn.title(for: .normal)
            return title!
        } else if janBtn.isSelected {
            let title = janBtn.title(for: .normal)
            return title!
        } else if febBtn.isSelected {
            let title = febBtn.title(for: .normal)
            return title!
        }
        return ""
    }
}


