//
//  AnnouncementResultCell.swift
//  MindSpace
//
//  Created by webwerks on 17/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class AnnouncementResultCell: UITableViewCell {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var pollCategory: UILabel!
    
    @IBOutlet weak var tablHeightConstraint: NSLayoutConstraint!
    let cellIdentifier = "AnnouncmentAnSCell"
    var selectedAnswerID = 0
    var dataSource : [PollAnswerList] = [] {
        didSet {
            setupTable()
        }
    }
    func setupTable()  {
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 420
        tableview.reloadData()
    }
    
}
extension AnnouncementResultCell : UITabBarDelegate,UITableViewDataSource{
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AnnouncmentAnSCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? AnnouncmentAnSCell
        let pollDetails = dataSource[indexPath.row]
        cell.ansLabel.text = pollDetails.AnswerText
        let percInt = doubleToInteger(data: Double(pollDetails.Result)!)
        cell.percentageLabel.text = "\(Int(percInt))%"
        
        let doubleVal = Double(pollDetails.Result)
        let result = Float((doubleVal ?? 100.0) / 100.0)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1) {
                cell.progressView.progress = Float(result)
                if(self.selectedAnswerID == pollDetails.PollAnswerId){
                    cell.progressView.progressTintColor = CommonHelper.hexStringToUIColor(hex: "1c5ab0")
                }else{
                    cell.progressView.progressTintColor = .darkGray
                }
                cell.progressView.layoutIfNeeded()
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func doubleToInteger(data:Double)-> Int {
        let doubleToString = "\(data)"
        let stringToInteger = (doubleToString as NSString).integerValue
        
        return stringToInteger
    }
}

