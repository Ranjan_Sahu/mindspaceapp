//
//  FAQCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {
    @IBOutlet weak var quesLabel: UILabel!
    @IBOutlet weak var ansLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
