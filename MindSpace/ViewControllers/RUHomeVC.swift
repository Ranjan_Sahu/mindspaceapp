
//
//  RUHomeVC.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUHomeVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "HomeScreenCollectionViewCell1"
    
    // MARK:- Outlets
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var imageViewBaanner: RoundImageView!
    @IBOutlet weak var marqeeLabel: MarqueeLabel!
    
    // MARK:- Objects
    var namesArray = [String]()
    var imgNameArray = [String]()
    var slides:[HomeScreenBannerView] = []
    var bannerList : [BannerList] = []
    var accountStatus : GetAccountStatus?
    var slide: CustomInfiniteScroller?
    var offerStrings: [String] = ["This is a long, attributed string, that's set up to loop in a continuous fashion!", "This is second long, attributed string!"]
    
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAccountStatus()
        self.getBannerList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.reloadData()
        bannerView.layer.cornerRadius = 4.0
        imageViewBaanner.layer.cornerRadius = 4.0
        self.setupScreen()
        self.setupNavigation()
        if((slide) != nil){
            if(bannerList.count > 1){
                slide?.startTimer()
                self.getBannerList()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if((slide) != nil){
            slide?.stopTimer()
        }
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}

//MARK:- Class Methods
extension RUHomeVC {
    private func setupScreen(){
        namesArray = ["Events ", "Gallery", "Services", "F & B", "Help Desk", "Marketplace"]
        imgNameArray = ["1", "6", "2", "3", "5", "4"]
        if AppDelegate.appDelegateShared.window?.frame.height ?? 0 > CGFloat(800.0) {
            bannerHeight.constant = 240
        }else{
            if(Constants.Device.IS_IPHONE_5 ){
                bannerHeight.constant = 190
            }else{
               bannerHeight.constant = 220
            }
            
        }
        
        self.view.layoutIfNeeded()
        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.isScrollEnabled = false
        self.addLeftMenuButton()
        // Disable Offers Label for now.
        //self.setupOffersLabel()
    }
    
    private func setupNavigation() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Mindspace"
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "icon_bell"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(didSelectNotification(sender:)))
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    @objc func didSelectNotification(sender: UIButton) {
        self.navigationController?.pushViewController(Storyboard.notificationsVC, animated: true)
    }
    
    private func setupOffersLabel() {
        marqeeLabel.tag = 101
        marqeeLabel.animationDelay = 0.0
        marqeeLabel.type = .continuous
        marqeeLabel.speed = .rate(60.0)
        marqeeLabel.fadeLength = 0.0
        marqeeLabel.trailingBuffer = 10.0
        
        let offersString = offerStrings.joined(separator: " ")
        let attributedOffersString = NSMutableAttributedString(string: offersString)
        attributedOffersString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.123, green: 0.331, blue: 0.657, alpha: 1.000), range: NSMakeRange(0,34))
        attributedOffersString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.657, green: 0.096, blue: 0.088, alpha: 1.000), range: NSMakeRange(34, attributedOffersString.length - 34))
        attributedOffersString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Regular", size:18.0) ?? UIFont.systemFont(ofSize: 18.0), range: NSMakeRange(0, 16))
        attributedOffersString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Regular", size:18.0) ?? UIFont.systemFont(ofSize: 18.0), range: NSMakeRange(33, attributedOffersString.length - 33))
        marqeeLabel.attributedText = attributedOffersString
    }
}

// MARK: - CollectionView Delegates and DataSource
extension RUHomeVC :  UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
        cell.LBL_title.text = namesArray[indexPath.row]
        cell.imgView.image = UIImage(named: imgNameArray[indexPath.row])
        cell.alpha = 0.2         //assigning the animation
        cell.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 1.0, animations: {
            cell.alpha = 1
            cell.transform = .identity
        })
        return cell
    }
    
}
extension RUHomeVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0 : self.navigationController?.pushViewController(Storyboard.ruEventsVC, animated: true)
        case 1 : self.navigationController?.pushViewController(Storyboard.ruGalleryVC, animated: true)
        case 2 : self.navigationController?.pushViewController(Storyboard.newRUServiceVC, animated: true)
        case 3 : self.navigationController?.pushViewController(Storyboard.ruFAndBLocationsVC, animated: true)
        case 4 : self.navigationController?.pushViewController(Storyboard.ruHelpDeskVC, animated: true)
        case 5 : self.navigationController?.pushViewController(Storyboard.ruMarketPlaceVC, animated: true)
        default : break
        }
    }
}
extension RUHomeVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (self.collectionView.bounds.size.width-6)/3
        //let h = (self.collectionView.bounds.size.height-6)/3
        let size = CGSize.init(width: w, height: w + 20)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
}


extension RUHomeVC {
    
    // MARK:- Service Calls
    
    func getAccountStatus(){
        
        let parameters = [
            "entity" : ["UserId" : CommonHelper.getUserID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()] as [String : Any]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetAccountStatus, objectType: GetAccountStatus.self, isHud: true, controller: self, success: { response in
            self.accountStatus = response
            if self.accountStatus?.Status == false{            //account is deactivated
                let alert = UIAlertController(title: self.accountStatus?.Message, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                    User.clearData()
                    let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
                    AppDelegate.appDelegateShared.user = nil
                    let navController = UINavigationController.init(rootViewController: targetVC)
                    //self.sideMenuViewController?.navigationController?.pushViewController(targetVC, animated: true)
                    //self.sideMenuViewController?.navigationController?.viewControllers = [targetVC]
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = navController
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        })

    }
    func getBannerList() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "10"],
                         "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetBannerList, objectType: BannerData.self, isHud: true, controller: self, success: { [weak self] bannerData in
            
            self?.bannerList = bannerData.DataList!
            self?.setUpBanner()
        })
    }
    
    // MARK:- Custom methods
    private func setUpBanner() {
        if bannerList.count > 0{
            slide = Bundle.main.loadNibNamed("CustomInfiniteScroller", owner: self, options: nil)?.first as? CustomInfiniteScroller
            slide!.frame = CGRect(x: 0, y: 0, width: bannerView.frame.size.width, height: bannerView.frame.size.height)
            bannerView.addSubview(slide!)
            slide!.setupBannerView(bannerArray: self.bannerList, delegate: self)
            slide?.startTimer()
        } else {
            bannerHeight.constant  = 0
        }
    }
    
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        
        if ((self.bannerList.count)>0) {
            for item in self.bannerList {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                let imgStr = item.BannerImage
                slide.bannerImgView.sd_setImage(with: URL(string: imgStr!), placeholderImage: UIImage(named: "bannerPlaceholder"))
                bannerVwArray.append(slide)
            }
        }else {
            let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
            slide.bannerImgView.image = UIImage(named: "bannerPlaceholder")
            bannerVwArray.append(slide)
        }
        return bannerVwArray
    }
}

extension RUHomeVC:CustomBannerDelegate{
    
    func didSelectBanner(bannerId:Int){
        
        if( bannerId > bannerList.count){
            return
        }
        print(bannerId)
        let banner = self.bannerList[bannerId-1]
        print(banner)
        switch banner.BannerTypeId {
        case 1 :
            let vc = Storyboard.eventDetailsVC
            vc.id = banner.BannerForId
            self.navigationController?.pushViewController(vc, animated: true)
        case 2 :
            let vc = Storyboard.galleryDetailsVC
            vc.albumID = banner.BannerForId
            self.navigationController?.pushViewController(vc, animated: true)
        case 3 :
            let vc = Storyboard.fandBDetailsVC
            vc.outletId = banner.BannerForId
            self.navigationController?.pushViewController(vc, animated: true)
        case 4 :
            let vc = Storyboard.announcementDetailsVC
            vc.annoucementId = banner.BannerForId
            self.navigationController?.pushViewController(vc, animated: true)
        case 5 :
            let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "GymDetailsViewController") as! GymDetailsViewController
            targetVC.id = "\(banner.BannerForId ?? 0)"
            self.navigationController?.pushViewController(targetVC, animated: true)
        default : break
        }
    }
}

