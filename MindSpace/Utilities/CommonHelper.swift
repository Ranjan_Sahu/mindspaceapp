//
//  CommonHelper.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import SystemConfiguration

enum FieldTypes {
    case Normal
    case Email
    case Phone
    case OtpValidation
}

enum FieldNames : String {
    case FirstName = "First name"
    case LastName  = "Last name"
    case Email     = "Email"
    case Phone     = "Phone"
    case Category     = "Category"
    case Title     = "Title"
    case Description     = "Description"
    case Price     = "Price"
}

class CommonHelper: NSObject {
    
    class  func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        let color =  UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        
        return color
    }
    
    class func getTimeStamp()->String {
        // let date = Date()
        //let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy/MM/dd HH:mm:ss a"
        //let updatedString = formatter.string(from: date)
        let timestamp = String(NSDate().timeIntervalSince1970)
        return timestamp
    }
    
    class func getDiffBetweenDates(strdate1:String,strdate2:String)->String{
        let userCalendar = Calendar.current
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second,.nanosecond]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let dateObj1 = dateFormatter.date(from: strdate1)
        let dateObj2 = dateFormatter.date(from: strdate2)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: dateObj1!, to: dateObj2!)
        let diff = "\(timeDifference.day ?? 0)"
        return diff
    }
    
    class func checkReachability()->Bool{
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func validateEmail(testStr:String) -> Bool {
        print("\(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func validatePhoneNumber(testStr:String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        print(result)
        return result
    }
    
    class func StatusBarCustom() {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = self.hexStringToUIColor(hex: "0d4b7c")
        }
        
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.backgroundColor=self.hexStringToUIColor(hex: "0d4b7c")
    }
    
    class func getDeviceInfo()->Dictionary<String, Any>
    {
        let deviceUDID = UIDevice.current.identifierForVendor?.uuidString //"2ftygifhuidhgiofjhitj"
        let timeStamp = self.getTimeStamp() //"1245012102"
        let deviceTypeID = "1"
        
        let deviceInfoDict = ["DeviceTypeId" : deviceTypeID,
                              "UDID" : deviceUDID,
                              "Timestamp" : timeStamp]
        
        return deviceInfoDict as Dictionary<String, Any>
    }
    
    class func getDeviceInfoWithUser()->Dictionary<String, Any>
    {
        let deviceUDID = UIDevice.current.identifierForVendor?.uuidString
        let timeStamp = self.getTimeStamp()
        let deviceTypeID = "1"
        let user = AppDelegate.appDelegateShared.user?.UserType
        if user == "Visitor"{
            let deviceInfoDict = ["DeviceTypeId" : deviceTypeID,
                                  "UDID" : deviceUDID as Any,
                                  "Timestamp" : timeStamp,
                                  "UserId" : AppDelegate.appDelegateShared.user?.VisitorId ?? 0] as [String : Any]
            return deviceInfoDict
            
        }else{
            let deviceInfoDict = ["DeviceTypeId" : deviceTypeID,
                                  "UDID" : deviceUDID as Any,
                                  "Timestamp" : timeStamp,
                                  "UserId" : AppDelegate.appDelegateShared.user?.UserId ?? 0] as [String : Any]
            return deviceInfoDict
        }
    }
//    {
//        let deviceUDID = "2ftygifhuidhgiofjhitj" //UIDevice.current.identifierForVendor?.uuidString
//        let timeStamp = "1245012102" //self.getTimeStamp()
//        let deviceTypeID = "1"
//
//        var deviceInfoDict = Dictionary<String, Any>()
//        deviceInfoDict["DeviceTypeId"] = deviceTypeID
//        deviceInfoDict["UDID"] = deviceUDID
//        deviceInfoDict["Timestamp"] = timeStamp
//
//
//
//        AppDelegate.appDelegateShared
//        return deviceInfoDict
//    }
    
    class func getComplexID()->Int {
        if let val = UserDefaults.standard.object(forKey: "Selected_Complex_ID") {
            return val as! Int
        }else {
            return 0
        }
    }
    
    class func getLocationID()->Int {
        if let val = UserDefaults.standard.object(forKey: "Selected_Location_ID") {
            return val as! Int
        }else {
            return 0
        }
    }
    class func getUserID() -> Int {
        if let id = User.init().UserId {
            return id
        }
        else {
            return 0
        }
    }
    class func getTenantID() -> Int {
        if let id = User.init().TenantId {
            return id
        }
        else {
            return 0
        }
    }
    class func showAlert(vc:UIViewController, title:String, msg:String) {
        let alert = UIAlertController.init(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default) { (UIAlertAction) in
        }
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
    
    class func validateField(type:FieldTypes, val:String, controller:UIViewController,fieldName:String? = "",expectedVal:String? = "1") -> Bool{
        let alert_title = "WARNING"
        switch type {
        case .Normal:
            if val.isEmpty{
                CommonHelper.showAlert(vc: controller, title:alert_title, msg: fieldName! + Warnings.Field_Empty)
                return false
            }
            return true
        case .Email:
            if !val.isValidEmail(){
                CommonHelper.showAlert(vc: controller, title:alert_title, msg:Warnings.Invalid_Email)
                 return false
            }
            return true
        case .Phone:
            if !val.isValidNumber(){
                CommonHelper.showAlert(vc: controller, title:alert_title, msg:Warnings.Invalid_Phone)
                return false
            }
            return true
        case .OtpValidation:
            let expVal = Int(expectedVal!)
            let orgVal = Int(val)
            
            if (expVal != orgVal){
                CommonHelper.showAlert(vc: controller, title:alert_title, msg:Warnings.Invalid_Otp)
                return false
            }
            return true
        }
    }
}
