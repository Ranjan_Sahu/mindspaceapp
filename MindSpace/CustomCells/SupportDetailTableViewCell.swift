//
//  SupportDetailTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 1/11/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit

class SupportDetailTableViewCell: UITableViewCell , UITextViewDelegate {

    @IBOutlet weak var LBL_CategoryName: UILabel!
    @IBOutlet weak var LBL_tickcetSubject: UILabel!
    @IBOutlet weak var LBL_priority: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var viewAttcehmentButton: RoundButton!
    @IBOutlet weak var LBL_status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
 
        descriptionTextView.delegate = self
        commentsTextView.delegate = self
        
        // Left align TextViews as per the other views.
        descriptionTextView.contentInset = UIEdgeInsetsMake(0.0, -5.0, 0.0, 0.0)
        commentsTextView.contentInset = UIEdgeInsetsMake(0.0, -5.0, 0.0, 0.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
    }
    
    @IBAction func viewAttchementTapped(_ sender: Any) {
 //       ViewTicketViewController.showAttcehmentPhoto()
    }
}


