//
//  FandBDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 08/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox
import Alamofire

class FandBDetailsVC: UIViewController {
    
    @IBOutlet weak var locarionBackImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var offerCollectionView: UICollectionView!
    @IBOutlet weak var offerViewHeightConstraints:NSLayoutConstraint!
    @IBOutlet weak var contactBtn: RoundCornerButton!
    @IBOutlet weak var containerView: RoundView!
    @IBOutlet weak var lblNoOffers: UILabel!


    let cellIdentifier = "MenuCollectionViewCell"
    let cellIdentifierOffer = "OffersCollectionViewCell"
    
    
    
    //MARK:- Objects
    private var fbDetails : FAndBDetails?
    public  var outletId  : Int?
    private var imageList : [LightboxImage] = []
    private var menuCardURLArray : [LightboxImage] = []
    var ownerContactNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getFAndBDetails()
    }
    
    @IBAction func onClickViewButton(_ sender: Any) {
        showMenuCard()
    }
}


extension FandBDetailsVC {
    
    func getFAndBDetails() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["FNBOutletId" : self.outletId ?? ""],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetFNBDetails, objectType: FAndBDetailsData.self, isHud: true, controller: self, success: { [weak self] fbData in
            
            
            guard let data = fbData.Data else {
                let alert = UIAlertController.init(title: "Error", message: Messages.noData, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
                    
                    self?.dismiss(animated: true, completion: nil)
                    
                    let viewControllers: [UIViewController] = (self?.navigationController!.viewControllers)!
                self?.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                }
                alert.addAction(okAction)
                self?.present(alert, animated: true, completion: nil)
                self?.view.isHidden = true
                return
            }
            
            self?.fbDetails = data
            if (self?.fbDetails) != nil {
                self?.setupData()
            }
        })
    }
    
    // MARK:- Custom Methods
    private func setupData() {
        
         if (self.fbDetails?.BannerImage) != nil {
            self.locarionBackImageView.sd_setImage(with: URL.init(string: self.fbDetails?.BannerImage ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        self.nameLabel.text = self.fbDetails?.FNBOutletName
        self.descriptionLabel.text = self.fbDetails?.Description
        self.sizeLabel.text = "\(self.fbDetails?.Capacity ?? 10)"
        self.addressLabel.text = self.fbDetails?.BuildingName
        self.timeLabel.text = self.fbDetails?.Time
        
        let ownerName = self.fbDetails?.ContactPerson
        ownerContactNo = self.fbDetails?.ContactNumber ?? ""
        
        let font1:UIFont = UIFont.init(name: "Ubuntu", size: 18.0)!
        let font2:UIFont = UIFont.init(name: "Ubuntu", size: 20.0)!
        let dict1 = [NSAttributedStringKey.font: font1] as [NSAttributedStringKey : Any]
        let dict2 = [NSAttributedStringKey.font: font2] as [NSAttributedStringKey : Any]
        let attString = NSMutableAttributedString()
        attString.append(NSAttributedString.init(string: "Contact \(ownerName!)", attributes: dict1))
        attString.append(NSAttributedString.init(string: " \(ownerContactNo)", attributes: dict2))
        
        self.contactBtn.setAttributedTitle(attString, for: UIControlState.normal)
        self.menuCollectionView.reloadData()
        self.offerCollectionView.reloadData()
        self.lblNoOffers.isHidden = true
        if(self.fbDetails?.Offers?.count == 0){
            self.lblNoOffers.isHidden = false
        }
        self.setImageViewerDataSource()
        self.navigationItem.title = fbDetails?.FNBOutletName?.capitalized ?? " "
    }
    private func setImageViewerDataSource() {
        self.imageList = (self.fbDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0.ImageName!)!) }))!
    }
}
extension FandBDetailsVC {
    // MARK:- IBActions
    @IBAction func bookNowBtnTapped(_ sender: UIButton) {
        
    }
    @IBAction func contactBtnTapped(_ sender: Any) {
        if let url = URL(string: "tel://\(ownerContactNo)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension FandBDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == offerCollectionView){
            guard let count = self.fbDetails?.Offers?.count else {return 0}
            return count

        }else{
            guard let count = self.fbDetails?.ImageUrls?.count else {return 0}
            return count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == menuCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MenuCollectionViewCell
            if let img = self.fbDetails?.ImageUrls?[indexPath.row] {
                cell.imgView.sd_setImage(with: URL.init(string: img.thumbnail ?? ""), placeholderImage:  #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.continueInBackground, completed: nil)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifierOffer, for: indexPath) as! OffersCollectionViewCell
            if let img = self.fbDetails?.Offers?[indexPath.row] {
                cell.offerImageView.sd_setImage(with: URL.init(string: img.BannerImage ?? ""), placeholderImage:  #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.continueInBackground, completed: nil)
            }
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == menuCollectionView{
            let controller = LightboxController(images: imageList, startIndex: indexPath.row)
            controller.pageDelegate = self
            controller.dismissalDelegate = self
            controller.dynamicBackground = true
            present(controller, animated: true, completion: nil)
        }else{
            if let info = self.fbDetails?.Offers?[indexPath.row]{
                let offerDetailsVC = Storyboard.offerDetailVC
                offerDetailsVC.offerID = info.OfferId
                self.navigationController?.pushViewController(offerDetailsVC, animated: true)
            }
        }
    }
}
extension FandBDetailsVC : LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}


extension FandBDetailsVC {
    func showMenuCard() {
        if let menuCardURL = fbDetails?.MenuCard {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    private func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Menu Card"
        navigationController?.pushViewController(controller, animated: true)
    }
}
