//
//  ReusableCell.swift
//  globestamp-ios
//
//  Created by Nuno Gonçalves on 23/04/16.
//  Copyright © 2016 globestamp. All rights reserved.
//

public protocol ReusableCell: class {
    static var reuseIdentifier: String { get }
}

public protocol NibReusable: ReusableCell {
    static var nib: UINib { get }
}

public extension ReusableCell {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
public extension NibReusable {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}

public extension UITableView {
    
    func registerReusableCell<T: UITableViewCell>(_ cellType: T.Type) where T: NibReusable {
        self.register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
   

    func registerReusableCellClass<T: UITableViewCell>(_ cellType: T.Type) where T: ReusableCell {
        self.register(cellType, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCellFor<T: UITableViewCell>(_ indexPath: IndexPath) -> T where T: ReusableCell {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    //kavi
    func registerReusableHeaderFooter<T: UITableViewHeaderFooterView>(_ headerFooterType: T.Type) where T: NibReusable {
        self.register(T.nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T where T: ReusableCell {
        return self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as! T
    }
}

public extension UICollectionView {
    
    func registerReusableCell<T: UICollectionViewCell>(_ cellType: T.Type) where T: NibReusable {
        register(T.nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func registerReusableCellClass<T: UICollectionViewCell>(_ cellType: T.Type) where T: ReusableCell {
        register(cellType, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCellFor<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T where T: ReusableCell {
        return dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }

}
