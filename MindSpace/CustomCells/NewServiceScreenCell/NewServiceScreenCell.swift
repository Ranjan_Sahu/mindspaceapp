//
//  NewServiceScreenCell.swift
//  MindSpace
//
//  Created by webwerks on 10/22/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class NewServiceScreenCell: UITableViewCell {

    @IBOutlet weak var eventBannerImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
