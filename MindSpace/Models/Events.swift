//
//  Events.swift
//  MindSpace
//
//  Created by Webwerks on 10/31/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct  EventData : Codable {
    var Status : Bool?
    var Message : String?
    var DataList : [EventList]?
    var BannerList : [BannerListing]?
    var Data : EventDetails?
    var TotalRecords : Int?
}

struct  EventList : Codable {
    let EventNewsId : Int?
    let EventNewsType : Int?
    let EventNewsTypeName : String?
    let EventNewsTitle : String?
    let EventNewsDesc : String?
    let LastRegisterDate : String?
    let EventLocation : String?
    let EventTime : String?
    let FromDate : String?
    let ToDate : String?
    let BannerImage : String?
    let RSVP : Bool?
    let CreatedOn : String?
    let ImageURLs : [String]?
}

class EventDetails : Codable{
    var EventNewsId : Int?
    var EventNewsType : Int?
    var EventNewsTypeName : String?
    var EventNewsTitle : String?
    var EventNewsDesc : String?
    var EventLocation : String?
    var EventTime : String?
    var BannerImage : String?
    var RSVP : Bool?
    var RSVPData: RSVP?
    var FromDate : String?
    var ToDate : String?
    var ImageURLs : [ImageURLS]?
}

struct ImageURLS : Codable {
    var ImageName : String?
    var thumbnail : String?
}

struct PollData: Codable {
    var Status: Bool?
    var Message: String?
    var DataList: [PollList]?
}
struct PollList: Codable {
    var PollId: Int = 0
    var PollTitle: String = ""
    var PollQuestion: String = ""
    var ComplexId: Int = 0
    var AnswerGiven: Bool?
    var PollAnswerId: Int = 0
    var AnswerList: [PollAnswerList]?
}
struct PollAnswerList: Codable {
    var PollAnswerId: Int = 0
    var AnswerText: String = ""
    var Result: String = ""
    var UserId: Int = 0
}

struct SetPoll: Codable {
    var Data: PollList?
    var Status: Bool?
    var Message: String = ""
}

