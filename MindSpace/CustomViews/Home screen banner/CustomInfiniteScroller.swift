//
//  CustomInfiniteScroller.swift
//  InfiniteScrollView
//
//  Created by Pervacio on 12/18/18.
//  Copyright © 2018 Pervacio. All rights reserved.
//

import UIKit

protocol CustomBannerDelegate {
    
    func didSelectBanner(bannerId:Int)
}

class CustomInfiniteScroller: UIView {
    
    @IBOutlet weak var containerView:UIView?
    @IBOutlet weak var infiniteScroller:UICollectionView?
    @IBOutlet weak var pageControl:UIPageControl?
    var delegate:CustomBannerDelegate?
    var bannerImageList = [BannerList]()
    var slideTimer:Timer?

    func setupBannerView(bannerArray:[BannerList],delegate:UIViewController){
       
        bannerImageList = bannerArray
        if(bannerArray.count == 1){
            bannerImageList.append(bannerArray[0])
            bannerImageList.append(bannerArray[0])
        }else{
            bannerImageList.insert(bannerArray[bannerImageList.count-1], at: 0)
            bannerImageList.append(bannerArray[0])
        }
        self.delegate = delegate as? CustomBannerDelegate
        infiniteScroller?.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        infiniteScroller?.dataSource = self
        infiniteScroller?.delegate = self
        pageControl?.currentPage = 0
        infiniteScroller?.decelerationRate = UIScrollViewDecelerationRateFast
        infiniteScroller?.contentSize = CGSize(width: UIScreen.main.bounds.size.width * CGFloat(bannerImageList.count), height: UIScreen.main.bounds.width * (9/16))
        infiniteScroller?.contentOffset = CGPoint(x: UIScreen.main.bounds.size.width, y: 0)

        if(bannerImageList.count > 0){
            infiniteScroller?.reloadData()
        }
    }
    
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        //get cell size
        let cellSize =   CGSize(width: frame.width, height: frame.height)//CGSizeMake(frame.width, frame.height);
        
        //get current content Offset of the Collection view
        let contentOffset = infiniteScroller?.contentOffset
        
        let xPosition = (contentOffset?.x)! + cellSize.width
        //scroll to next cell
        infiniteScroller?.scrollRectToVisible(CGRect(x: xPosition, y: contentOffset?.y ?? 0, width: cellSize.width, height:  cellSize.height), animated: true)
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        slideTimer = Timer.scheduledTimer(timeInterval: TimeInterval(slideTimeInterveal), target: self, selector: #selector(CustomInfiniteScroller.scrollToNextCell), userInfo: nil, repeats: true);
    }
    
    func stopTimer(){
        if((slideTimer) != nil){
            slideTimer?.invalidate()
        }
    }
    
}

extension CustomInfiniteScroller:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl?.numberOfPages = bannerImageList.count - 2
        return bannerImageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let bannerData = bannerImageList[indexPath.row] as BannerList
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        let imgStr = bannerData.BannerImage
        cell.imageView.sd_setImage(with: URL(string: imgStr!), placeholderImage: UIImage(named: "bannerPlaceholder"))
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     //   let height = UIScreen.main.bounds.width * (9/16)
    //  return CGSize(width: UIScreen.main.bounds.width, height: height+10)
         return  collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    // ScrollViewDelegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewWillBeginDecelerating(infiniteScroller!)
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let xPos = scrollView.contentOffset.x
        let pageWidth: CGFloat = scrollView.frame.size.width
        if xPos > 0 {
            // When scroll to the last image
            if xPos >=  (pageWidth * (CGFloat(bannerImageList.count - 1))) {
                // set offset to 2nd image
                scrollView.contentOffset = CGPoint(x: pageWidth, y: 0)
            }
        } else if xPos <= 0 {
            // Swipe right
            scrollView.contentOffset = CGPoint(x: pageWidth * (CGFloat(bannerImageList.count - 2)) , y: 0)
        }
        pageControl!.currentPage = (Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)) % bannerImageList.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected item \(indexPath.item)")
        self.delegate?.didSelectBanner(bannerId: indexPath.item)
    }
}


