//
//  VisitorRequest.swift
//  MindSpace
//
//  Created by webwerks on 21/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct  ComplexData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [ComplexList]?
    let TotalRecords : Int?
}

struct  ComplexList : Codable {
    let ComplexId : Int?
    let ComplexName : String?
}

struct  BuildingList: Codable {
    let BuildingId : Int?
    let BuildingName: String?
}

struct  LocationData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [LocationList]?
    let TotalRecords : Int?
}

struct  LocationList : Codable {
    let LocationId : Int?
    let LocationName : String?
}

class ComplexAndDepartmentData : Codable {
    var Status : Bool?
    var Message : String?
    var Data : ComplexAndDepartmentDetails?
}
class ComplexAndDepartmentDetails : Codable {
    var ComplexList : [ComplexList]?
    var DepartmentList : [DepartmentList]?
}

class BuildingData: Codable {
    var DataList: [BuildingList]?
    var Status: Bool?
    var Message: String?
}

class DepartmentData : Codable {
    var Status : Bool?
    var DataList : [DepartmentList]?
}
class  DepartmentList : Codable {
    var DepartmentId : Int?
    var DepartmentName : String?
}

class MeetingPersonData : Codable {
    var Status : Bool?
    var Message : String?
    var DataList : [MeetingPersonList]?
}

class  MeetingPersonList : Codable {
    var Id : Int?
    var Name : String?
}

class VisitorRequestDataRes: Codable {
    var Status : Bool?
    var Message : String?
//    var Data : [MeetingPersonList]?
}

class TenantData : Codable {
    var Status : Bool?
    var DataList : [Tenant]?
}

class Tenant : Codable {
    var TenantId : Int?
    var TenantName : String?
}

struct  VisitorRequestData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [VisitorRequestList]?
    let TotalRecords : Int?
}

struct  VisitorRequestList : Codable {
    let VisitorRequestId : Int?
    let VisitorRequestNumber : String?
    let StartTime : String?
    let ComplexName : String?
    let TenantName : String?
    let NumberOfVisitor : Int?
    let QRCode : String?
}
