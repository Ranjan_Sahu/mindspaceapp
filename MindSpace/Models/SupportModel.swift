//
//  SupportModel.swift
//  MindSpace
//
//  Created by webwerks on 12/18/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

struct SupportModel: Codable {
    var DateList: [SupportDataListModel]?
    var TotalRecords: Int = 0
    var Status: Bool = false
    var Message: String?
}
struct SupportDataListModel: Codable {
    var TicketType: String?
    var Subject: String?
    var Priority: String?
    var Status: String?
    var CreatedOn: String?
    var ComplaintTicketsId : Int?
}

struct TicketTypeListModel: Codable {
    var DateList: [TicketTypeListData]?
    var Status: Bool?
    var Message: String?
}

struct TicketTypeListData: Codable {
    var ComplaintTypeId: Int = 0
    var ComplaintTypeName: String?
}
struct RaisedTicket: Codable {
    var Status: Bool = false
    var Message: String?
}
