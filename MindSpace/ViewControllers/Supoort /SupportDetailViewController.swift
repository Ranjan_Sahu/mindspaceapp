//
//  SupportDetailViewController.swift
//  MindSpace
//
//  Created by webwerks on 12/18/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

enum TextFieldTitle: String {
    case ticketType = "Ticket Type"
    case subject = "Subject"
    case description = "Description"
    case priority = "Priority"
}
struct TicketData {
    var ticketType: String = ""
    var ticketTypeID: String = ""
    var description: String?
    var priority: String = ""
    var priorityID: String = ""
}

class SupportDetailViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var tickectTypeTextField: RoundTextField!
    @IBOutlet weak var enterSubjectTextField: RoundTextField!
    @IBOutlet weak var descrptionTextView: UITextView!
    @IBOutlet weak var selectPriority: RoundTextField!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var imageView: UIImageView!
    
    var ticketList = [TicketTypeListData]()
    var activeTextField: UITextField?
    let priorityArray = ["Low","Medium","High"]
    let priorityDict = [["1":"Low"],["2":"Medium"],["3":"High"]]
    var ticketData = TicketData()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        title = "Raise A New Ticket"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setUIData()
        getTicketTypeListData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    func setUIData() {
        if ticketData.description == nil{
            setTextView()
        }
        descrptionTextView.layer.borderWidth = 1
        descrptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        pickerView.dataSource = self
        pickerView.delegate = self
        tickectTypeTextField.delegate = self
        selectPriority.delegate = self
        descrptionTextView.delegate = self
        imagePicker.delegate = self
    }
    func getTicketTypeListData() {
        
        let parameters = ["deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.ticketTypeListData, objectType: TicketTypeListModel.self, isHud: true, controller: self, success: { [weak self] listingData in
            if let tcktList = listingData.DateList {
                self?.ticketList = tcktList
            }
        })
    }
    
    @IBAction func onClickDoneButton(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func onClickUploadFile(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera(sender: sender as! UIButton)
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery(sender: sender as! UIButton)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmitButton(_ sender: Any) {
        validation()
    }
}
extension SupportDetailViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView?.autoresizingMask = .flexibleHeight
        activeTextField = textField
        if textField == tickectTypeTextField || textField == selectPriority {
            textField.inputView = pickerView
            pickerView?.reloadAllComponents()
        }
        textField.inputAccessoryView = toolBar
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case tickectTypeTextField:
            if let pickView = pickerView {
                selectRow(row: pickView.selectedRow(inComponent: 0))
            }
        case selectPriority:
            if let pickView = pickerView {
                selectRow(row: pickView.selectedRow(inComponent: 0))
            }
        default:
            break
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
extension SupportDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if activeTextField == tickectTypeTextField {
            return ticketList.count
        }
        return priorityArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if activeTextField == tickectTypeTextField {
            return ticketList[row].ComplaintTypeName ?? ""
        }
        return priorityArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
}
extension SupportDetailViewController {
    func selectRow(row : Int) {
        if activeTextField == tickectTypeTextField {
            if row < ticketList.count {
                activeTextField?.text = ticketList[row].ComplaintTypeName
                ticketData.ticketType =  ticketList[row].ComplaintTypeName!
                ticketData.ticketTypeID = String(ticketList[row].ComplaintTypeId)
            }else {
                return
            }
        }
        if activeTextField == selectPriority{
            let value = priorityDict[row]
            selectPriority?.text = value.values.first
            ticketData.priority = value.values.first ?? ""
            ticketData.priorityID = value.keys.first ?? ""
        }
    }
}
extension SupportDetailViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descrptionTextView.text == "    Enter Discription" {
            textView.text = ""
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if descrptionTextView.text.count <= 0 {
            setTextView()
        } else {
            ticketData.description = descrptionTextView.text
        }
    }
}
extension SupportDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera(sender:UIButton) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType    = .camera
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            present(imagePicker, animated: true, completion: nil)
        }else {
            CommonHelper.showAlert(vc: self, title:"WARNING", msg: "Can not access Camera !")
        }
    }
    
    func openGallery(sender:UIButton) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)){
            self.imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType    = .photoLibrary
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(imagePicker, animated: true, completion: nil)
        }else {
            CommonHelper.showAlert(vc: self, title:"WARNING", msg: "Can not access Gallery !")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if imageView.image != nil {
            imageView.image = nil
        }
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = chosenImage
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
extension SupportDetailViewController {
    
    func validation() {
        var descriptn = ""
        if descrptionTextView.text! == "    Enter Discription"{
            descriptn = ""
        } else {
            descriptn = descrptionTextView.text!
        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: tickectTypeTextField.text!, controller: self,fieldName :TextFieldTitle.ticketType.rawValue)){
            return
        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: enterSubjectTextField.text!, controller: self,fieldName :TextFieldTitle.subject.rawValue)){
            return
        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val:descriptn, controller: self,fieldName :TextFieldTitle.description.rawValue)){
            return
        }
        if(!CommonHelper.validateField(type: FieldTypes.Normal, val: selectPriority.text!, controller: self,fieldName :TextFieldTitle.priority.rawValue)){
            return
        }
        let userId =  AppDelegate.appDelegateShared.user?.UserId ?? 0
        
        let dict = ["UserId":"\(userId)","CategoryId": ticketData.ticketTypeID, "Subject": enterSubjectTextField.text!, "Description": descriptn, "TenantPriorityId": ticketData.priorityID, "UserComments":""]
        var imageDict = [String: UIImage]()
        if imageView.image != nil {
            imageDict["attachment"] = imageView.image
        }
        print(dict)
        Webservice.uploadFiles(imageArray: imageDict, parameters: dict, url: WebserviceURLs.raiseTicket, objectType: RaisedTicket.self, controller: self, success: { response in
            print(response)
            Constants.showAlertWithMessage(message: response.Message ?? "", presentingVC: self, okBlock: { _ in
                if response.Status == true{
                    self.navigationController?.popViewController(animated: true)
                }
            })
        })
    }
    
    func emptyUI(){
        tickectTypeTextField.text = ""
        enterSubjectTextField.text = ""
        descrptionTextView.text = ""
        selectPriority.text = ""
        ticketData.ticketType = ""
        ticketData.ticketTypeID = ""
        ticketData.description = ""
        ticketData.priority = ""
        ticketData.priorityID = ""
        setTextView()
        imageView.image = nil
    }
    
    func setTextView() {
        descrptionTextView.text = "    Enter Discription"
        descrptionTextView.textColor = .lightGray
    }
}
