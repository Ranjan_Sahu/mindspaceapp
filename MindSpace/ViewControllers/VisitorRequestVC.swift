//
//  VisitorRequestVC.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
struct VisitorRequestCommon {
    var department : String = ""
    var departmentId : String = ""
    var complex : String = ""   
    var complexId : String = ""
    var building: String = ""
    var buildingId: String = ""
    var meetingPerson : String = ""
    var meetingPersonId : String = ""
    var meetingStartTime :String = ""
    var meetingEndTime :String = ""
    var meetingDate :String = ""
    var isMeetingAdhoc : Bool = true
}
class VisitorRequestVC: UIViewController {
    let cellIdentifier = "VisitorRequestCommonContentCell"
    let cellIdentifier1 = "VisitorRequestCell"
    var count = 1
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    // MARK:- data
    var visitorList : [VisitorRequest] = [VisitorRequest()]
    var commonFields = VisitorRequestCommon()
    var complexNDepartmentList : ComplexAndDepartmentDetails?
    var meetingPersonList : [MeetingPersonList] = []
    var buildingList: [BuildingList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Visitor Request"
        //tabBarController?.tabBar.isHidden = true
        self.tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil) , forCellReuseIdentifier: cellIdentifier)
        self.tableView.register(UINib.init(nibName: cellIdentifier1, bundle: nil) , forCellReuseIdentifier: cellIdentifier1)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 420
        tableView.keyboardDismissMode = .interactive
        getComplexNDepartmentList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func addNewVisitorBtnClicked(_ sender: Any) {
        visitorList.append(VisitorRequest())
        count = count + 1
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: count - 1, section: 1)], with: .bottom)
        tableView.endUpdates()
    }
    @IBAction func submitBtnClick(_ sender: Any) {
        view.endEditing(true)
        if validate() {
            submitRequest()
        }
        
    }
    @IBAction func resignPicker(_ sender: Any) {
        view.endEditing(true)
    }
    func validate() -> Bool {
        //common fields
        //complex
        guard commonFields.complexId.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select complex.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        // Building
        guard commonFields.buildingId.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select building.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        //Depertment
        guard commonFields.departmentId.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select department.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        //meetingperson
        guard commonFields.meetingPersonId.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select meeting person.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        //date
        guard commonFields.meetingDate.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select date.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        //start time
        guard commonFields.meetingStartTime.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select start time.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        //end time
        guard commonFields.meetingEndTime.isNotEmpty() else{
            Constants.showAlertWithMessage(message: "Please select end tim.", presentingVC: self, okBlock: { (str) in })
            return false
        }
        
        for visitor in visitorList {
            //first name
            guard visitor.firstName.isNotEmpty() else{
                Constants.showAlertWithMessage(message: "Please enter first name.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            //last name
            guard visitor.lastName.isNotEmpty() else{
                Constants.showAlertWithMessage(message: "Please enter last name.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            //email
            guard visitor.emailId.isNotEmpty(), visitor.emailId.isValidEmail() else{
                Constants.showAlertWithMessage(message: "Please enter valid email id.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            //phone number
            guard visitor.phoneNo.isNotEmpty(), visitor.phoneNo.isValidContact() else{
                Constants.showAlertWithMessage(message: "Please enter valid mobile number.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            //vehicle
            if visitor.isVehicle == true && !visitor.vehicleNumber.isNotEmpty() {
                Constants.showAlertWithMessage(message: "Please enter vehicle number.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            //laptop
            if visitor.isLaptop == true, !visitor.laptopModelNumber.isNotEmpty(){
                Constants.showAlertWithMessage(message: "Please enter laptop model number.", presentingVC: self, okBlock: { (str) in })
                return false
            }
            
        }
        
        return true
        
    }
}
// MARK: - TableView datsource and delegates
extension VisitorRequestVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            var cell: VisitorRequestCommonContentCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? VisitorRequestCommonContentCell
            if cell == nil {
                tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? VisitorRequestCommonContentCell
            }
            cell.departmentTxtField.text = commonFields.department
            cell.complexTxtField.text = commonFields.complex
            cell.buildingTextfield.text = commonFields.building
            cell.meetingTxtField.text = commonFields.meetingPerson
            cell.dateTxtField.text = commonFields.meetingDate
            cell.startTimeTxtField.text = commonFields.meetingStartTime
            cell.endTimeTxtField.text = commonFields.meetingEndTime
            cell.adhocBtn.isSelected = commonFields.isMeetingAdhoc
            cell.scheduledBtn.isSelected = !commonFields.isMeetingAdhoc
            
            cell.delegate = self
            cell.pickerView = pickerView
            cell.toolBar = toolBar
            cell.datePickerView = datePickerView
            cell.complexNDepartmentList = complexNDepartmentList
            cell.buildingList = buildingList
            cell.meetingPersonList = meetingPersonList
            cell.commonFields = commonFields

            return cell
        } else {
            var cell: VisitorRequestCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier1) as? VisitorRequestCell
            if cell == nil {
                tableView.register(UINib(nibName: cellIdentifier1, bundle: nil), forCellReuseIdentifier: cellIdentifier1)
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? VisitorRequestCell
            }
            cell.cellIndex = indexPath.row
            cell.delegate = self
            let visitor = visitorList[indexPath.row]
            cell.firstNameTxtField.text = visitor.firstName
            cell.lastNameField.text = visitor.lastName
            cell.emailIdTxtField.text = visitor.emailId
            cell.mobileNumberTxtField.text = visitor.phoneNo
            cell.vehicleNoTxtFld.text = visitor.vehicleNumber
            cell.laptopModelNoTxtFld.text = visitor.laptopModelNumber
            
            //Laptop
            cell.laptopNoBtn.isSelected = !visitor.isLaptop
            cell.laptopYesBtn.isSelected = visitor.isLaptop
            cell.laptopDetailView.isHidden = !visitor.isLaptop
            
            //vehical
            cell.vehicleNoBtn.isSelected = !visitor.isVehicle
            cell.vehicleYesBtn.isSelected = visitor.isVehicle
            cell.vehicleDetailView.isHidden = !visitor.isVehicle
            
            //baggage
            cell.baggageNoBtn.isSelected = !visitor.isBaggage
            cell.baggageYesBtn.isSelected = visitor.isBaggage
            return cell
        }
    }
    
}
//MARK: - Custom Delegates
extension VisitorRequestVC : VisitorRequestCommonCellDelegate,VisitorRequestCellDelegate{
    func visitorDataChanged(index: Int, visitorDetails: VisitorRequest) {
        visitorList[index] = visitorDetails
    }
    func visitorDataChanged(common:VisitorRequestCommon) {
        commonFields = common
    }
    func hideVehicalDetail(cell : VisitorRequestCell, hide : Bool){
        cell.vehicleDetailView.isHidden = hide
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    func hideLaptopDetail(cell : VisitorRequestCell, hide : Bool) {
        cell.laptopDetailView.isHidden = hide
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    func departmentChanged(selectedDepartment: DepartmentList) {
        getMeetingPersonList(departmentId: selectedDepartment.DepartmentId!)
    }
    func complexChanged(complex: ComplexList) {
        self.meetingPersonList.removeAll()
        self.tableView.reloadSections([0], with: .none)
        getBuildingList(complexId: complex.ComplexId)
    }
}
// MARK: - Webservices
extension VisitorRequestVC {
    func getComplexNDepartmentList() {
        let parameters = ["deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetComplexNDepartmentList, objectType: ComplexAndDepartmentData.self, isHud: true, controller: self, success: {
            response in
            guard let list = response.Data else {return}
            self.complexNDepartmentList = list
            self.tableView.reloadSections([0], with: .none)
        })
    }
    
    func getBuildingList(complexId: Int?) {
        guard let selectedComplexId = complexId else { return }
        let parameters = ["pageInfo":["PageIndex":"1","PageSize":"10"],
                          "entity":["ComplexId": selectedComplexId],"deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getBuildingList, objectType: BuildingData.self, isHud: true, controller: self, success: {
            response in
            guard let list = response.DataList else {return}
            self.buildingList = list
            self.tableView.reloadSections([0], with: .none)
        })
    }
    
    func getMeetingPersonList(departmentId : Int) {
        let parameters = ["entity":["DepartmentId":departmentId],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetMeetingPersonList, objectType: MeetingPersonData.self, isHud: true, controller: self, success: {
            response in
            guard let list = response.DataList else {return}
            self.meetingPersonList = list
            self.tableView.reloadSections([0], with: .none)
        })
    }
    func submitRequest() {
        var type = "0"
        if commonFields.isMeetingAdhoc {
            type = "1"
        }
        let param = ["DepartmentId" : commonFields.departmentId,
                     "ComplexId" : commonFields.complexId,
                     "BuildingId": commonFields.buildingId,
                     "MeetingTypeId" : type,
                     "MeetingPersonId" : commonFields.meetingPersonId,
                     "MeetingPerson": commonFields.meetingPerson,
                     "Date" : commonFields.meetingDate,
                     "StartTime" : commonFields.meetingStartTime,
                     "EndTime" : commonFields.meetingEndTime,
                     "VisitorList" : visitorList.map({ $0.getDictionary()}) ] as [String : Any]
        let parameters = ["entity":param,
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_AddVisitorRequest, objectType: VisitorRequestDataRes.self, isHud: true, controller: self, success: {
            response in
            Constants.showAlertWithMessage(message: response.Message ?? "", presentingVC: self, okBlock: { _ in
                if response.Status == true{
                    self.navigationController?.popViewController(animated: true)
                }
            })
            
        })
    }
}

