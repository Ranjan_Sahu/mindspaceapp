//
//  SettingScreenVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class SettingScreenVC: UIViewController {
// this controller belongs to Visitor module ssettings
    // Constants
    
    // Outlets
    @IBOutlet weak var BTN_aboutUs: UIButton!
    @IBOutlet weak var BTN_PrivacyPolicy: UIButton!
    @IBOutlet weak var view_PrivacyPolicy: UIView!
    @IBOutlet weak var view_AboutUs: UIView!
    @IBOutlet weak var view_Logout: UIView!
    @IBOutlet weak var LBL_Version: UILabel!
    @IBOutlet weak var webViewPP: UIWebView!
    
    // Objects
    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ppText = UserDefaults.standard.value(forKey: "PrivacyPolicy_String")
        self.webViewPP.loadHTMLString(ppText as! String, baseURL: nil)
        
        self.view_AboutUs.isHidden = false
        self.BTN_aboutUs.isSelected = true
        self.view_PrivacyPolicy.isHidden = true
        self.BTN_PrivacyPolicy.isSelected = false
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        if let currentUser = appDelegate.user, currentUser.UserType == "Visitor" {
            view_Logout.isHidden = true
        }else {
            view_Logout.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addLeftMenuButton()
        self.navigationController?.navigationBar.isHidden = false
       self.title = "Settings"
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        self.LBL_Version.text = ":    " + appVersion!

    }
    
    // MARK:- IBActions
    @IBAction func bellAction(_ sender: UIButton) {
    }
    
    @IBAction func aboutUsAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.BTN_PrivacyPolicy.isSelected = false
        self.view_AboutUs.isHidden = !sender.isSelected
        self.view_PrivacyPolicy.isHidden = sender.isSelected
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.BTN_aboutUs.isSelected = false
        self.view_AboutUs.isHidden = sender.isSelected
        self.view_PrivacyPolicy.isHidden = !sender.isSelected
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        User.clearData()
        let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
        AppDelegate.appDelegateShared.user = nil
        let navController = UINavigationController.init(rootViewController: targetVC)
        //self.sideMenuViewController?.navigationController?.pushViewController(targetVC, animated: true)
        //self.sideMenuViewController?.navigationController?.viewControllers = [targetVC]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navController
        
        /*let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
         self.tabBarController?.navigationController?.pushViewController(targetVC, animated: true)
         self.tabBarController?.navigationController?.viewControllers = [targetVC]*/
    }
    
}
