//
//  EAPModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

class  EAPDetailsData : Codable {
    var Statue : Bool?
    var Message : String?
    var Data : EAPDetails?
}

class  EAPDetails : Codable {
    var EntertainmentPlayGroundId : Int?
    var Title : String?
    var Description : String?
    var ContactDetails : String?
    var BookingProcessDocument : String?
    var IsActive : Bool?
    var ComplexId : Int?
    var ComplexName : String?
    var ImageUrls : [String]?
    var Capacity : String?
    var Time : String?
}
