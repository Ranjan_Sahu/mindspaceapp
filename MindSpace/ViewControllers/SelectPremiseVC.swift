//
//  SelectPremiseVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class SelectPremiseVC: UIViewController,FieldSelectionprotocol  {
    
    //Enums
    enum ServiceNames {
        case Get_Location
        case Get_Premises
    }
    
    // Constants
    
    // Outlets
    @IBOutlet weak var BTN_Submit: UIButton!
    @IBOutlet weak var TF_Location: DropDown!
    @IBOutlet weak var TF_Premise: DropDown!
    
    // Objects
    var complexList : [ComplexList]? = nil
    var locationsList : [LocationList]? = nil
    var selectedLocation : LocationList? = nil
    var selectedComplex : ComplexList? = nil
    var titleArray = [String]()
    var sName:ServiceNames? = nil
    
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TF_Premise.isEnabled = false
        self.getLocationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        TF_Premise.fieldDelegate = self
        TF_Location.fieldDelegate = self
        self.setupTF()
    }
    func didSelectVal(_ selectedText: String, _ index: Int, _ field:UITextField)  {
        //self.refreshData()
        if (field == self.TF_Location) {
            self.selectedLocation = self.locationsList?[index]
            self.TF_Premise.text = ""
            self.getComplexList()
        }else if (field == self.TF_Premise) {
            self.selectedComplex = self.complexList?[index]
            //self.BTN_Submit.isEnabled = true
        }
    }
    
    func didSelectField(selectedFiled: DropDown) {
        
        if(selectedFiled ==  TF_Location){
            if(TF_Premise.isSelected){
                TF_Premise.hideList()
                TF_Premise.isSelected = false
            }
        }else{
            if(TF_Location.isSelected){
                TF_Location.hideList()
                TF_Location.isSelected = false
                
            }
        }
    }
}
// MARK:- IBActions
extension SelectPremiseVC {
    @IBAction func submitAction(_ sender: Any) {
        
        if !((self.TF_Location.text?.count)!>0) {
            CommonHelper.showAlert(vc: self, title: "Alert", msg: "Please Select Location.")
            return
        }else if !((self.TF_Premise.text?.count)!>0) {
            CommonHelper.showAlert(vc: self, title: "Alert", msg: "Please Select Complex.")
            return
        }else {
            UserDefaults.standard.set(self.selectedLocation?.LocationId, forKey: "Selected_Location_ID")
            UserDefaults.standard.set(self.selectedComplex?.ComplexId, forKey: "Selected_Complex_ID")
            UserDefaults.standard.synchronize()
            self.navigationController?.pushViewController(Storyboard.loginScreenVC, animated: true)
        }
    }
    
}
// MARK:- Service Calls
extension SelectPremiseVC {
    
    private func getComplexList() {
        
        sName = ServiceNames.Get_Premises
        let parameters = ["pageInfo"    : ["PageIndex" : "2", "PageSize":"3"],
                          "entity"      : ["LocationId": selectedLocation?.LocationId! ?? 0],
                          "deviceInfo"  : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetComplexList, objectType: ComplexData.self, isHud: true, controller: self, success: {[weak self] premises in
            self?.complexList = premises.DataList
            self?.titleArray.removeAll()
            guard let complexList = self?.complexList else {
                self?.refreshData()
                return
            }
            self?.titleArray = complexList.map({ $0.ComplexName! })
            self?.refreshData()
        })
    }
    
    private func getLocationList() {
        
        let parameter = ["deviceInfo" : CommonHelper.getDeviceInfo()]
        sName = ServiceNames.Get_Location
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetLocationList, objectType: LocationData.self, isHud: true, controller: self, success: { [weak self] locations in
            
            self?.locationsList = locations.DataList
            guard let locationList = self?.locationsList else {
                self?.refreshData()
                return
            }
            self?.titleArray.removeAll()
            self?.titleArray = locationList.map({ $0.LocationName! })
            self?.refreshData()
        })
    }
}
// MARK: - Custom Methods
extension SelectPremiseVC {
    private func setupTF(){
        let paddingView1: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 64, height: 20))
        self.TF_Location.leftView = paddingView1
        self.TF_Location.leftViewMode = .always
        let paddingView2: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 64, height: 20))
        self.TF_Premise.leftView = paddingView2
        self.TF_Premise.leftViewMode = .always
        self.TF_Location.attributedPlaceholder = NSAttributedString(string: "Select Location",
                                                                    attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.TF_Premise.attributedPlaceholder = NSAttributedString(string: "Select Premise",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        self.TF_Location.parentController = self
        self.TF_Premise.parentController = self
    }
    
    
    private func refreshData() {
        
        if (sName == ServiceNames.Get_Location) {
            self.TF_Location.optionArray = titleArray
        }else if (sName == ServiceNames.Get_Premises) {
            self.TF_Premise.optionArray = titleArray
            self.TF_Premise.isEnabled = true
        }
    }
    
}


