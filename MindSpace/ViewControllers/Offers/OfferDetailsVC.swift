//
//  OfferDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 01/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class OfferDetailsVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var expiryLbl: UILabel!
    @IBOutlet weak var conditionLbl: UILabel!
    @IBOutlet weak var offerImg: UIImageView!
    private var offersData : OfferDetails?
    var offerID : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Offer Details"
        self.offerImg.layer.cornerRadius = 4.0
        
        callWebService()
    }
}
extension OfferDetailsVC {
    //MARK:- Webservices
    private func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["OfferId" : "\(offerID ?? 0)"],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getOfferDetails, objectType: OfferDetailData.self, isHud: true, controller: self, success: {
            response in
            self.offersData = response.Data
            self.updateUI()
        })
    }
    //MARK:- Custom Methods
    private func updateUI()  {
        titleLbl.text = offersData?.OfferTitle
        descriptionLbl.text = offersData?.OfferDesc
        expiryLbl.text = "Valid till \(offersData?.ExpiryDate ?? "")"
         if (self.offersData?.BannerImage) != nil {
            self.offerImg.sd_setImage(with: URL.init(string: self.offersData?.BannerImage ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
        }
    }
}
