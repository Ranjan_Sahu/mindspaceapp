//
//  UploadKYC2TableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 1/17/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit

class UploadKYC2TableViewCell: UITableViewCell {

    @IBOutlet weak var LBL_depatrment: UILabel!
    @IBOutlet weak var LBL_tenantName: UILabel!
    @IBOutlet weak var LBL_meetingPerson: UILabel!
    @IBOutlet weak var LBL_meetingType: UILabel!
    @IBOutlet weak var LBL_visitorMobileNymber: UILabel!
    @IBOutlet weak var LBL_firstName: UILabel!
    @IBOutlet weak var LBL_meetingTime: UILabel!
    @IBOutlet weak var LBL_emailId: UILabel!
    @IBOutlet weak var LBL_baggage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
