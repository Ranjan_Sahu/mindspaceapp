//
//  WebViewVC.swift
//  MindSpace
//
//  Created by webwerks on 24/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {
    
    // Constants
    
    // Outlets
    @IBOutlet weak var BTN_Agree: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var webView: UIWebView!
    var isFromSideMenu = false
    var htmlString = ""
    
    // Objects
    var sysDataInfo : SystemDataInfo? = nil
    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        addLeftMenuButton()
        self.navigationController?.navigationBar.isHidden = false
        self.addShadowInView(vw: self.shadowView)
        
        var webTxt = ""
        if (htmlString == "About Us") {
            webTxt = UserDefaults.standard.string(forKey: "AboutUs_String")!
        }else {
            webTxt = UserDefaults.standard.string(forKey: "Terms_String")!
        }

        self.webView.loadHTMLString(webTxt, baseURL: nil)
    }
    
    // IBActions
    @IBAction func agreeAction(_ sender: Any) {
        let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
    
    // MARK:- Custom methods
    func addShadowInView(vw:UIView) {
        vw.layer.masksToBounds = false
        let sColor = Constants.AppColor.COLOR_Shadow
        vw.layer.shadowColor = sColor.cgColor
        vw.layer.shadowOpacity = 0.3
        vw.layer.shadowOffset = CGSize.zero
        vw.layer.shadowRadius = 8
    }
}

