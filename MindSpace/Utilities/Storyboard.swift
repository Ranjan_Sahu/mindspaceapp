//
//  Storyboard.swift
//  MindSpace
//
//  Created by Webwerks on 10/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct Storyboard {
    static var eventDetailsVC : EventDetailsVC {
        return UIStoryboard.advance.instantiate(EventDetailsVC.self)
    }
    static var ruEventsVC : RUEventsVC {
        return UIStoryboard.advance.instantiate(RUEventsVC.self)
    }
    static var ruGalleryVC : RUGalleryVC {
        return UIStoryboard.advance.instantiate(RUGalleryVC.self)
    }
    static var ruFAndBLocationsVC : RUFAndBLocationsVC {
        return UIStoryboard.advance.instantiate(RUFAndBLocationsVC.self)
    }
    static var ruHelpDeskVC : RUHelpDeskVC {
        return UIStoryboard.advance.instantiate(RUHelpDeskVC.self)
    }
    static var ruMarketPlaceVC : RUMarketPlaceVC {
        return UIStoryboard.advance.instantiate(RUMarketPlaceVC.self)
    }
    static var galleryDetailsVC : GalleryDetailsVC {
        return UIStoryboard.advance.instantiate(GalleryDetailsVC.self)
    }
    static var ruFAQVC : RUFAQVC {
        return UIStoryboard.advance.instantiate(RUFAQVC.self)
    }
    static var supportVC : SupporListViewController {
        return UIStoryboard.advance.instantiate(SupporListViewController.self)
    }
    static var supportDetailVC : SupportDetailViewController {
        return UIStoryboard.advance.instantiate(SupportDetailViewController.self)
    }
    static var fandBDetailsVC : FandBDetailsVC {
        return UIStoryboard.advance.instantiate(FandBDetailsVC.self)
    }
    static var offerDetailVC : OfferDetailsVC {
        return UIStoryboard.advance.instantiate(OfferDetailsVC.self)
    }
    static var visitorRequestVC : VisitorRequestVC {
        return UIStoryboard.advance.instantiate(VisitorRequestVC.self)
    }
    static var aboutCommonSpacesVC : AboutCommonSpacesVC {
        return UIStoryboard.advance.instantiate(AboutCommonSpacesVC.self)
    }
    static var crecheListVC : CrecheListVC {
        return UIStoryboard.advance.instantiate(CrecheListVC.self)
    }
    static var entertainmentAndPlaygroundListVC : EntertainmentAndPlaygroundListVC {
        return UIStoryboard.advance.instantiate(EntertainmentAndPlaygroundListVC.self)
    }
    static var crecheDetailsViewController : CrecheDetailsViewController {
        return UIStoryboard.advance.instantiate(CrecheDetailsViewController.self)
    }
    static var carParkingViewController : CarParkingViewController {
        return UIStoryboard.advance.instantiate(CarParkingViewController.self)
    }
    static var assemblyPointViewController : AssemblyPointViewController {
        return UIStoryboard.advance.instantiate(AssemblyPointViewController.self)
    }
    static var contactViewController : ContactViewController {
        return UIStoryboard.advance.instantiate(ContactViewController.self)
    }
    static var uploadKYCViewController : UploadKYCViewController {
        return UIStoryboard.advance.instantiate(UploadKYCViewController.self)
    }
    static var announcementDetailsVC : AnnouncementDetailsVC {
        return UIStoryboard.advance.instantiate(AnnouncementDetailsVC.self)
    }
    static var productDetailsVC : ProductDetailsVC {
        return UIStoryboard.advance.instantiate(ProductDetailsVC.self)
    }
    static var viewTicketViewController : ViewTicketViewController{
        return UIStoryboard.advance.instantiate(ViewTicketViewController.self)
    }
    static var upcomingViewController : UpcomingViewController{
        return UIStoryboard.main.instantiate(UpcomingViewController.self)
    }
    static var atmViewController : ATMViewController{
        return UIStoryboard.main.instantiate(ATMViewController.self)
    }
    
    static var ambulanceViewController : AmbulanceViewController{
        return UIStoryboard.main.instantiate(AmbulanceViewController.self)
    }
}
extension Storyboard {
    static var ruGymVC : RUGymVC {
        return UIStoryboard.main.instantiate(RUGymVC.self)
    }
    static var notificationsVC : NotificationsVC {
        return UIStoryboard.main.instantiate(NotificationsVC.self)
    }
    static var newRUServiceVC : NewRUServiceVC {
        return UIStoryboard.main.instantiate(NewRUServiceVC.self)
    }
    static var loginScreenVC : LoginScreenVC {
        return UIStoryboard.main.instantiate(LoginScreenVC.self)
    }
    static var aboutUsVC : AboutUsVC {
        return UIStoryboard.main.instantiate(AboutUsVC.self)
    }
    static var termsConditionsVC : TermsConditionsVC {
        return UIStoryboard.main.instantiate(TermsConditionsVC.self)
    }
    static var ruEmergencyContactsVC : RUEmergencyContactsVC {
        return UIStoryboard.main.instantiate(RUEmergencyContactsVC.self)
    }
    
    static var ruTicketsRaisedVC : RUTicketsRaisedVC {
        return UIStoryboard.main.instantiate(RUTicketsRaisedVC.self)
    }
    static var selectPremiseVC : SelectPremiseVC {
        return UIStoryboard.main.instantiate(SelectPremiseVC.self)
    }
    static var ruAboutCommanSpaceDetailsVC : RUAboutCommanSpaceDetailsVC {
        return UIStoryboard.main.instantiate(RUAboutCommanSpaceDetailsVC.self)
    }
    static var ruConciergeServicesVC : RUConciergeServicesVC {
        return UIStoryboard.main.instantiate(RUConciergeServicesVC.self)
    }
    static var visitDetailsVC : VisitDetailsVC {
        return UIStoryboard.main.instantiate(VisitDetailsVC.self)
    }
    static var ruBookACabVC : RUBookACabVC {
        return UIStoryboard.main.instantiate(RUBookACabVC.self)
    }
    static var qrCodeViewController : QRCodeViewController {
        return UIStoryboard.main.instantiate(QRCodeViewController.self)
    }
    static var registerViewController : RegisterViewController {
        return UIStoryboard.main.instantiate(RegisterViewController.self)
    }
}
