//
//  OffersCategoryCell.swift
//  MindSpace
//
//  Created by webwerks on 9/28/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class OffersCategoryCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var offerSelected : ((_ offerInfo : OffersList) -> ())?
    let cellIdentifier = "Menu"
    var dataSource : [OffersList] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension OffersCategoryCell : UICollectionViewDelegate,UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! MenuCollectionViewCell
        let info = dataSource[indexPath.row]
         if (info.BannerImage) != nil {
           cell.imgView.sd_setImage(with: URL(string: info.BannerImage ?? ""), placeholderImage: UIImage(named: "bannerPlaceholder"))
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        offerSelected?(dataSource[indexPath.item])
    }
    
    
}

