//
//  CrecheListVC.swift
//  MindSpace
//
//  Created by webwerks on 09/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class CrecheListVC: ListBaseVC {

    //private var creche : [CrecheList] = []
    var totalRecords : Int?

    
    var creche : [CrecheList] = [] {
        didSet {
            tableView.reloadData()
            if creche.count == 0{
                // tblView.tableFooterView = noDataView
                DispatchQueue.main.async {
                    self.view.makeToast("No data available", duration: 1.0, position: .center)
                }
            }else {
                tableView.tableFooterView = nil
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Creche"
        callWebService()
    }
    override func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_CrecheList, objectType: CrecheListData.self, isHud: true, controller: self, success: {
            response in
            
            self.totalRecords = response.TotalRecords
            if self.totalRecords == nil{
                self.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            
            guard let list = response.DataList else {return}
            if self.pageInfo.pageIndex == 1  {
                self.creche = list
            }else {
                self.creche.append(contentsOf: list)
            }
            self.tableView.reloadData()
        })
    }
}

extension CrecheListVC  {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.creche.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CrecheCell", for: indexPath) as! CrecheCell
        let crecheData = self.creche[indexPath.row]
        cell.LBL_eventTitle?.text       = crecheData.Title
        cell.LBL_eventLocation?.text    = crecheData.BuildingName
        cell.LBL_eventTime?.text        = crecheData.Time
        cell.LBL_subText?.text          = crecheData.Capacity
        
         if (crecheData.BannerImage) != nil {
             cell.eventBannerImgView.sd_setImage(with: URL.init(string: crecheData.BannerImage!), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
         }
        return cell
    }
}
extension CrecheListVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let crecheData = creche[indexPath.row]
        let targetVC = Storyboard.crecheDetailsViewController
        targetVC.crecheID = self.creche[indexPath.row].CrecheId
        targetVC.bannerImageName = crecheData.BannerImage!
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
