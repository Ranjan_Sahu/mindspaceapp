//
//  RUServicesVC.swift
//  MindSpace
//
//  Created by webwerks on 10/8/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUServicesVC: UIViewController,  UIScrollViewDelegate {
    
    // MARK:- Constants
    let cellIdentifier = "HomeScreenCollectionViewCell"
    
    // MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK:- Objects
    var namesArray = [String]()
    var imgNameArray = [String]()
    
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = false
        //namesArray = ["Visitor Request", "Visit Details", "Car Parking", "About Common Spaces", "Creche", "Book Cab", "Entertainment & Playground", "Gym"]
        namesArray = ["Visitor Request", "Visit Details", "Car Parking", "About Common Spaces", "Creche", "Entertainment & Playground", "Gym"]
        imgNameArray = ["icon_visitor_request", "icon_visit_details", "icon_car_parking", "icon_commonspaces", "icon_creche", "icon_playground", "icon_gym"]

        
        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Services"
        
    }
}
extension RUServicesVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: - CollectionView Delegates and DataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
        cell.LBL_title.text = namesArray[indexPath.row]
        cell.imgView.image = UIImage(named: imgNameArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row == 0) {
            let targetVC = Storyboard.visitorRequestVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 1) {
            let targetVC = Storyboard.visitDetailsVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 2) {
            self.navigationController?.pushViewController(Storyboard.carParkingViewController, animated: true)
        }else if (indexPath.row == 3) {
            let targetVC = Storyboard.aboutCommonSpacesVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 4) {
            let targetVC = Storyboard.crecheListVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 5) {
            let targetVC = Storyboard.ruBookACabVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 6) {
            let targetVC = Storyboard.entertainmentAndPlaygroundListVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 7) {
            let targetVC = Storyboard.ruGymVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (self.collectionView.bounds.size.width-4)/2
        //let h = (self.collectionView.bounds.size.height-6)/3
        let size = CGSize.init(width: w, height: w)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
}

