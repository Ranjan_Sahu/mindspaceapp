//
//  SupportListTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 12/18/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class SupportListTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var ticketTypeTextLabel: UILabel!
    @IBOutlet weak var subjectTestLabel: UILabel!
    @IBOutlet weak var priorityTextLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var addedClockLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpCell(){
        bgView.layer.borderWidth = 0.5
        bgView.layer.borderColor = UIColor.black.cgColor
        selectionStyle = .none
    }
}
