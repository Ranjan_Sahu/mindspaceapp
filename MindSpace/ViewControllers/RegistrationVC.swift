//
//  RegistrationVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {

    // Constants
    
    // Outlets
    
    // Objects
    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
    }
}
