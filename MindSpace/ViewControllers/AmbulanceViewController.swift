//
//  AmbulanceViewController.swift
//  MindSpace
//
//  Created by webwerks on 2/18/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit
import FRHyperLabel

class AmbulanceViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier  = "ATMTableViewCell"
    var ambulanceList = [Ambulancelist]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ambulances"
        self.setupTableView()
       
    }
    func setupTableView(){
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 180
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.separatorColor = .clear
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAmbulanceList()
    }

    func getAmbulanceList(){
        
        let parameters = ["pageInfo" : ["PageIndex" : 1,
                                        "PageSize" : 30],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getAmbulanceList, objectType: GetAmbulanceLists.self, isHud: true, controller: self, success: { [weak self] response in
            if response.TotalRecords == 0{
                self?.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            self?.ambulanceList = response.DataList ?? []
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
        
    }
}

extension AmbulanceViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ambulanceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ATMTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ATMTableViewCell
        var ambulanceInfo = self.ambulanceList[indexPath.row]
        cell.backgroundColor = .clear
        cell.separatorInset = .zero
        cell.bannerImageView.image = UIImage(named: "icon_ambulance_38")
        cell.LBL_Name_ATM.text = "XYZ"
        cell.lblAtmDesc.text = "\(ambulanceInfo.ComplexName ?? ""), \(ambulanceInfo.LocationName ?? "")"
        
        ambulanceInfo.ContactNumber = ambulanceInfo.ContactNumber?.replacingOccurrences(of: ",", with: " , ")
        cell.lmgLocation.image = UIImage(named: "icon_call")
        var searchElementArr = [String]()
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: ambulanceInfo.ContactNumber!, range: NSRange(ambulanceInfo.ContactNumber!.startIndex..., in: ambulanceInfo.ContactNumber!))
            for match in matches{
                if match.resultType == .phoneNumber, let number = match.phoneNumber {
                    print(number)
                    searchElementArr.append(number)
                }
            }
        } catch {
            print(error)
        }
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                          NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)]
        
        cell.LBL_Buiolding_Name?.attributedText = NSAttributedString(string: ambulanceInfo.ContactNumber!, attributes: attributes)
        
        let handlr = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if (substring?.contains("@"))! {
                //Open email box
                let mailUrl = URL(string: "mailto:\(substring ?? "")")
                if mailUrl != nil {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(mailUrl!)
                    } else {
                        UIApplication.shared.openURL(mailUrl!)
                    }
                }
            } else {
                if let url = URL(string: "tel://\(substring ?? "")"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }else{
                    print("cant open url")
                }
            }
        }
        
        cell.LBL_Buiolding_Name?.setLinksForSubstrings(searchElementArr, withLinkHandler: handlr)
                
        return cell
    }
}
