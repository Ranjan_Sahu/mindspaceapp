//
//  ATMTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 2/8/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit
import FRHyperLabel

class ATMTableViewCell: UITableViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var LBL_Name_ATM: UILabel!
    @IBOutlet weak var LBL_Buiolding_Name: FRHyperLabel!
    @IBOutlet weak var lblAtmDesc: UILabel!

    @IBOutlet weak var lmgLocation: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bannerImageView.layer.borderWidth = 1
        bannerImageView.layer.masksToBounds = false
        bannerImageView.layer.borderColor = UIColor.lightGray.cgColor
        bannerImageView.layer.cornerRadius = bannerImageView.frame.height/2
        bannerImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
