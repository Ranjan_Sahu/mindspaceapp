//
//  OffersCollectionViewCell.swift
//  MindSpace
//
//  Created by webwerks on 2/11/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit

class OffersCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var offerImageView: RoundImageView!
}
