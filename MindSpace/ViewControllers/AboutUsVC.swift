//
//  AboutUsVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import WebKit

class AboutUsVC: UIViewController {

  
    //MARK:- Outlets
    @IBOutlet weak var BTN_Done: UIButton!
    @IBOutlet weak var webView: UIWebView!
    var visitor : Bool = false
    //MARK:- Variables
    public var sysDataInfo : SystemDataInfo? = nil
    
    //MARK:-  Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSystemConfiguration()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.title = "About Us"
        if visitor {
            self.BTN_Done.isHidden = true
        }
    }
}
// MARK:- IBActions
extension AboutUsVC {
    
    @IBAction func doneAction(_ sender: Any) {
        let targetVC =  Storyboard.termsConditionsVC
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
// MARK:- Service Calls
extension AboutUsVC {
    //MARK:- Class Methods
    private func setupController(){
        self.webView.delegate = self
        if CommonHelper.checkReachability() == false {
            CommonHelper.showAlert(vc: self, title: "Network Issue", msg: "Please check your internet connection !")
            BTN_Done.isHidden = true
        }else {
            self.getSystemConfiguration()
            BTN_Done.isHidden = false
        }
    }
    
    //MARK:- Webservice Call
    func getSystemConfiguration() {
        
        let parameters = ["pageInfo": ["PageIndex" : "1",
                                       "PageSize" : "20"],
                          "entity" : ["ConfigurationId" : "4"],
                          "deviceInfo" : CommonHelper.getDeviceInfo()
                        ]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetSystemConfiguration, objectType: SystemData.self, isHud: true, controller: self, success: {
            response in
            guard let aboutUs = response.Data?.AboutUs else {return}
            
            self.sysDataInfo = response.Data
        
            UserDefaults.standard.set(self.sysDataInfo?.AboutUs, forKey: "AboutUs_String")
            UserDefaults.standard.set(self.sysDataInfo?.TermsAndCondition, forKey: "Terms_String")
            UserDefaults.standard.set(self.sysDataInfo?.PrivacyPolicy, forKey: "PrivacyPolicy_String")
            UserDefaults.standard.set(self.sysDataInfo?.BannerSlideTime, forKey: "BannerSlide_Time")
            UserDefaults.standard.set(self.sysDataInfo?.OlaIOSStoreLink, forKey: "OlaIOSStoreLink")
            UserDefaults.standard.set(self.sysDataInfo?.UberIOSStoreLink, forKey: "UberIOSStoreLink")
            UserDefaults.standard.set(self.sysDataInfo?.UberIOSAppLink, forKey: "UberIOSAppLink")
            UserDefaults.standard.set(self.sysDataInfo?.OlaIOSAppLink, forKey: "OlaIOSAppLink")
            UserDefaults.standard.synchronize()
            
            self.webView.loadHTMLString(aboutUs, baseURL: nil)
        })
    }
}

extension AboutUsVC : UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
