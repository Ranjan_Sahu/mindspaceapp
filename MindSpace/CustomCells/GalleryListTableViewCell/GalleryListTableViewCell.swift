//
//  GalleryListTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 10/5/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class GalleryListTableViewCell: UITableViewCell {

    @IBOutlet weak var galleryBannerImgView: UIImageView!
    @IBOutlet weak var LBL_Title: UILabel!
    @IBOutlet weak var LBL_Location: UILabel!
    @IBOutlet weak var galleryDateIconView: UIView!
    @IBOutlet weak var LBL_month: UILabel!
    @IBOutlet weak var LBL_date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.galleryDateIconView.layer.cornerRadius = 6.0
        self.galleryDateIconView.layer.borderWidth = 1.0
        self.galleryDateIconView.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
