//
//  RUFAndBLocationsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/3/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUFAndBLocationsVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "CrecheCell"
    let refreshControl = UIRefreshControl()
    private var offersData : OffersData?
    private var offerList : [CategoryList]?
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var topConstartintForTableView: NSLayoutConstraint!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    private var slides:[HomeScreenBannerView] = [];
    private var bannerTimer:Timer? = nil
    @IBOutlet weak var topConstarintTableView: NSLayoutConstraint!

    

    
    
    // MARK:- Objects
    var fbList : [FAndBList] = [] {
        didSet {
            tblView.reloadData()
            if fbList.count == 0{
               // tblView.tableFooterView = noDataView
                DispatchQueue.main.async {
                    self.view.makeToast("No data available", duration: 1.0, position: .center)
                }
            }else {
                tblView.tableFooterView = nil
            }
        }
    }
    private var fbData : FAndBData?
    private var pageInfo = PageInfo()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        SVProgressHUD.show()
        addRefreshControlToTableView()
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        //self.callWebServiceForBanner()
        self.tblView.reloadData()
        self.tblView.tableHeaderView = UIView()
        self.getFAndBList()
        self.addTapToScroll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "F&B"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
    private func addTapToScroll(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        self.bannerScrollView.addGestureRecognizer(tap)
    }
    @objc func scrollViewTapped(){
        let currentBanner = self.pgControl.currentPage
        let bannerID = self.offersData?.BannerList?[currentBanner].Id
        let vc = Storyboard.offerDetailVC
        vc.offerID = bannerID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension RUFAndBLocationsVC {
    // MARK:- Service Calls
    private func getFAndBList() {
        
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
                                        "PageSize" : "\(pageInfo.pageSize)"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters,  url: WebserviceURLs.Service_GetFNBOutletList, objectType: FAndBData.self, isHud: true, controller: self, success: { [weak self] fAndBData in
            self?.fbData = fAndBData
            if let list = fAndBData.DataList {
                if self?.pageInfo.pageIndex == 1  {
                    self?.fbList = list
                }else {
                    self?.fbList.append(contentsOf: list)
                }
            }
            //self?.setBanner()
            self?.tblView.reloadData()
        })
    }
    
    private func callWebServiceForBanner() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getOfferList, objectType: OffersData.self, isHud: true, controller: self, success: {[weak self]
            response in
            
                guard let list = response.DataList else {
                   let alert = UIAlertController.init(title: "Error", message: Messages.noData, preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
                        self?.navigationController?.popViewController(animated: true)
                    }
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                    self?.view.isHidden = true
                    return
                }
                
            self?.offersData = response
            self?.offerList = list
            self?.tblView.reloadData()
            self?.tblView.tableHeaderView = UIView()
            //self?.setBanner()
        })
    }
    
    // MARK:- Custom methods
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        getFAndBList()
        refreshControl.endRefreshing()
    }
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
}
extension RUFAndBLocationsVC : UITableViewDataSource{
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return fbList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        
        var cell: CrecheCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CrecheCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CrecheCell
        }
        
            let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.center = (cell.imageView?.center)!
            myActivityIndicator.startAnimating()
            cell.imageView?.addSubview(myActivityIndicator)
        
        
        let fAndB =  self.fbList[indexPath.row]
        cell.LBL_eventTitle.text = fAndB.FNBOutletName
        cell.LBL_eventLocation.text = fAndB.BuildingName
        cell.LBL_eventTime.text = fAndB.Time
        cell.LBL_subText.text = "\(fAndB.Capacity ?? 0)"
         if (fAndB.BannerImage) != nil {
            cell.eventBannerImgView.sd_setImage(with: URL.init(string: fAndB.BannerImage ?? ""), placeholderImage: nil, options: SDWebImageOptions.highPriority, completed: nil)
            
        }
        
        return cell
    }

}
extension RUFAndBLocationsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.fandBDetailsVC
        targetVC.outletId = self.fbList[indexPath.row].FNBOutletId
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
// MARK:- ScrollView Delegate
extension RUFAndBLocationsVC : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
            if (endScrolling >= Int(scrollView.contentSize.height)){
                pageInfo.pageIndex += 1
                if let totalRecords = self.fbData?.TotalRecords {
                    if (self.fbList.count) < totalRecords {
                        self.getFAndBList()
                    }
                }
            }
        }
    }
}
extension RUFAndBLocationsVC{
    private func setBanner(){
        // Banner Setup
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        if((self.bannerTimer) != nil){
            self.bannerTimer?.invalidate()
            self.bannerTimer = nil
        }
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
    }
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        print("offers data contains ")

        if offersData?.BannerList?.count ?? 0 <= 0 {
           self.tblView.sectionHeaderHeight = 0
        }
        
        print("valyu of consrtaniunt is \(topConstarintTableView.constant)")
        if let bannerImgArray = self.offersData?.BannerList?.map({ $0.BannerImage ?? ""}) {
        
            for imgStr in bannerImgArray  {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                slide.bannerImgView!.sd_setImage(with: URL.init(string: imgStr), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
                bannerVwArray.append(slide)
            }
            return bannerVwArray
        }
        
        return []
    }
    
    private func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(self.offersData?.BannerList?.count ?? 0)
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
    
    
}


