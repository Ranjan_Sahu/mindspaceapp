//
//  RUGalleryVC.swift
//  MindSpace
//
//  Created by webwerks on 10/5/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUGalleryVC: UIViewController {
    private let refreshControl = UIRefreshControl()
    private var pageInfo = PageInfo()
    // MARK:- Constants
    let cellIdentifier = "GalleryListTableViewCell"
    var totalRecords : Int? = 0
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var gallaryBannerView: UIView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var janBtn: UIButton!
    @IBOutlet weak var febBtn: UIButton!
    @IBOutlet weak var calenderBtn: UIButton!
    @IBOutlet weak var noDataView: UIView!

    
    // MARK:- Objects
    private var catrgoryArray = [String]()
    private var slides:[HomeScreenBannerView] = [];
    private var bannerTimer:Timer? = nil
    private var dummyImgArray : [String]? = nil
    private var galleryData : GalleryData?
    private var refresh : UIRefreshControl! = UIRefreshControl()
    var checkData = false
    private var galleryList : [GalleryList] = []
    private var gBannerView:UIView? = nil
    
//    private var galleryList : [GalleryList] = []{
//        didSet {
//            tblView.reloadData()
//            if galleryList.count == 0{
//                // tblView.tableFooterView = noDataView
//                DispatchQueue.main.async {
//                    self.view.makeToast("No data available", duration: 1.0, position: .center)
//                }
//            }else {
//                tblView.tableFooterView = nil
//            }
//        }
//    }
    
//    //MARK:- Class Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentDate = Date()
        janBtn.setTitle(currentDate.getCurrentMonth(), for: .normal)
        febBtn.setTitle(currentDate.getNextMonth(), for: .normal)
        self.addRefreshControlToTableView()
        self.addTapToScroll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let customFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        customFooterView.backgroundColor = .clear
        gBannerView = gallaryBannerView
        self.tblView.tableFooterView = customFooterView
        self.setupScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
    private func addTapToScroll(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        self.bannerScrollView.addGestureRecognizer(tap)
    }
    @objc func scrollViewTapped(){
        let currentBanner = self.pgControl.currentPage
        let bannerID = self.galleryData?.BannerList?[currentBanner].Id
        let vc = Storyboard.galleryDetailsVC
        vc.albumID = bannerID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
// MARK: - Button Action
extension RUGalleryVC {
    @IBAction func allBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: true, janBtn: false, febBtn: false, calenderbtn: false)
    }
    @IBAction func janBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: true, febBtn: false, calenderbtn: false)
    }
    @IBAction func febBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: false, febBtn: true, calenderbtn: false)
    }
    @IBAction func calenderBtnAction(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: false, febBtn: false, calenderbtn: true)
    }
}
// MARK:- Custom methods
extension RUGalleryVC {
    
    //MARK:- Class Methods
    private func setupScreen(){
        self.view.layoutIfNeeded()
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.getGalleryList()
        self.addRefreshControlToTableView()
        self.setupNavigation()
    }
    private func setupNavigation(){
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Gallery"
    }
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    private func setBanner(){
        // Banner Setup
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        if((self.bannerTimer) != nil){
            self.bannerTimer?.invalidate()
            self.bannerTimer = nil
        }
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
    }
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        if let bannerImgArray = self.galleryData!.BannerList?.map({ $0.BannerImage ?? ""}) {
            for imgStr in bannerImgArray  {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                slide.bannerImgView!.sd_setImage(with: URL.init(string: imgStr), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
                bannerVwArray.append(slide)
            }
            return bannerVwArray
        }
        
        return []
    }
    
    private func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
   
    private func buttonSelection(allBtn : Bool, janBtn : Bool, febBtn: Bool, calenderbtn  : Bool) {
        self.allBtn.isSelected = allBtn
        self.janBtn.isSelected = janBtn
        self.febBtn.isSelected = febBtn
        checkData = false
        handleRefresh()
    }
    
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(self.galleryData?.BannerList?.count ?? 0)
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
    
    //MARK:- Service Calls
    private func getGalleryList() {
        
        let month = getMonthButtontitle()
        
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)", "Month": "\(month)"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters,  url: WebserviceURLs.Service_GetGalleryList, objectType: GalleryData.self, isHud: true, controller: self, success: { [weak self] galleryData in
            UIApplication.shared.endIgnoringInteractionEvents()
            self?.galleryData = galleryData
            
            DispatchQueue.main.async {
                self?.hideBanner()
            }
            guard galleryData.DataList != nil else {

                if (self?.galleryList.isEmpty)! {
                    self?.galleryList.removeAll()
                }
        
                if (self?.pageInfo.pageIndex)! > 1 {
                    self?.pageInfo.pageIndex = (self?.pageInfo.pageIndex)! - 1
                    self?.view.makeToast("No more data available", duration: 1.0, position: .center)

                } else {
                    self?.view.makeToast("No data available", duration: 1.0, position: .center)
                }
                DispatchQueue.main.async {
                    self?.tblView.reloadData()
                }
                return
            }
            
            if self?.pageInfo.pageIndex == 1  {
                self?.galleryList = galleryData.DataList ?? []
                self?.totalRecords = self?.galleryData?.TotalRecords ?? 0

            }else {
                if let galleryList = galleryData.DataList {
                    self?.galleryList.append(contentsOf: galleryList)
                }
            }
            
            if (self!.galleryData!.BannerList!.count) > 0{
                DispatchQueue.main.async {
                    self?.showBanner()
                    self?.setBanner()
                }
            }else{
                DispatchQueue.main.async {
                    self?.hideBanner()
                }
            }
           
            DispatchQueue.main.async {
                self?.tblView.reloadData()
            }
            
        })
    }
    
    func hideBanner(){
        self.tblView.tableHeaderView = UIView()
    }
    
    func showBanner(){
        self.tblView.tableHeaderView = gBannerView
        self.bannerScrollView.isHidden = false
        self.pgControl.isHidden = false
    }
    
}
extension RUGalleryVC {
    private func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        UIApplication.shared.beginIgnoringInteractionEvents()
        self.galleryList.removeAll()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.getGalleryList()
        }
    }
}
// MARK: - TableView datsource and delegates
extension RUGalleryVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return galleryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: GalleryListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? GalleryListTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? GalleryListTableViewCell
        }
        cell.selectionStyle = .none
        let details = self.galleryList[indexPath.row]
        
        
        cell.LBL_Title.text = details.AlbumName
        cell.LBL_Location.text = String(format: "%@, %@", details.ComplexName!,details.LocationName!)
        if let imgUrlStr = details.BannerImage,  let url:URL = URL(string: imgUrlStr) {
            cell.galleryBannerImgView.sd_setImage(with: url, placeholderImage: UIImage(named: "bannerPlaceholder"))
        }
        if let date = details.Date?.components(separatedBy: "/") {
            cell.LBL_date.text = date[0]
            cell.LBL_month.text = date[1]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.galleryDetailsVC
        targetVC.albumID = galleryList[indexPath.row].AlbumId
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
   
}

// MARK:- ScrollView Delegate
extension RUGalleryVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            if(totalRecords == self.galleryList.count){
                return;
            }
            let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
            if (endScrolling >= Int(scrollView.contentSize.height)){
                pageInfo.pageIndex += 1
                getGalleryList()
            }
        }
    }
}
extension RUGalleryVC {
    func getMonthButtontitle() -> String {
        if allBtn.isSelected {
            let title = allBtn.title(for: .normal)
            return title!
        } else if janBtn.isSelected {
            let title = janBtn.title(for: .normal)
            return title!
        } else if febBtn.isSelected {
            let title = febBtn.title(for: .normal)
            return title!
        }
        return ""
    }
}
