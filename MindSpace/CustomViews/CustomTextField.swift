//
//  CustomTextField.swift
//  MindSpace
//
//  Created by Zeeshan  on 21/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation
import UIKit

class RegisterationTextField : UITextView {
    override func layoutSubviews() {
        layer.borderColor = UIColor.gray.cgColor
        layer.cornerRadius = self.layer.frame.size.height / 2
        layer.borderWidth = 1.0
    }
}
