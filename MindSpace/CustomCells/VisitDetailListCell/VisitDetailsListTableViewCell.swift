//
//  VisitDetailsListTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 10/8/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitDetailsListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var LBL_visitorInfo: UILabel!
}
