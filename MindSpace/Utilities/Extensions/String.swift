//
//  String.swift
//  MindSpace
//
//  Created by Webwerks on 11/1/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

extension String {
    func getDateArray() -> [String] {
        
        let dateFormatter = DateFormatter()
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a") {
            //phone is set to 12 hours
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        } else {
            //phone is set to 24 hours
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
    
        let newDate = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let str =  dateFormatter.string(from: newDate!)
        let dateArray = str.components(separatedBy: "-")
        return dateArray
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
