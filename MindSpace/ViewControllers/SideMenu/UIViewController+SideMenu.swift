import UIKit

// MARK: - UIViewController+SideMenu

extension UIViewController {
    
    var sideMenuViewController: SideMenu? {
        get {
            var iter : UIViewController = self.parent!

            while (iter.nibName != nibName) {
                if iter.isKind(of: SideMenu.classForCoder()) {
                    return (iter as! SideMenu)
                } else if (iter.parent != nil && iter.parent != iter) {
                    iter = iter.parent!
                }
            }
            return nil
        }
        set(newValue) {
            self.sideMenuViewController = newValue
        }
    }
    
    // MARK: - Public
    // MARK: - IB Action Helper methods
    
    @IBAction public func presentLeftMenuViewController(sender: AnyObject) {
       
        if let tabBarVC = self.tabBarController {
            tabBarVC.navigationController?.sideMenuViewController!.presentLeftMenuViewController()
            return
        }
        self.navigationController?.sideMenuViewController!.presentLeftMenuViewController()

    }
    
    @IBAction public func presentRightMenuViewController(sender: AnyObject) {
        self.sideMenuViewController!.presentRightMenuViewController()
    }
    
    func addLeftMenuButton() {
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "Menu"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(presentLeftMenuViewController(sender:)))
        self.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    
    func addNotificationBarButton(_ viewController: UIViewController, action: Selector)
    {
    }
    
    func addMenuBarButton(_ viewController: UIViewController, action: Selector, title: String)
    {
        
        let item = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: viewController, action: action)
        viewController.navigationItem.leftBarButtonItem = item
        viewController.navigationItem.title = title
        item.tintColor = UIColor.white
        
    }
    
    
    func addNotificationAndFilterButton( viewController: UIViewController, notificationAction: Selector, filterAction: Selector)
    {
        // Creating Notification Bar button
//        let NotificationButton = UIButton.init(type: UIButtonType.custom)
//        NotificationButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//        NotificationButton.addTarget(viewController, action: notificationAction, for: UIControlEvents.touchUpInside)
//        NotificationButton.setImage(UIImage(named: "Notification"), for: .normal)
//        let notificationBarButtonItem = UIBarButtonItem(customView: NotificationButton)
        
        // Creating Filter Bar button
        let filterButton = UIButton.init(type: UIButtonType.custom)
        filterButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        filterButton.addTarget(viewController, action: filterAction, for: UIControlEvents.touchUpInside)
        filterButton.setImage(UIImage(named: "Filter"), for: .normal)
        let filterBarButtonItem = UIBarButtonItem(customView: filterButton)
      
        let spaceButton = UIButton.init(type: UIButtonType.custom)
        spaceButton.frame = CGRect(x: 0, y: 0, width: 0.5, height: 20)
        let spaceBarButtonItem = UIBarButtonItem(customView: spaceButton)
        //viewController.navigationItem.rightBarButtonItems=[ filterBarButtonItem,spaceBarButtonItem ,notificationBarButtonItem]
        viewController.navigationItem.rightBarButtonItems=[ filterBarButtonItem,spaceBarButtonItem]
        
    }
    func addNotificationAndMeterButton( viewController: UIViewController, notificationAction: Selector, meterAction: Selector)
    {
        // Creating Notification Bar button
        let NotificationButton = UIButton.init(type: UIButtonType.custom)
        NotificationButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        NotificationButton.addTarget(viewController, action: notificationAction, for: UIControlEvents.touchUpInside)
        NotificationButton.setImage(UIImage(named: "Notification"), for: .normal)
        let notificationBarButtonItem = UIBarButtonItem(customView: NotificationButton)
        
        // Creating Filter Bar button
        let meterButton = UIButton.init(type: UIButtonType.custom)
        meterButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        meterButton.addTarget(viewController, action: meterAction, for: UIControlEvents.touchUpInside)
        meterButton.setImage(UIImage(named: "SpeedIndicator"), for: .normal)
        let filterBarButtonItem = UIBarButtonItem(customView: meterButton)
       
        let spaceButton = UIButton.init(type: UIButtonType.custom)
        spaceButton.frame = CGRect(x: 0, y: 0, width: 0.5, height: 20)
        let spaceBarButtonItem = UIBarButtonItem(customView: spaceButton)
        viewController.navigationItem.rightBarButtonItems=[ filterBarButtonItem,spaceBarButtonItem ,notificationBarButtonItem]
        
    }
}
