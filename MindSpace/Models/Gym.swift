//
//  GymModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  GymList : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [GymListing]?
    let TotalRecords : Int?
}

struct GymListing : Codable {
    var GymId : Int?
    var Title : String?
    var ContactDetails : String?
    var BuildingName : String?
    var ComplexName : String?
    var ComplexId : Int?
    var IsActive : Bool?
    var CreatedOn : String?
    var StartTime : String?
    var EndTime : String?
    var Time : String?
    var Capacity : Int?
    var BannerImage : String?
    var ImageURLs : [String]?
}

struct  GymData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : GymDetails?
    
}

struct  GymDetails : Codable {
    let Title : String?
    let Description : String?
    let ContactDetails : String?
    let BookingProcessDocument : String?
    let FloorId : Int?
    let FloorName : String?
    let BuildingId : Int?
    let BuildingName : String?
    let ComplexId : Int?
    let ComplexName : String?
    let StartTime : String?
    let EndTime : String?
    let Time : String?
    var Capacity : Int?
    var BannerImage : String?
    let ImageUrls : [String]?
}
