//
//  SystemDataModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  SystemData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : SystemDataInfo?
}

struct  SystemDataInfo : Codable {
    let AboutUs : String?
    let TermsAndCondition : String?
    let FacebookLink : String?
    let TwitterLink : String?
    let OlaLink : String?
    let UberLink : String?
    let ConfigurationId : Int?
    let OlaIOSAppLink : String?
    let OlaIOSStoreLink : String?
    let OlaAndroidAppLink : String?
    let OlaAndroidStoreLink : String?
    let UberIOSAppLink : String?
    let UberIOSStoreLink : String?
    let UberAndroidAppLink : String?
    let UberAndroidStoreLink : String?
    let CreatedOn : String?
    let PrivacyPolicy : String?
    let BannerSlideTime : Int?
}
