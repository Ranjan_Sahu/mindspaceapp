//
//  BannerModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  BannerData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [BannerList]?
    let TotalRecords : Int?
}

struct  BannerList : Codable {
    let BannerId : Int?
    let BannerName : String?
    let BannerDesc : String?
    let BannerImage : String?
    let BannerType : String?
    let BannerTypeId : Int?
    let BannerFor : String?
    let BannerForId : Int?
    let CreatedOn : String?
    let Id : Int?
    let Title : String?
}

struct  BannerListingData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [BannerListing]?
}

class BannerListing : Codable {
    var Id : Int?
    var Title : String?
    var BannerImage : String?
}

struct BannerVisitorData : Codable {
    let DataList : [BannerVisitorList]
    let Status : Bool?
}
struct BannerVisitorList : Codable {
    let Id : Int?
    let Title : String?
    let BannerImage : String?
}
