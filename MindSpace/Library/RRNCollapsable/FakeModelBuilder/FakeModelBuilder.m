//
//  FakeModelBuilder.m
//  Example
//
//  Created by Robert Nash on 19/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

#import "FakeModelBuilder.h"

@implementation FakeModelBuilder

+(NSMutableArray *)buildMenuCompany
{
    

    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 13; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Company Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Industry Type:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Phone Numbers:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Email Addresses:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Social Profiles:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Websites:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Address:";
                section.items =  @[[NSNull null]];
                break;
                
            case 9:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
                
            case 10:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                 break;
            case 11:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
            case 12:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuProject
{
 
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 10; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Project Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Address:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Before Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"After Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
                
            case 9:
                section.title = @"Tools:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuContact
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 12; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Contact Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Projects:";
                section.items = @[[NSNull null]];
                break;
            
            case 2:
                section.title = @"Phone Numbers:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Email Addresses:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Social Profiles:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Websites:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Address:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 9:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 10:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
            case 11:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuCalendar
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 6; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Calendar Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Projects:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Contacts:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuPurchase
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 9; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Purchase Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            
                
            case 6:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Tools:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuProjectEdit
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 12; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Company Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Industry Type:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Phone Numbers:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Email Addresses:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Social Profiles:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Websites:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Address:";
                section.items =  @[[NSNull null]];
                break;
                
            case 9:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 10:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 11:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
                
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuToolEdit
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 7; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Tool Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
     
                
            case 1:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
                
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuCallEdit
{
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 3; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Score:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
            case 1:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
            case 2:
                section.title = @"Calendar:";
                section.items =  @[[NSNull null]];
                break;
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}


+(NSMutableArray *)buildMenuAccountEdit
{
    NSMutableArray *collector = [NSMutableArray new];
    MenuSection *section = [MenuSection new];
    section.title = @"Basic Account Information:";
    section.isVisible = @YES;
    section.items = @[@"0"];
    [collector addObject:section];
    return [collector copy];
}

+(NSMutableArray *)buildMenuViewCompany
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 7; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Contact Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contact:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Projects:";
                section.items = @[[NSNull null]];
                break;

                
            case 3:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
                
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}


+(NSMutableArray *)buildMenuViewTransactionDetail
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 3; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"One Time Fee Transaction:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Monthly Fee Transaction:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Call Transaction Fee:";
                section.items = @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}


+(NSMutableArray *)buildMenuViewCalendar
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 6; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Calendar Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Companies:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Contact:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuViewContact
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 7; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Contact Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"About:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuViewPurchase
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 10; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Tools:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
            case 4:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
            case 6:
                section.title = @"About:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
            case 8:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
            case 9:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}
+(NSMutableArray *)buildMenuViewTools
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 7; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
           
            case 1:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
            case 2:
                section.title = @"Photos:";
                section.items =  @[[NSNull null]];
                break;
            case 3:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
//            case 4:
//                section.title = @"About:";
//                section.items =  @[[NSNull null]];
//                break;
            case 4:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"Projects:";
                section.items =  @[[NSNull null]];
                break;
            case 6:
                section.title = @"Companies:";
                section.items =  @[[NSNull null]];
                break;
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}
+(NSMutableArray *)buildMenuViewProject
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 9; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Project Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Tools:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Before/During Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"After Photos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 5:
                section.title = @"About:";
                section.items =  @[[NSNull null]];
                break;
                
            case 6:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            case 7:
                section.title = @"Videos:";
                section.items =  @[[NSNull null]];
                break;
                
            case 8:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuViewCall
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 6; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Call Details:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Contacts:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Score:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Calendar Events:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Form Entry:";
                section.items =  @[[NSNull null]];
                break;
            
            case 5:
                section.title = @"Notes:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuAgreementDetails
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 1; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic PPCall Agreement Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuAgreementEdit
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 5; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic PPCall Agreement Information:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Premium PPCall Price:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Cap PPCall Price:";
                section.items =  @[[NSNull null]];
                break;
                
            case 3:
                section.title = @"Discounted PPCall Price:";
                section.items =  @[[NSNull null]];
                break;
                
            case 4:
                section.title = @"Monthly PPCall Price:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSMutableArray *)buildMenuBillingEdit
{
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 3; i++) {
        
        MenuSection *section = [MenuSection new];
        
        switch (i) {
            case 0:
                section.title = @"Basic Billing Settings:";
                section.isVisible = @YES;
                section.items = @[[NSNull null]];
                break;
                
            case 1:
                section.title = @"Primary Payment Method:";
                section.items = @[[NSNull null]];
                break;
                
            case 2:
                section.title = @"Secondary Payment Method:";
                section.items =  @[[NSNull null]];
                break;
                
            default:
                break;
        }
        
        [collector addObject:section];
    }
    
    return [collector copy];
}


@end
