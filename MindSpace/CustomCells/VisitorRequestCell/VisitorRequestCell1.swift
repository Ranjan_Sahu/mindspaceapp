//
//  VisitorRequestCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitorRequestCell1: UITableViewCell {
    @IBOutlet weak var firstNameTxtField: RoundTextField!
    @IBOutlet weak var lastNameField: RoundTextField!
    @IBOutlet weak var emailIdTxtField: RoundTextField!
    @IBOutlet weak var mobileNumberTxtField: RoundTextField!
    @IBOutlet weak var baggageYesBtn: UIButton!
    @IBOutlet weak var baggageNoBtn: UIButton!
    @IBOutlet weak var vehicalYesBtn: UIButton!
    @IBOutlet weak var vehicalNoBtn: UIButton!
    @IBOutlet weak var laptopYesBtn: UIButton!
    @IBOutlet weak var laptopNoBtn: UIButton!
    @IBOutlet weak var lastNameHeight: NSLayoutConstraint!
    @IBOutlet weak var lastNameView: UIView!
    
    var cellNumber : Int?
    public var setDetails : VisitorInfo?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        guard let details = self.setDetails else {return}
        self.firstNameTxtField.text = details.VistorName
        self.mobileNumberTxtField.text = details.MobileNumber
        self.emailIdTxtField.text = details.Email
        
        if details.IsVehicle! {
            self.vehicalYesBtn.isSelected = true
            self.vehicalNoBtn.isSelected = false
        }else {
            self.vehicalYesBtn.isSelected = false
            self.vehicalNoBtn.isSelected = true
        }
        if details.IsBaggage! == true {
            self.baggageYesBtn.isSelected = true
            self.baggageNoBtn.isSelected = false
        }else {
            self.baggageYesBtn.isSelected = false
            self.baggageNoBtn.isSelected = true
        }
        if details.IsLaptop! == true {
            self.laptopYesBtn.isSelected = true
            self.laptopNoBtn.isSelected = false
        }else {
            self.laptopYesBtn.isSelected = false
            self.laptopNoBtn.isSelected = true
        }
    }
}


extension VisitorRequestCell1 : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
//        switch textField {
//        case firstNameTxtField:
//            visitorRequest.firstName = textField.text ?? ""
//        case lastNameField:
//            visitorRequest.lastName = textField.text ?? ""
//        case emailIdTxtField:
//            visitorRequest.emailId = textField.text ?? ""
//        case mobileNumberTxtField:
//            visitorRequest.phoneNo = textField.text ?? ""
//        default:
//            break
//        }
//        delegate?.visitorDataChanged(index: cellIndex, visitorDetails: visitorRequest)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
