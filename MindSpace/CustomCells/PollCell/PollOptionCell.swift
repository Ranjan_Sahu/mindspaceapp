//
//  PollOptionCell.swift
//  MindSpace
//
//  Created by Webwerks on 12/14/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class PollOptionCell: UITableViewCell {

    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lblOption: UILabel!
}


