//
//  FAndBModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  FAndBData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [FAndBList]?
    let TotalRecords : Int?
}

struct  FAndBList : Codable {
    var FNBOutletId : Int?
    var FNBOutletName : String?
    var MenuCard : String?
    var Capacity : Int?
    var Time : String?
    var FloorNumber : String?
    var BuildingName : String?
    var ComplexName : String?
    var LocationName : String?
    var ComplexId : Int?
    var BannerImage : String?
}

struct  FAndBDetailsData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : FAndBDetails?
}

struct  FAndBDetails : Codable {
    let FNBOutletId : Int?
    let FNBOutletName : String?
    let Description : String?
    let Time : String?
    let Capacity : Int?
    let ContactPerson : String?
    let ContactNumber : String?
    let ComplexId : Int?
    let ComplexName : String?
    let FloorId : Int?
    let FloorNumber : String?
    let BuildingId : Int?
    let BuildingName : String?
    let MenuCard : String?
    let FNBOutletTypeName : String?
    let FNBOutletTypeId : Int?
    var BannerImage : String?
    let ImageUrls : [ImageList]?
    var Offers : [OffersList]?
}
