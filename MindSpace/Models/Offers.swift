//
//  Offers.swift
//  MindSpace
//
//  Created by webwerks on 01/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct  OffersData : Codable {
    var Status : Bool?
    var Message : String?
    var DataList : [CategoryList]?
    var TotalRecords : Int?
    var BannerList : [BannerListing]?
}

struct  CategoryList : Codable {
    var OfferTypeName : String?
    var OfferList : [OffersList]?
}

struct  OffersList : Codable {
    var OfferId : Int?
    var OfferTitle : String?
    var OfferDesc : String?
    var ExpiryDate : String?
    var OfferTypeId : Int?
    var FNBOutletId : Int?
    var FNBOutletName : String?
    var ComplexName : String?
    var ComplexId : Int?
    var BannerImage : String?
//    var ImageURLs : [ImageList] = []
}

struct  OfferDetailData : Codable {
    var Status : Bool?
    var Message : String?
    var Data : OfferDetails?
    var BannerList : [BannerListing]?
}

struct  OfferDetails : Codable {
    var OfferId : Int?
    var OfferTitle : String?
    var OfferDesc : String?
    var OfferCode : String?
    var OfferTypeId : Int?
    var OfferTypeName : String?
    var FNBOutletId : Int?
    var FNBOutletName : String?
    var ExpiryDate : String?
    var BannerImage : String?
    var ImageUrls : [ImageList]?
}

