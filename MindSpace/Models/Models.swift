//
//  Models.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct UploadReqData : Codable {
    let Status : Bool?
    let Message : String?
}

struct SetInterested: Codable {
    var Status: Bool?
}

struct GetAccountStatus : Codable {
    var Status : Bool?
    var Message : String?
}
