//
//  AnnouncmentAnSCell.swift
//  MindSpace
//
//  Created by webwerks on 17/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class AnnouncmentAnSCell: UITableViewCell {
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var ansLabel: UILabel!
    @IBOutlet weak var progressView: CustomProgressView!
    @IBOutlet weak var percentageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
