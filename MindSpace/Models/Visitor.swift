//
//  Visitor.swift
//  MindSpace
//
//  Created by Zeeshan  on 19/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class Visitor: Codable {
    var DataList : VisitorInfo?
    var Status : Bool?
    var Message : String?
}

class VisitorInfo : Codable {
    var ComplexName : String?
    var TenantName : String?
    var DepartmentName : String?
    var MeetingPerson : String?
    var MeetingType : String?
    var MeetingTime : String?
    var VisitorId : Int?
    var VistorName : String?
    var VisitorRequestId : Int?
    var MobileNumber : String?
    var Email : String?
    var IsVehicle : Bool?
    var VehicleNumber : String?
    var IsBaggage : Bool?
    var IsLaptop : Bool?
    var LaptopNumber: String?
    var KYC: String?
}
