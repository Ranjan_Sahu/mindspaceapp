//
//  CarParkingViewController.swift
//  MindSpace
//
//  Created by Zeeshan  on 02/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class CarParkingViewController: UIViewController {
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgParking: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    
    private var imageList : [LightboxImage]?
    private var parkingDetails : [CarPArkingDetails]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnMore.isHidden = true
        self.getCarParkingDetails()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigation()
    }
    @IBAction func imageClicked(_ sender: UITapGestureRecognizer) {

        guard (self.imageList?.count)!>0 else{
            return;
        }
        let controller = LightboxController(images: self.imageList ?? [])
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
}
extension CarParkingViewController {
    private func getCarParkingDetails(){
        let parameter = [
            "entity" : ["ComplexId" : CommonHelper.getComplexID(),"TenantId" : CommonHelper.getTenantID()],
            "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.getParkingDetails, objectType: CarParkingData.self, isHud: true, controller: self, success: { [weak self] commonSpaceData in
            guard commonSpaceData.DataList != nil else{
                return
            }
            self?.parkingDetails = commonSpaceData.DataList
            self?.setData()
        })
    }
    private func setData(){
        //self.lblLocation?.text = self.parkingDetails?.ComplexName ?? ""
        let pathForFile = Bundle.main.path(forResource: "bannerPlaceholder", ofType: "png")
        let defaultImageURL = URL.init(string: pathForFile ?? "")
        self.imgParking.sd_setImage(with: URL.init(string: self.parkingDetails![0].ParkingMAP ?? ""), completed: nil)
        self.lblLocation.text = self.parkingDetails![0].BuildingName ?? ""
        self.imageList = self.parkingDetails?.map({ LightboxImage(imageURL: URL.init(string: $0.ParkingMAP ?? "") ?? defaultImageURL!, text: $0.BuildingName!, videoURL: nil) }) ?? []
        if (self.imageList?.count)!>1 {
            self.btnMore.setTitle("+\((self.imageList?.count)!-1)", for: .normal)
            self.btnMore.isHidden = false
        }
    }
    private func setupNavigation(){
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Car Parking"
    }
    
    @IBAction func moreACtion(_ sender: UIButton) {
        
        guard (self.imageList?.count)!>0 else{
            return;
        }
        let controller = LightboxController(images: self.imageList ?? [])
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
}
extension CarParkingViewController : LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

