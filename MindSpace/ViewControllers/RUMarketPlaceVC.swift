//
//  RUMarketPlaceVC.swift
//  MindSpace
//
//  Created by webwerks on 10/3/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUMarketPlaceVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "HomeScreenCollectionViewCell1"
    
    // MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    
    // MARK:- Objects
    var namesArray = [String]()
    var imgNameArray = [String]()
    var slides:[HomeScreenBannerView] = [];
    var bannerTimer:Timer? = nil
    var bannerList : [BannerListing] = []

    
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        namesArray = ["Post Ad", "Products on Sale", "Concierge Services"]
        namesArray = ["Post Ad", "Products on Sale"]

        
        imgNameArray = ["icon_post_ad", "icon_products_sale", "icon_concierge_service"]
        imgNameArray = ["icon_post_ad", "icon_products_sale"]

        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)

        self.view.layoutIfNeeded()
        // Banner Setup
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
        self.getBannerList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Marketplace"
        
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
}
// MARK: - CollectionView Delegates and DataSource
extension RUMarketPlaceVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return namesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
            cell.LBL_title?.text = namesArray[indexPath.row]
            //cell.backImgView?.image = UIImage(named: imgNameArray[indexPath.row])
            cell.imgView.image = UIImage(named: imgNameArray[indexPath.row])
            cell.alpha = 0.2         //assigning the animation
            cell.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            UIView.animate(withDuration: 1.0, animations: {
                cell.alpha = 1
                cell.transform = .identity
            })
                return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath.row == 1) {
            let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "RUBuyVC") as! RUBuyVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
        else if (indexPath.row == 2) {
            let targetVC = Storyboard.ruConciergeServicesVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        } else if (indexPath.row == 0){
            let targetVC = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "PostAdViewController") as! PostAdViewController
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
        
//        else if (indexPath.row == 2) {
//            let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "RURentingVC") as! RURentingVC
//            self.navigationController?.pushViewController(targetVC, animated: true)
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (self.collectionView.bounds.size.width-4)/2
        //let h = (self.collectionView.bounds.size.height-6)/3
        let size = CGSize.init(width: w - 20, height: w - 20)
        
//        let w = (self.collectionView.bounds.size.width-6)/3
//        //let h = (self.collectionView.bounds.size.height-6)/3
//        let size = CGSize.init(width: w, height: w + 20)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
    
    // MARK:- Custom methods
    
    func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        
        if ((self.bannerList.count)>0) {
            for item in self.bannerList {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                let imgStr = item.BannerImage
                slide.bannerImgView.sd_setImage(with: URL(string: imgStr!), placeholderImage: UIImage(named: "bannerPlaceholder"))
                bannerVwArray.append(slide)
            }
        } else {
            let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
            slide.bannerImgView.image = UIImage(named: "bannerPlaceholder")
            bannerVwArray.append(slide)
        }
        return bannerVwArray
    }
    
    func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
    
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
}
extension RUMarketPlaceVC : UIScrollViewDelegate{
    // MARK:- ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }
    
}

extension RUMarketPlaceVC{
    // MARK:- Service Calls
    func getBannerList(){
        
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "20"],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetMarketPlaceBannerList, objectType: BannerListingData.self, isHud: true, controller: self, success: { [weak self] bannerData in
            print("***************")
            print(bannerData)
            self?.bannerList = bannerData.DataList!
            self?.setUpBanner()

           
        })
    }
    
    private func setUpBanner() {
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
    }
}

