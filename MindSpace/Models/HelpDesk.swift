//
//  HelpDesk.swift
//  MindSpace
//
//  Created by Zeeshan  on 02/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class HelpDesk : Codable {
    var DataList : [HelpDeskDetails]?
    var Status : Bool?
    var Message: String?
}

class HelpDeskDetails : Codable {
    var HelpDeskId : Int?
    var Ambulance : String?
    var Hospital : String?
    var FireBrigade : String?
    var Police : String?
    var HelpDeskCategoryName : String?
    var HelpDeskCategoryId : Int?
    var AssemblyPoint : String?
    var ReachUs : String?
    var VisitorAssistance : String?
    var Icon : String?
}
