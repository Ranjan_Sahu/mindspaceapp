//
//  GalleryDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox
class GalleryDetailsVC: UIViewController {
    @IBOutlet weak var menuCollectionView: ScalingCarouselView!
    @IBOutlet weak var imagesCountLabel: UILabel!
    @IBOutlet weak var imagesDescLabel: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    let cellIdentifier = "GalleryCollectionViewCell"

    private var galleryDetails : GalleryDetails?
    private var imageCount  : Int?
    private var imageDescription  : String?
    private var imageList : [LightboxImage]?
    public  var albumID     : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        callGalleryDetails ()
    }
}

extension GalleryDetailsVC {
    //MARK:- Webservice Call
    private func callGalleryDetails (){
        let parameters = ["pageInfo":["PageIndex":"1","PageSize":"20"],
            "entity":["AlbumId":self.albumID ?? 0],
            "deviceInfo": CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetGalleryDetails, objectType: GalleryData.self , isHud: true, controller: self, success: { [weak self] galleryDetails in
            guard let details = galleryDetails.Data else {
                CommonHelper.showAlert(vc: self!, title: "Error", msg: Messages.noData)
                self?.view.isHidden = true
                return
            }
            self?.galleryDetails = details
            self?.setData()
            self?.navigationItem.title = self?.galleryDetails?.AlbumName
        })

    }
    private func setData(){
        self.imageCount = self.galleryDetails?.ImageUrls?.count
        self.imagesCountLabel?.text = "Image 1 of \(String(describing: self.imageCount ?? 1))"
       // self.imageDescription = self.galleryDetails?.Description
        self.imagesDescLabel?.text = self.galleryDetails?.Description
        self.menuCollectionView.reloadData()
        self.lblTitle.text = self.galleryDetails?.AlbumName ?? ""
        self.lblSubTitle.text = self.galleryDetails?.ComplexName ?? ""
        //self.imageList = self.galleryDetails?.ImageUrls?.map({ LightboxImage(imageURL: URL.init(string:  $0.ImageName ?? "" ) ?? URL(fileURLWithPath: ""))})
        self.imageList = self.galleryDetails?.ImageUrls?.map({ LightboxImage(imageURL: URL.init(string:  $0.ImageName ?? "" ) ?? URL(fileURLWithPath: ""), text: $0.ImageTitle ?? "" , videoURL: nil)})
        self.setDate()
    }
    private func setDate(){
        if let date = self.galleryDetails?.Date?.components(separatedBy: "/") {
            self.lblDate.text = date[0]
            self.lblMonth.text = date[1]

        }
    }
    
}
extension GalleryDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let itemsCount = self.galleryDetails?.ImageUrls?.count else { return 0 }
        return itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! GalleryCollectionViewCell
        let imageInfo = self.galleryDetails?.ImageUrls![indexPath.row]
        if  let url:URL = URL(string: (imageInfo?.thumbnail)!) {
            cell.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "bannerPlaceholder"))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList ?? [], startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = menuCollectionView.currentCenterCellIndex?.row ?? 0
        imagesCountLabel?.text = "image \(index + 1) of \(imageCount ?? 0)"
        self.imagesDescLabel?.text = self.galleryDetails?.Description
    }
}

extension GalleryDetailsVC :  LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}
