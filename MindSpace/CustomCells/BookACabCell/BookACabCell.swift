//
//  BookACabCell.swift
//  MindSpace
//
//  Created by webwerks on 10/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class BookACabCell: UITableViewCell {

    @IBOutlet weak var LBL_title: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
