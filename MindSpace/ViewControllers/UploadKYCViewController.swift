//
//  UploadKYCViewController.swift
//  MindSpace
//
//  Created by Zeeshan  on 16/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Alamofire
class UploadKYCViewController: UIViewController {
    
    private let cellIdentifier  = "UploadKYC2TableViewCell"
    //    private let cellIdentifier1 = "VisitorRequestCell1"
    
    @IBOutlet weak var tableView: RoundTableView!
    @IBOutlet weak var imageeUploaduploadImageView: UIImageView!
    @IBOutlet weak var uploadFileView: UIView!
    
    private var imagePicker = UIImagePickerController()
    private var selectedKYCImage : UIImage?
    private var visitorDetails : VisitorInfo?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setController()
        self.tableView.tableHeaderView = uploadFileView
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .highlighted)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
}
//MARK:- Actions
extension UploadKYCViewController {
    @IBAction func uploadFileClicked(_ sender: UIButton) {
        self.setupPicker()
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func submitClicked(_ sender: UIButton) {
        guard selectedKYCImage != nil else { CommonHelper.showAlert(vc: self, title: "error", msg: "Please add KYC file")
            return
        }
        self.callUploadKYC()
        
    }
}

extension UploadKYCViewController {
    //MARK:- Class Methods
    private func setController(){
        navigationItem.title = "Upload KYC"
        self.setupRegisterCell()
        getVisitorDetails()
    }
    private func setupRegisterCell(){
        self.tableView.register(UINib(nibName: "UploadKYC2TableViewCell", bundle: nil) , forCellReuseIdentifier: "UploadKYC2TableViewCell")
        //     self.tableView.register(UINib.init(nibName: cellIdentifier1, bundle: nil) , forCellReuseIdentifier: cellIdentifier1)
    }
    private func setupPicker(){
        self.imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType    = .photoLibrary
    }
    
    //MARK:- Webservices
    private func getVisitorDetails(){
        let parameters = ["entity":["VisitorId" : AppDelegate.appDelegateShared.user?.VisitorId  ?? 0],
                          "deviceInfo":CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getVisitorInfo, objectType: Visitor.self, isHud: true, controller: self, success: {[weak self] response in
            self?.visitorDetails = response.DataList
            if ((self?.visitorDetails?.KYC) != nil) {
                self?.imageeUploaduploadImageView.sd_setImage(with: URL.init(string: self?.visitorDetails?.KYC ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
            }
            self?.tableView.reloadData()
        })
    }
    
    private func callUploadKYC(){
        let parameters = ["VisitorId" :
            String(describing: AppDelegate.appDelegateShared.user?.VisitorId ?? 0) ,
                          "DeviceTypeId" : "2",
                          "UDID": "ugoktjhierjhdiuhgidrigh",
                          "Timestamp": "487486464654"]
        let imageArray = ["KYC" : self.selectedKYCImage] as! [String:UIImage] //"ProfilePhoto", Document
        Webservice.uploadFiles(imageArray: imageArray, parameters: parameters, url: WebserviceURLs.uploadKYC, objectType: UploadReqData.self, controller: self, success: {response in
            Constants.showAlertWithMessage(message: response.Message ?? "", presentingVC: self, okBlock: { _ in
                if response.Status == true{
                    self.navigationController?.popViewController(animated: true)
                }
            })
        })
    }
}

extension UploadKYCViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //     if indexPath.section == 0 {
        let cell: UploadKYC2TableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? UploadKYC2TableViewCell
        cell.selectionStyle = .none
        cell.LBL_tenantName.text = self.visitorDetails?.TenantName
        cell.LBL_depatrment.text = self.visitorDetails?.DepartmentName
        cell.LBL_meetingPerson.text = self.visitorDetails?.MeetingPerson
        cell.LBL_meetingType.text = self.visitorDetails?.MeetingType
        cell.LBL_meetingTime.text = self.visitorDetails?.MeetingTime
        cell.LBL_firstName.text = self.visitorDetails?.VistorName
        cell.LBL_emailId.text = self.visitorDetails?.Email
        cell.LBL_visitorMobileNymber.text = self.visitorDetails?.MobileNumber
        if visitorDetails?.IsBaggage == true{
          cell.LBL_baggage.text = "Yes"
        }
       //
        //            if self.visitorDetails?.MeetingType == "Scheduled" {
        //                cell.scheduledBtn.isSelected = true
        //                cell.adhocBtn.isSelected = false
        //            }
        //            else {
        //                cell.scheduledBtn.isSelected = false
        //                cell.adhocBtn.isSelected = true
        //            }
        //  cell.timeTxtField.text = self.visitorDetails?.MeetingTime
        return cell
        //    }
        //        else {
        //            let cell: VisitorRequestCell1 = tableView.dequeueReusableCell(withIdentifier: cellIdentifier1) as! VisitorRequestCell1
        //            cell.selectionStyle = .none
        //            cell.setDetails = self.visitorDetails
        //            return cell
        //        }
    }
    
    
}
extension UploadKYCViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.selectedKYCImage = pickedImage
            DispatchQueue.main.async {
                self.imageeUploaduploadImageView.image = nil
                self.imageeUploaduploadImageView.image =  pickedImage
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

