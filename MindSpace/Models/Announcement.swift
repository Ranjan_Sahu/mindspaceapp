//
//  AnnouncementModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  AnnouncementListData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [AnnouncementList]?
    let TotalRecords : Int?
    //    var Data : [AnnouncementDetails]?
}

struct  AnnouncementList : Codable {
    let AnnouncementId : Int?
    let AnnouncementTitle : String?
    let AnnouncementDesc : String?
    let FloorNumber : String?
    let BuildingName : String?
    let ComplexId : Int?
    let ComplexName : String?
    let ImageURLs : [String]?
    let Date : String?
}

struct  AnnouncementDetailsData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : AnnouncementDetails?
}

struct  AnnouncementDetails : Codable {
    let AnnouncementTitle : String?
    let AnnouncementDesc : String?
    let FloorId : Int?
    let FloorNumber : String?
    let BuildingId : Int?
    let BuildingName : String?
    let ComplexId : Int?
    let ComplexName : String?
    let BannerImage : String?
    let ImageUrls : [String]?
}

