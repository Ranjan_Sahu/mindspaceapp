//
//  EventListTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 9/28/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {

    @IBOutlet weak var eventBannerImgView: UIImageView!
    @IBOutlet weak var LBL_eventTitle: UILabel!
    @IBOutlet weak var LBL_eventLocation: UILabel!
    @IBOutlet weak var LBL_eventTime: UILabel!
    @IBOutlet weak var icon_time: UIImageView!
    @IBOutlet weak var eventDateIconView: UIView!
    @IBOutlet weak var LBL_month: UILabel!
    @IBOutlet weak var LBL_date: UILabel!
    @IBOutlet weak var LBL_subText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.eventDateIconView.layer.cornerRadius = 6.0
        self.eventDateIconView.layer.borderWidth = 1.0
        let color = CommonHelper.hexStringToUIColor(hex: "999999")
        self.eventDateIconView.layer.borderColor = color.cgColor
    }

}
