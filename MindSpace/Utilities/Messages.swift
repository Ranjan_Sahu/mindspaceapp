//
//  Messages.swift
//  MindSpace
//
//  Created by Zeeshan  on 15/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct Messages {
    static let title = "Mindspace"
    static let noData = "Unable to load data. Please try again later."
    static let uploadKYC = "Please select a file to submit"
    static let kycSuccessful = "KYC File uploaded successfully"
    static let pdfNotFound = "File Not Found"
}

struct Warnings {
    static let Field_Empty=" should not be empty"
    static let Invalid_Email="Invalid Email Address"
    static let Invalid_Phone="Invalid Phone Number"
    static let Invalid_Otp="Invalid OTP"
    static let errorMessage = "Oops something went wrong! Please try again"
}
