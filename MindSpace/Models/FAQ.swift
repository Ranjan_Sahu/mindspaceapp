//
//  FAQModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  FAQData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [FAQListData]?
    let TotalRecords : Int?
}

struct  FAQListData : Codable {
    let FAQId : Int?
    let QuestionText : String?
    let AnswerText : String?
}
