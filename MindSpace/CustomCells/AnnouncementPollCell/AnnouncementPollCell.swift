//
//  AnnouncementPollCell.swift
//  MindSpace
//
//  Created by webwerks on 10/16/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

protocol PollQuesDelegates {
    func didSelectAnswer(selectedAns:PollAnswerList,selectedCellNo:Int)
}

class AnnouncementPollCell: UITableViewCell {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var pollCategory: UILabel!
    var pollDelegate : PollQuesDelegates? = nil
    var cellNumber:Int = 0
    
    @IBOutlet weak var tablHeightConstraint: NSLayoutConstraint!
     let cellIdentifier = "PollQuestionCell"

    var dataSource : [PollAnswerList] = [] {
        didSet {
            setupTable()
        }
    }
    
    func setupTable()  {
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 420
        tableview.reloadData()
    }
    
}

extension AnnouncementPollCell : UITableViewDelegate,UITableViewDataSource{
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PollQuestionCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PollQuestionCell
        let pollDetails = dataSource[indexPath.row]
        cell.radioBtn.isHidden = false
        cell.ansLabel.text = pollDetails.AnswerText
        cell.selectionStyle = .none
        return cell
}
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let cell:PollQuestionCell = tableView.cellForRow(at: indexPath) as! PollQuestionCell
        cell.radioBtn.isSelected = true
        let pollDetails = dataSource[indexPath.row]
        self.pollDelegate?.didSelectAnswer(selectedAns: pollDetails, selectedCellNo: cellNumber)
    }

}
