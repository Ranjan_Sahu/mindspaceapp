//
//  ACommonSpaceDetailsCell.swift
//  MindSpace
//
//  Created by webwerks on 10/15/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox


class ACommonSpaceDetailsCell: UITableViewCell {

    @IBOutlet weak var LBL_description: UILabel!
    @IBOutlet weak var LBL_peopleCount: UILabel!
    @IBOutlet weak var LBL_title: UILabel!
    @IBOutlet weak var LBL_buildingNo: UILabel!
    @IBOutlet weak var LBL_timing: UILabel!
    @IBOutlet weak var LBL_bookThrough: UILabel!
    @IBOutlet weak var LBL_contactPerson: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tapGestureForCall: UITapGestureRecognizer!
    @IBOutlet weak var viewButton: RoundButton!

    var parent : UIViewController?
    private var imageList : [LightboxImage] = []
    let cellIdentifier = "MenuCollectionViewCel"


    var dataSource : [String] = [] {
        didSet {
            collectionView.reloadData()
            setImageViewerDataSource()
        }
    }
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView?.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func setImageViewerDataSource() {
        self.imageList = dataSource.map({ (imageUrl) -> LightboxImage in
            if imageUrl.count > 0 {
               return LightboxImage(imageURL: URL(string: imageUrl)!)
            }
            return LightboxImage(image: #imageLiteral(resourceName: "bannerPlaceholder"))
            
        })
    }
    
}
extension ACommonSpaceDetailsCell : UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MenuCollectionViewCel
         let img = dataSource[indexPath.row]
        cell.imgView.sd_setImage(with: URL.init(string: img), placeholderImage:  #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.continueInBackground, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
       parent?.present(controller, animated: true, completion: nil)
    }
    
}
extension ACommonSpaceDetailsCell : LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}
