
//
//  ProductDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 17/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class ProductDetailsVC: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var prizeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var interestedButton: UIButton!
    
    let cellIdentifier = "MenuCollectionViewCell"

    var productID : Int?
    private var productDetails : ProductDetails?
    private var imageList: [LightboxImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callProductDetails()
        self.backImageView.layer.cornerRadius = 4.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func onClickInterestedButton(_ sender: Any) {
        guard let idP = productID else {
            return
        }
        let parameters = ["entity": ["ClassifiedId" : idP],  "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_SetInterested, objectType: SetInterested.self, isHud: true, controller: self) { [weak self] response in
            if response.Status == true {
                self?.interestedButton.isHidden = true
                CommonHelper.showAlert(vc: self!, title: "Message", msg: "Your request has been sent successfully.")
            }
        }
    }
}

extension ProductDetailsVC {
    //MARK:- Webservice
    private func callProductDetails(){
        guard let idP = productID else {
            return
        }
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["ClassifiedId" : idP],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetProductDetails, objectType: ProductDetailsData.self, isHud: true, controller: self, success: {[weak self]
            response in
            guard let details = response.Data else {return}
            self?.productDetails = details
            self?.menuCollectionView.reloadData()
            self?.refreshUI()
            self?.setData()
            if (self?.productDetails?.Interested) != nil {
                if !(self?.productDetails?.Interested == "No") {
                    self?.interestedButton.isHidden = true
                }
            }
        })
    }
    
    //MARK:- Class Methods
    private func refreshUI()  {
        self.nameLabel?.text = productDetails?.Title?.capitalized ?? ""
        self.timeLabel?.text = productDetails?.PostedOn?.capitalized ?? ""
        self.descriptionLabel?.text = productDetails?.Description?.capitalized ?? ""
        self.prizeLabel.text = "₹ \(productDetails?.Price ?? 0)"
        self.title = productDetails?.Title?.capitalized ?? ""
        if let image = productDetails?.ImageUrls?.first?.ImageName {
            backImageView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
    }
    
    private func setData(){
        self.imageList = self.productDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0.ImageName!)!) }) ?? []
        
    }
    
    private func registerCell() {
        self.menuCollectionView?.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCollectionViewCell")
    }

}

//MARK:- Collectionview Datasource
extension ProductDetailsVC : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.productDetails?.ImageUrls?.count else { return 0 }
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        if let image = self.productDetails?.ImageUrls?[indexPath.row].thumbnail {
            cell.imgView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        return cell
    }
}
extension ProductDetailsVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
}

extension ProductDetailsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.menuCollectionView.frame.height
        return CGSize(width: height, height: height)
    }
}

extension ProductDetailsVC : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
    }
}
