//
//  MenuCollectionViewCell.swift
//  MindSpace
//
//  Created by webwerks on 09/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var imgView: UIImageView!
}
