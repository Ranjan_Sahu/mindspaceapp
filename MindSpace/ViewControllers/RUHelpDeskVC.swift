//
//  RUHelpDeskVC.swift
//  MindSpace
//
//  Created by webwerks on 10/3/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUHelpDeskVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "HomeScreenCollectionViewCell1"
    
    // MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK:- Objects
    //    private var namesArray = [String]()
    //    private var imgNameArray = [String]()
    private var helpDesk : [HelpDeskDetails]?
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self;
        self.webserviceCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Help Desk"
        
    }
}
extension RUHelpDeskVC {
    //MARK:- Webservice Calls
    private func webserviceCall() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "10"],
                         "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.getHelpDeskList, objectType: HelpDesk.self, isHud: true, controller: self, success: { [weak self] helpDeskData in
            self?.helpDesk = helpDeskData.DataList
            self?.collectionView.reloadData()
            
            
        })
    }
}

// MARK: - CollectionView Delegates and DataSource
extension RUHelpDeskVC : UICollectionViewDelegate, UICollectionViewDataSource,  UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.helpDesk?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
        let helpData = self.helpDesk?[indexPath.row]
        cell.LBL_title.text = helpData?.HelpDeskCategoryName // namesArray[indexPath.row]
        cell.imgView.image = self.getIcon(helpDeskName: (helpData?.HelpDeskCategoryName)!)
        cell.alpha = 0.2         //assigning the animation
        cell.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 1.0, animations: {
            cell.alpha = 1
            cell.transform = .identity
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let catergoryID = self.helpDesk![indexPath.row].HelpDeskCategoryId
        switch catergoryID {
        case 1:
            let vc = Storyboard.ruEmergencyContactsVC
            vc.ambulanceNoStr =  self.helpDesk![indexPath.row].Ambulance ?? ""
            vc.policeNoStr =  self.helpDesk![indexPath.row].Police ?? ""
            vc.hospitalNoStr =  self.helpDesk![indexPath.row].Hospital ?? ""
            vc.fireBrigadeNoStr =  self.helpDesk![indexPath.row].FireBrigade ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2: self.assemblyPoint(url: self.helpDesk![indexPath.row].AssemblyPoint ?? "")
            //            let vc = Storyboard.assemblyPointViewController
            //            vc.url = self.helpDesk![indexPath.row].AssemblyPoint
            //            self.navigationController?.pushViewController(vc, animated: true)
            
        case 3:
            let vc = Storyboard.contactViewController
            vc.email = self.helpDesk![indexPath.row].ReachUs
            vc.titleStr = "Reach Us"
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 4:
            let vc = Storyboard.contactViewController
            vc.titleStr = "Visitor Assistance"
            vc.email = self.helpDesk![indexPath.row].VisitorAssistance
            self.navigationController?.pushViewController(vc, animated: true)
        case 5:
            let vc = Storyboard.ruFAQVC
            //            vc.titleStr = "FAQ"
            //            vc.email = self.helpDesk![indexPath.row].VisitorAssistance
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 6:
            let vc = Storyboard.supportVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }
    private func assemblyPoint(url : String){
        SVProgressHUD.show()
        if let remotePDFDocumentURL = URL(string: url), let doc = PDFDocument(url: remotePDFDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Assembly Point"
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func getIcon(helpDeskName:String)->UIImage{
        switch helpDeskName {
        case "Visitor Assistance":
            return UIImage.init(named: "icon_visitorassist")!
        case "Emergency Contacts":
            return UIImage.init(named: "icon_emergency")!
        case "FAQ":
            return UIImage.init(named: "icon_faq")!
        case "Support":
            return UIImage.init(named: "icon_support")!
        case "Reach US":
            return UIImage.init(named: "icon_reachus")!
        default:
            return UIImage.init(named: "icon_poibts")!
        }
    }
}
extension RUHelpDeskVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (self.collectionView.bounds.size.width-4)/2
        //let h = (self.collectionView.bounds.size.height-6)/3
        let size = CGSize.init(width: w - 20, height: w - 20)
        return size
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
}


