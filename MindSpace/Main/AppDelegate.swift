//
//  AppDelegate.swift
//  MindSpace
//
//  Created by webwerks on 9/24/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Toast_Swift

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var appDelegateShared = UIApplication.shared.delegate as! AppDelegate
    var reachability    : Reachability = Reachability.networkReachabilityForInternetConnection()!
    var isReachable     : Bool?        = false
    var user            : User? = User()
    private var sysDataInfo : SystemDataInfo? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        // Nav view Bg
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().backgroundColor = CommonHelper.hexStringToUIColor(hex: "1c5ab0")
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-1000, 0), for:UIBarMetrics.default)
        UINavigationBar.appearance().tintColor = UIColor.white
        let font:UIFont = UIFont.init(name: "Montserrat-Medium", size: 20.0)!
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font: font]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.clear], for: UIControlState.normal)
        
        let backImg = UIImage(named: "nav_icon_back")
        UINavigationBar.appearance().backIndicatorImage = backImg
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImg
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.statusBarView?.backgroundColor = CommonHelper.hexStringToUIColor(hex: "1c5ab0")
        
        // Customize SVProgressHUD Appearance
        SVProgressHUD.setBackgroundColor(UIColor.groupTableViewBackground)
        SVProgressHUD.setRingRadius(2.0)
        SVProgressHUD.setForegroundColor(CommonHelper.hexStringToUIColor(hex: "3DAFA9"))
        
        // Customize toastview
        ToastManager.shared.style.backgroundColor = CommonHelper.hexStringToUIColor(hex: "1C5AB0")
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityDidChange(_:)), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        checkLoggedInUser()
//        isReachable = reachability.currentReachabilityStatus == .notReachable ? false : true        
//        _ = reachability.startNotifier()
        return true
    }
    
    func checkReachability() {
        if reachability.isReachable  {
            isReachable = true
        } else {
            isReachable = false
        }
    }
    
    func checkLoggedInUser() {
        let user = User.init()
        if (((user.UserId != nil) && user.UserId != 0) || ((user.VisitorId != nil) && user.VisitorId != 0)) {
            
            let parameters = ["pageInfo": ["PageIndex" : "1",
                                           "PageSize" : "20"],
                              "entity" : ["ConfigurationId" : "4"],
                              "deviceInfo" : CommonHelper.getDeviceInfo()
            ]
            Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetSystemConfiguration, objectType: SystemData.self, isHud: false, controller: UIViewController(), success: {
                response in
                guard (response.Data?.AboutUs) != nil else {return}
                self.sysDataInfo = response.Data
                
                UserDefaults.standard.set(self.sysDataInfo?.FacebookLink, forKey: "FacebookLink")
                UserDefaults.standard.set(self.sysDataInfo?.TwitterLink, forKey: "TwitterLink")
                UserDefaults.standard.set(self.sysDataInfo?.OlaLink, forKey: "OlaLink")
                UserDefaults.standard.set(self.sysDataInfo?.UberLink, forKey: "UberLink")
                UserDefaults.standard.set(self.sysDataInfo?.ConfigurationId, forKey: "ConfigurationId")
                UserDefaults.standard.set(self.sysDataInfo?.OlaIOSAppLink, forKey: "OlaIOSAppLink")
                UserDefaults.standard.set(self.sysDataInfo?.OlaIOSStoreLink, forKey: "OlaIOSStoreLink")
                UserDefaults.standard.set(self.sysDataInfo?.OlaAndroidAppLink, forKey: "OlaAndroidAppLink")
                UserDefaults.standard.set(self.sysDataInfo?.OlaAndroidStoreLink, forKey: "OlaAndroidStoreLink")
                UserDefaults.standard.set(self.sysDataInfo?.UberIOSAppLink, forKey: "UberIOSAppLink")
                UserDefaults.standard.set(self.sysDataInfo?.UberIOSStoreLink, forKey: "UberIOSStoreLink")
                UserDefaults.standard.set(self.sysDataInfo?.UberAndroidAppLink, forKey: "UberAndroidAppLink")
                UserDefaults.standard.set(self.sysDataInfo?.UberAndroidStoreLink, forKey: "UberAndroidStoreLink")
                UserDefaults.standard.set(self.sysDataInfo?.CreatedOn, forKey: "CreatedOn")
                UserDefaults.standard.set(self.sysDataInfo?.BannerSlideTime, forKey: "BannerSlide_Time")
                
                UserDefaults.standard.synchronize()
            })
            
            var targetVC:UIViewController? = nil
            
            /*if user.UserType == "User" {
                targetVC = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
            }
            else if user.UserType == "Visitor"{
                targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "VisitorTabBarController") as! VisitorTabBarController
            }*/
            
             targetVC = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
            
            let navVC = UINavigationController.init(rootViewController: targetVC!)
            navVC.isNavigationBarHidden = true
            self.window?.rootViewController = navVC
            return
        }
    }

    @objc func reachabilityDidChange(_ notification: Notification) {
        checkReachability()
    }
}

