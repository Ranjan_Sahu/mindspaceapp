//
//  RUSocialVC.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUSocialVC: UIViewController {

    // Constants
    private let fbPageUrl = "https://www.facebook.com/pages/Accenture-Mindspace-Airoli/261299010566918"
    private let twitterPgUrl = "https://twitter.com/mindspaceme?lang=en"
    
    // Outlets
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var wbview: UIWebView!
    @IBOutlet weak var shadowView: UIView!
    public var sysDataInfo : SystemDataInfo? = nil

    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSystemConfiguration()
        self.addLeftMenuButton()
        self.wbview.loadRequest(URLRequest(url: URL(string:fbPageUrl )!))
        self.wbview.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Social"
    }
}
extension RUSocialVC {
    
    // MARK: - Button Action
    @IBAction func facebookBtnAction(_ sender: Any) {
        facebookBtn.isSelected = true
        twitterBtn.isSelected = false
        self.wbview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        self.wbview.loadRequest(URLRequest(url: URL(string: fbPageUrl)!))
    }
    @IBAction func twitterBtnAction(_ sender: Any) {
        facebookBtn.isSelected = false
        twitterBtn.isSelected = true
        self.wbview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        self.wbview.loadRequest(URLRequest(url: URL(string: self.sysDataInfo?.TwitterLink ?? "")!))
    }
}
//MARK:- Webview Delegates
extension RUSocialVC : UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}

extension RUSocialVC{
    func getSystemConfiguration() {
        
        let parameters = ["pageInfo": ["PageIndex" : "1",
                                       "PageSize" : "20"],
                          "entity" : ["ConfigurationId" : "4"],
                          "deviceInfo" : CommonHelper.getDeviceInfo()
        ]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetSystemConfiguration, objectType: SystemData.self, isHud: true, controller: self, success: {
            response in
           // guard let aboutUs = response.Data?.AboutUs else {return}
            self.sysDataInfo = response.Data
            print(self.sysDataInfo?.FacebookLink)
            print(self.sysDataInfo?.TwitterLink)
        })
    }
}
