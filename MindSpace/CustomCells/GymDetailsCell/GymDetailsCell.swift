//
//  GymDetailsCell.swift
//  MindSpace
//
//  Created by webwerks on 10/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class GymDetailsCell: UITableViewCell {

    
    @IBOutlet weak var LBL_gymTitle: UILabel!
    @IBOutlet weak var LBL_contactPerson: UILabel!
    @IBOutlet weak var LBL_bookThrough: UILabel!
    @IBOutlet weak var LBL_buildingNo: UILabel!
    @IBOutlet weak var LBL_peopleCount: UILabel!
    @IBOutlet weak var LBL_gymTiming: UILabel!
    @IBOutlet weak var LBL_gymDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
