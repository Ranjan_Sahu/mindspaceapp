//
//  CrecheDetailsViewController.swift
//  MindSpace
//
//  Created by Zeeshan  on 02/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class CrecheDetailsViewController: UIViewController {
    
    @IBOutlet weak var bookingDetailLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    var crecheID : Int?
    private var crecheDetails : CrecheDetails?
    private var imageList: [LightboxImage] = []
    private var menuCardURLArray : [LightboxImage] = []
    var contactNo : String = ""
    var bannerImageName : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callCrecheDetails()
        
        bookingDetailLbl.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(CrecheDetailsViewController.makeCall))
        bookingDetailLbl.addGestureRecognizer(tap)
    }
    
    @objc func makeCall(){
        if let url = URL(string: "tel://\(crecheDetails?.ContactDetails ?? "")"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onClickViewButton(_ sender: Any) {
        showMenuCard()
    }
}

extension CrecheDetailsViewController {
    //MARK:- Webservice
    private func callCrecheDetails(){
        guard let idC = crecheID else {
            return
        }
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["CrecheId" : idC],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getCrecheDetails, objectType: CrecheListData.self, isHud: true, controller: self, success: {[weak self]
            response in
            guard let details = response.Data else {return}
            self?.crecheDetails = details
            self?.collectionView.reloadData()
            self?.refreshUI()
            self?.setData()
        })
    }
    //MARK:- Class Methods
    private func refreshUI()  {
        self.titleLbl?.text = crecheDetails?.Title ?? ""
        self.timeLbl?.text = crecheDetails?.Time ?? ""
        self.descriptionLbl.text = crecheDetails?.Description ?? ""
        self.bookingDetailLbl.text = "Contact \(crecheDetails?.ContactDetails ?? "")"
        self.contactNo = crecheDetails?.ContactDetails ?? ""
        
        let tapGestureForCall = UITapGestureRecognizer(target: self, action:#selector(self.handleTap))
        bookingDetailLbl.addGestureRecognizer(tapGestureForCall)
        
        self.title = crecheDetails?.Title ?? ""
        if bannerImageName != nil {
            bannerImageView.sd_setImage(with: URL.init(string: bannerImageName ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let contactno1 = String(contactNo.suffix(10)).trimmingCharacters(in: .whitespaces)
        print(contactno1)
        print(contactNo)
        if let url = URL(string: "tel://\(contactno1)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    private func setData(){
        self.imageList = self.crecheDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0.ImageName!)!) }) ?? []
    }
    private func registerCell() {
        self.collectionView?.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCollectionViewCell")
    }
}
//MARK:- Collectionview Datasource
extension CrecheDetailsViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.crecheDetails?.ImageUrls?.count else { return 0 }
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        if let image = self.crecheDetails?.ImageUrls?[indexPath.row].thumbnail {
            cell.imgView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        return cell
    }
}
extension CrecheDetailsViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
}
extension CrecheDetailsViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}

extension CrecheDetailsViewController : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
}
extension CrecheDetailsViewController {
    func showMenuCard() {
        if let menuCardURL = crecheDetails?.BookingProcessDocument {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    private func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Business Process Document"
        navigationController?.pushViewController(controller, animated: true)
    }
}
