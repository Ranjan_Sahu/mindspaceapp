//
//  PostAdd.swift
//  MindSpace
//
//  Created by webwerks on 12/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class ProductCategoryListData : Codable {
    var Status : Bool?
    var Message : String?
    var DateList : [ProductCategoryList]?
}

class ProductCategoryList : Codable {
    var ProductCategoryId : Int?
    var ProductCategoryName : String?
}
