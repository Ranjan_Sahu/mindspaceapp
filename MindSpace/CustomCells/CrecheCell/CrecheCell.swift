//
//  CrecheCell.swift
//  MindSpace
//
//  Created by webwerks on 09/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class CrecheCell: UITableViewCell {
    @IBOutlet weak var eventBannerImgView: UIImageView!
    @IBOutlet weak var LBL_eventTitle: UILabel!
    @IBOutlet weak var LBL_eventLocation: UILabel!
    @IBOutlet weak var LBL_eventTime: UILabel!
    @IBOutlet weak var icon_time: UIImageView!
    @IBOutlet weak var LBL_subText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
