//
//  CustomNavViewController.swift
//  MindSpace
//
//  Created by webwerks on 04/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class CustomNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        switch self.topViewController {
        case  is SelectPremiseVC, is LoginScreenVC, is OTPScreenVC:
            return UIStatusBarStyle.default
        default:
            return UIStatusBarStyle.lightContent
        }
    }
    
}
