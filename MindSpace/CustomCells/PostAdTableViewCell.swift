//
//  PostAdTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 12/6/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
protocol PostAdCellDelegate {
    func uploadButtonTapped(imageType : Int)
}
class PostAdTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryTxtField: RoundTextField!
    @IBOutlet weak var productTitleTxtField: RoundTextField!
    @IBOutlet weak var descriptionTxtFeild: RoundTextField!
    @IBOutlet weak var priceTxtField: RoundTextField!
    @IBOutlet weak var viewHeightConstaraintForImages: NSLayoutConstraint!
    @IBOutlet weak var imgVw1: UIImageView!
    @IBOutlet weak var imgVw2: UIImageView!
    @IBOutlet weak var imgVw3: UIImageView!
    @IBOutlet weak var imgVw4: UIImageView!
    @IBOutlet weak var imgVw5: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    
//    var postCellDelegate : PostAdCellDelegate?
//    private var imagePicker = UIImagePickerController()
//    private var imageType : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
//    @IBAction func uploadBTNTapped(_ sender: Any) {
//        self.imageType = 0
//    }
}
