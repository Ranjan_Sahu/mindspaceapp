//
//  LoginScreenVC.swift
//  MindSpace
//
//  Created by webwerks on 9/24/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class LoginScreenVC: UIViewController {

    // MARK:- Constants
    
    // MARK:-  Outlets
    @IBOutlet weak var TF_UserId: UITextField!
    @IBOutlet weak var BTN_Register: UIButton!
    
    // MARK:-  Objects
    var user : UserData? = nil
    var otpText : String? = ""
    
    // MARK:-  Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TF_UserId.delegate = self
//        self.TF_UserId.text = "9769000600"
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(sender:)))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.TF_UserId.attributedPlaceholder = NSAttributedString(string: "Enter your mobile number",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.BTN_Register.setAttributedTitle(self.callMethodForAttributedString(), for: UIControlState.normal)

    }
}
extension LoginScreenVC {
    
    // MARK:- Custom methods
    private func callMethodForAttributedString()->(NSMutableAttributedString) {
        
        let font1:UIFont = UIFont.init(name: "Ubuntu", size: 16.0)!
        let font2:UIFont = UIFont.init(name: "Ubuntu-Bold", size: 16.0)!
        let color1:UIColor = CommonHelper.hexStringToUIColor(hex: "C0CCD0")
        let color2:UIColor = UIColor.white
        
        let dict1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor : color1] as [NSAttributedStringKey : Any]
        let dict2 = [NSAttributedStringKey.font: font2, NSAttributedStringKey.foregroundColor : color2] as [NSAttributedStringKey : Any]

        let attString = NSMutableAttributedString()
        attString.append(NSAttributedString.init(string: "Don't have an account? ", attributes: dict1))
        attString.append(NSAttributedString.init(string: "REGISTER", attributes: dict2))
        
        return attString
    }
    private func callWebservice() {
        let parameters = ["entity" : ["MobileNumber" : self.TF_UserId.text!,"ComplexId" :  CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_AcccountLogin, objectType: UserData.self, isHud: true, controller: self, success: { [weak self] user in
            UserDefaults.standard.set(user.Data?.UserType!, forKey: "user")
            let appDelegate =  AppDelegate.appDelegateShared //UIApplication.shared.delegate as! AppDelegate
            
            guard let userData = user.Data else {
                self!.TF_UserId.text = ""
                CommonHelper.showAlert(vc: (self)!, title: "Alert", msg: (user.Message ?? "Unauthorized mobile number"))
                return
            }
            appDelegate.user = userData
            User.saveData(user: userData)
            
            if let val = (user.Data?.OTP) {
                self!.otpText = val
            }
            
            if (user.Status ?? false) {
                self?.sendToOTPScreen()
            }else {
                CommonHelper.showAlert(vc: (self)!, title: "Alert", msg: (user.Message)!)
            }
        })
    }
    private func validate() -> Bool {
        if !((self.TF_UserId.text?.count)!>0) {
            CommonHelper.showAlert(vc: self, title: "Alert", msg: "Please Enter your phone number.")
            return false
        }else if ((self.TF_UserId.text?.count)! != 10) {
            CommonHelper.showAlert(vc: self, title: "Alert", msg: "Phone number should be of 10 digits")
            return false
        }
        return true
    }
    func sendToOTPScreen() {
        if let _ = AppDelegate.appDelegateShared.user?.UserType {
            let targetVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPScreenVC") as! OTPScreenVC
            targetVC.otpText = self.otpText
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
        else {
            CommonHelper.showAlert(vc: self, title: "Mindspace", msg: "User not registered")
        }
    }
    // MARK:-  Selector methods
    @objc func didTapOnView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}
// MARK:-  IBActions
extension LoginScreenVC {
    
    @IBAction func loginAction(_ sender: Any) {

//        self.sendToOTPScreen()
//        return
        if validate() {
            self.callWebservice()
        }
    }
    @IBAction func registerAction(_ sender: Any) {
        self.navigationController?.pushViewController(Storyboard.registerViewController, animated: true)
    }
}
extension LoginScreenVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! + (string.count - range.length)) > 10 {
            return false
        }
        return true
    }
}
