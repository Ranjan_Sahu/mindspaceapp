//
//  RUEnterAndPlaygDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/15/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class RUEnterAndPlaygDetailsVC: UIViewController {
    
    
    // MARK:- Constants
    let cellIdentifier = "EntertainmentDetialsTableViewCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    
    @IBOutlet weak var banerImageView: RoundImageView!
    
    // MARK:- Objects
    var catrgoryArray = [String]()
    var eapDetails : EAPDetails? = nil
    private var imageList: [LightboxImage] = []
    
    var id : String?
    private var menuCardURLArray : [LightboxImage] = []
    var banerImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        self.view.layoutIfNeeded()
        // Banner Setup
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = " "
        self.getGymData()
        
        banerImageView.sd_setImage(with: URL.init(string: banerImage), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    
    @objc func makeCall(){
        if let url = URL(string: "tel://\(self.eapDetails?.ContactDetails ?? "")"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
}
extension RUEnterAndPlaygDetailsVC {
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let contactDetails = self.self.eapDetails?.ContactDetails
        let contactno1 = String((contactDetails?.suffix(10))!).trimmingCharacters(in: .whitespaces)
        if let url = URL(string: "tel://\(contactno1)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    // MARK:- Service Calls
    func getGymData() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "10"],
                         "entity" : ["EntertainmentPlaygroundId" : id ?? ""],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetEntertainmentPlaygroundDetails, objectType: EAPDetailsData.self, isHud: true, controller: self, success: { [weak self] eapDetailsdata in
            print(eapDetailsdata)
            guard eapDetailsdata.Data != nil else {
                CommonHelper.showAlert(vc: self!, title: "Error", msg: Messages.noData)
                return
            }
            self?.eapDetails = eapDetailsdata.Data
            self?.setData()
            self?.tblView.reloadData()
            self?.navigationItem.title = self!.eapDetails?.Title?.capitalized ?? " "
        })
    }
    
    private func setData(){
        self.imageList = self.eapDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0)!) }) ?? []
    }
}
// MARK: - TableView datsource and delegates
extension RUEnterAndPlaygDetailsVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.eapDetails != nil {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: EntertainmentDetialsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EntertainmentDetialsTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EntertainmentDetialsTableViewCell
            
        }
        cell.viewButton.addTarget(self, action:#selector(handlePresentAction), for: .touchUpInside)
        cell.selectionStyle = .none
        
        
        if let details = self.eapDetails {
            cell.LBL_gymTitle.text = details.Title?.capitalized
            cell.LBL_gymDesc.text = details.Description
            cell.LBL_contactPerson.text  = details.ContactDetails
            
            cell.LBL_contactPerson.isUserInteractionEnabled = true
            cell.LBL_contactPerson.text = details.ContactDetails
            let tapGestureForCall = UITapGestureRecognizer(target: self, action:#selector(self.handleTap))
            cell.LBL_contactPerson.addGestureRecognizer(tapGestureForCall)

            cell.LBL_peopleCount.text = String(describing: details.Capacity ?? "0")
            cell.LBL_gymTiming.text = details.Time
            cell.imageURL = details.ImageUrls
            cell.parentVC = self
            cell.imageList = self.imageList
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}



extension RUEnterAndPlaygDetailsVC : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    
}

extension RUEnterAndPlaygDetailsVC {
    @objc func handlePresentAction() {
        if let menuCardURL = eapDetails?.BookingProcessDocument {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Business Process Document"
        navigationController?.pushViewController(controller, animated: true)
    }
}

