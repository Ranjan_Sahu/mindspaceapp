//
//  EventPollTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 12/20/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class EventPollTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var forthSeasonView: UIView!
    @IBOutlet weak var fifthSeasonView: UIView!
    @IBOutlet weak var mindSpaceLabel: UILabel!
    @IBOutlet weak var favuriteSeasonLabel: UILabel!
    @IBOutlet weak var summerView: UIView!
    @IBOutlet weak var winterView: UIView!
    @IBOutlet weak var rainySeasonView: UIView!
    @IBOutlet weak var onclickSummerBtn: UIButton!
    @IBOutlet weak var onClickWinterBtn: UIButton!
    @IBOutlet weak var onClickRainyBtn: UIButton!
    @IBOutlet weak var onClickForthSeasonBtn: UIButton!
    @IBOutlet weak var onClickFifthSeasonBtn: UIButton!
    @IBOutlet weak var questionStack: UIStackView!
    @IBOutlet weak var summerLabel: UILabel!
    @IBOutlet weak var winterLabel: UILabel!
    @IBOutlet weak var rainyLabel: UILabel!
    @IBOutlet weak var season1Label: UILabel!
    @IBOutlet weak var season2Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCellUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setCellUI() {
        selectionStyle = .none
        bgView.layer.cornerRadius = 10
    }
}
