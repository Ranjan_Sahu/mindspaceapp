//
//  VisitorHomeVC.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitorHomeVC: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "HomeScreenCollectionViewCell1"
    
    // MARK:- Outlets
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var imageViewBaanner: RoundImageView!
    
    // MARK:- Objects
    var namesArray = [String]()
    var imgNameArray = [String]()
    var slides:[HomeScreenBannerView] = []
    var bannerList : [BannerList] = []
    var slide: CustomInfiniteScroller?
    var accountStatus : GetAccountStatus?
    
    // MARK:- Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAccountStatus()
        self.getBannerList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.reloadData()
        bannerView.layer.cornerRadius = 4.0
        imageViewBaanner.layer.cornerRadius = 4.0
        self.setupScreen()
        self.setupNavigation()
        if((slide) != nil){
            if(bannerList.count > 1){
                slide?.startTimer()
                self.getBannerList()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if((slide) != nil){
            slide?.stopTimer()
        }
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}
//MARK:- Class Methods
extension VisitorHomeVC {
    private func setupScreen(){
       
        namesArray = ["About Us", "Gallery", "Upload KYC", "Visit Details"]
        imgNameArray = ["icon_aboutus", "icon_gallery", "icon_uploadkyc", "icon_visitdetails"]
        
        if AppDelegate.appDelegateShared.window?.frame.height ?? 0 > CGFloat(800.0) {
            bannerHeight.constant = 240
        }else{
            if(Constants.Device.IS_IPHONE_5 ){
                bannerHeight.constant = 190
            }else{
                bannerHeight.constant = 220
            }
        }
        
        self.view.layoutIfNeeded()
        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.isScrollEnabled = false
        self.addLeftMenuButton()
    }
    private func setupNavigation() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Mindspace"
        _ = UIBarButtonItem(image: UIImage(named: "icon_bell"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(didSelectNotification(sender:)))
       // self.navigationItem.rightBarButtonItem = barButtonItem
    }
    @objc func didSelectNotification(sender: UIButton) {
        self.navigationController?.pushViewController(Storyboard.notificationsVC, animated: true)
    }
}
// MARK: - CollectionView Delegates and DataSource
extension VisitorHomeVC :  UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
        cell.LBL_title.text = namesArray[indexPath.row]
        cell.imgView.image = UIImage(named: imgNameArray[indexPath.row])
        
        cell.alpha = 0.2         //assigning the animation
        cell.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 1.0, animations: {
            cell.alpha = 1
            cell.transform = .identity
        })
        return cell
    }
    
}
extension VisitorHomeVC : UICollectionViewDelegate {
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            switch indexPath.row {
                case 0 : let targetVC = Storyboard.aboutUsVC
                        targetVC.visitor = true
    //                     targetVC.BTN_Done.isHidden = true
                         self.navigationController?.pushViewController(targetVC, animated: true)
                case 1 : self.navigationController?.pushViewController(Storyboard.ruGalleryVC, animated: true)
                case 2 : self.navigationController?.pushViewController(Storyboard.uploadKYCViewController, animated: true)
                case 3 : self.navigationController?.pushViewController(Storyboard.visitDetailsVC, animated: true)
                default : break
            }
        }
}
extension VisitorHomeVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
                    let w = (self.collectionView.bounds.size.width-4)/2
                    let size = CGSize.init(width: w - 20 , height: w - 20)
                    return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
}


extension VisitorHomeVC {
    
    // MARK:- Service Calls
    func getAccountStatus(){
        
        let parameters = [
            "entity" : ["UserId" : CommonHelper.getUserID()],
            "deviceInfo" : CommonHelper.getDeviceInfo()] as [String : Any]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetAccountStatus, objectType: GetAccountStatus.self, isHud: true, controller: self, success: { response in
            self.accountStatus = response
            if self.accountStatus?.Status == false{            //account is deactivated
                let alert = UIAlertController(title: self.accountStatus?.Message, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                    User.clearData()
                    let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "SelectPremiseVC") as! SelectPremiseVC
                    AppDelegate.appDelegateShared.user = nil
                    let navController = UINavigationController.init(rootViewController: targetVC)
                    //self.sideMenuViewController?.navigationController?.pushViewController(targetVC, animated: true)
                    //self.sideMenuViewController?.navigationController?.viewControllers = [targetVC]
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = navController
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        })
        
    }
    
    func getBannerList() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "10", "Month":"All"],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetVisitorBanner, objectType: BannerData.self, isHud: true, controller: self, success: { [weak self] bannerData in
            
            self?.bannerList = bannerData.DataList!
            
            self?.setUpBanner()
        })
    }
    
    // MARK:- Custom methods
    private func setUpBanner() {
        if bannerList.count > 0{
            slide = Bundle.main.loadNibNamed("CustomInfiniteScroller", owner: self, options: nil)?.first as? CustomInfiniteScroller
            slide!.frame = CGRect(x: 0, y: 0, width: bannerView.frame.size.width, height: bannerView.frame.size.height)
            bannerView.addSubview(slide!)
            slide!.setupBannerView(bannerArray: self.bannerList, delegate: self)
            slide?.startTimer()
        }
    }
    
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        
        if ((self.bannerList.count)>0) {
            for item in self.bannerList {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                let imgStr = item.BannerImage
                slide.bannerImgView.sd_setImage(with: URL(string: imgStr!), placeholderImage: UIImage(named: "bannerPlaceholder"))
                bannerVwArray.append(slide)
            }
        }else {
            let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
            slide.bannerImgView.image = UIImage(named: "bannerPlaceholder")
            bannerVwArray.append(slide)
        }
        return bannerVwArray
    }
 
}

extension VisitorHomeVC:CustomBannerDelegate{
    
    func didSelectBanner(bannerId:Int){
        
        if( bannerId > bannerList.count){
            return
        }
        print(bannerId)
        let banner = self.bannerList[bannerId-1]
        print(banner)
        let vc = Storyboard.galleryDetailsVC
        vc.albumID = banner.Id
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
