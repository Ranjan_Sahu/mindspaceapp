//
//  VisitorTabBarController.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitorTabBarController : UITabBarController {

    let TABBAR_HEIGHT = 49
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabBar()
        self.view.layoutIfNeeded()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}
//MARK:- Custom Methods
extension VisitorTabBarController {
    private func setupTabBar() {
        self.delegate = self
        self.selectedIndex = 0
        let tabBarItem1 = self.tabBar.items![0]
        let tabBarItem2 = self.tabBar.items![1]
     //   let tabBarItem3 = self.tabBar.items![2]
        let selectedFont:UIFont = UIFont.boldSystemFont(ofSize: 11)
        let unSelectedFont:UIFont = UIFont.systemFont(ofSize: 11)
        let selectedColor:UIColor = UIColor.white
        let unSelectedColor:UIColor = CommonHelper.hexStringToUIColor(hex: "B6D2DD")
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : unSelectedColor, NSAttributedStringKey.font:unSelectedFont], for: UIControlState.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : selectedColor, NSAttributedStringKey.font:selectedFont], for: UIControlState.selected)
        tabBarItem1.title = "Home"
        tabBarItem2.title = "Social"
 //       tabBarItem3.title = "Setting"
        tabBarItem1.image = UIImage(named: "tab_icon_home_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem1.selectedImage = UIImage(named: "tab_icon_home")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem2.image = UIImage(named: "tab_icon_social_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem2.selectedImage = UIImage(named: "tab_icon_social")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        tabBarItem3.image = UIImage(named: "tab_icon_setting_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        tabBarItem3.selectedImage = UIImage(named: "tab_icon_setting")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)   
    }
}
//MARK:- TabBar Delegate
extension VisitorTabBarController:  UITabBarControllerDelegate {
    // TabBar delegates
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        currentIndex = self.selectedIndex
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }

}
