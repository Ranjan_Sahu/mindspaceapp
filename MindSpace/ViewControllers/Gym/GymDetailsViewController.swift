//
//  GymDetailsViewController.swift
//  MindSpace
//
//  Created by webwerks on 12/3/18.
//  Copyright © 2018 Neo. All rights reserved.
//


import UIKit
import Lightbox

class GymDetailsViewController: UIViewController {
    
    // MARK:- Constants
    let cellIdentifier = "EntertainmentDetialsTableViewCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerImage: UIImageView!
    
    // MARK:- Objects
    var gymDetails : GymDetails?
    var dummyImgArray : [ImageList]?
    private var imageList: [LightboxImage] = []
    private var menuCardURLArray : [LightboxImage] = []
    var id : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        self.view.layoutIfNeeded()
        self.getGymData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    
    @objc func makeCall(){
        if let url = URL(string: "tel://\(self.gymDetails?.ContactDetails ?? "")"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension GymDetailsViewController{
    
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    // MARK:- Service Calls
    func getGymData() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "20"],
                         "entity" : ["GymId" : id ?? ""],
                         "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetGymDetails, objectType: GymData.self, isHud: true, controller: self, success: { [weak self] gymDetailsdata in
            print(gymDetailsdata)
            guard gymDetailsdata.Data != nil else {
                let alert = UIAlertController.init(title: "Error", message: Messages.noData, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
                    self?.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okAction)
                self?.present(alert, animated: true, completion: nil)
                self?.view.isHidden = true
                return
            }
            self?.gymDetails = gymDetailsdata.Data
            if (self!.gymDetails?.BannerImage) != nil {
                self?.bannerImage.sd_setImage(with: URL.init(string: self!.gymDetails?.BannerImage ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
            }
            self?.setData()
            self?.tblView.reloadData()
            
        })
    }
    
    private func setData(){
//        dummyImgArray = self.gymDetails?.ImageUrls
        
        
        self.imageList = self.gymDetails?.ImageUrls!.map({ LightboxImage(imageURL: URL.init(string: $0)!) }) ?? []
        self.navigationItem.title = gymDetails!.Title?.capitalized ?? " "
    }
}


extension GymDetailsViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.gymDetails != nil {
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: EntertainmentDetialsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EntertainmentDetialsTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EntertainmentDetialsTableViewCell
        }
        cell.selectionStyle = .none
        cell.viewButton.addTarget(self, action:#selector(showMenuCard), for: .touchUpInside)
        
        if let details = self.gymDetails {
            cell.LBL_gymTitle.text = details.Title?.capitalized
            cell.LBL_gymDesc.text = details.Description
            
            cell.LBL_contactPerson.isUserInteractionEnabled = true
            cell.LBL_contactPerson.text = self.gymDetails?.ContactDetails
            let tapGestureForCall = UITapGestureRecognizer(target: self, action:#selector(self.handleTap))
            cell.LBL_contactPerson.addGestureRecognizer(tapGestureForCall)
            
            cell.LBL_peopleCount.text = String(describing: details.Capacity ?? 0)
            cell.LBL_gymTiming.text = details.Time
            cell.imageURL = details.ImageUrls
            cell.imageList = self.imageList
            cell.parentVC = self
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        }
    }
}

extension GymDetailsViewController {
    @objc func showMenuCard() {
        if let menuCardURL = gymDetails?.BookingProcessDocument {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            } else if extensionType == "pdf" {
                showPDF(url : menuCardURL)
            }
        }
    }
    
    private func showPDF(url : String){
        SVProgressHUD.show()
        if let pdfDocumentURL = URL(string: url), let doc = PDFDocument(url: pdfDocumentURL) {
            showDocument(doc)
            SVProgressHUD.dismiss()
        } else {
            CommonHelper.showAlert(vc: self, title: Messages.title, msg: Messages.pdfNotFound)
            print("Document named \(url) not found")
            SVProgressHUD.dismiss()
        }
    }
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Business Process Document"
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let contactDetails = self.gymDetails?.ContactDetails
        let contactno1 = String((contactDetails?.suffix(10))!).trimmingCharacters(in: .whitespaces)
        if let url = URL(string: "tel://\(contactno1)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}


extension GymDetailsViewController : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    
}







