//
//  VisitorRequestCommonContentCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class VisitorRequestCommonContentCell1: UITableViewCell {

    @IBOutlet weak var departmentTxtField: DropDownForTable!
    @IBOutlet weak var tenantTxtField: DropDownForTable!
    @IBOutlet weak var meetingTxtField: DropDownForTable!
    @IBOutlet weak var timeTxtField: DropDownForTable!
    @IBOutlet weak var scheduledBtn: UIButton!
    @IBOutlet weak var adhocBtn: UIButton!
    @IBOutlet weak var departmentBtn: UIButton!
    @IBOutlet weak var tenantBtn: UIButton!
    @IBOutlet weak var meetingBtn: UIButton!
    @IBOutlet weak var timeBtn: UIButton!

    var scheduledBtnClicked: (() -> ())?
    var adhocBtnClicked: (() -> ())?
    var selectDepartment: (() -> ())?
    var selectTenant: (() -> ())?
    var selectMeeting: (() -> ())?
    var selectTime: (() -> ())?
    var controller : UIViewController?
    var item : Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
extension VisitorRequestCommonContentCell1{
    @IBAction func scheduledBtnTapped(_ sender: Any) {
        scheduledBtnClicked?()
    }
    @IBAction func adhocBtnTapped(_ sender: Any) {
        adhocBtnClicked?()
    }
    @IBAction func selectDepartmentBtnTapped(_ sender: UIButton) {
        selectDepartment?()
    }
    @IBAction func selectTenantTapped(_ sender: UIButton) {
        selectTenant?()
    }
    @IBAction func selectTimeBtnTapped(_ sender: UIButton) {
        selectTime?()
    }
    @IBAction func selectMeetingBtnTapped(_ sender: UIButton) {
        selectMeeting?()
    }
}
