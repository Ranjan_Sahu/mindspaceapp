//
//  EntertainmentAndPlaygroundVC.swift
//  MindSpace
//
//  Created by webwerks on 09/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class EntertainmentAndPlaygroundListVC: ListBaseVC {
    var totalRecords : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Entertainment & Playground"
        self.callWebService()
        // Do any additional setup after loading the view.
    }
    override func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)"],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetEntertainmentPlaygroundList, objectType: EntertainmentAndPlaygroundData.self, isHud: true, controller: self, success: {
            response in
            self.totalRecords = response.TotalRecords
            
            if self.totalRecords == 0{
                self.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            print("*************")
            print(response)
            guard let list = response.DataList else {return}
            if self.pageInfo.pageIndex == 1  {
                self.datasourceArray = list
            }else {
                self.datasourceArray.append(contentsOf: list)
            }
            self.tableView.reloadData()
        })
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "RUEnterAndPlaygDetailsVC") as! RUEnterAndPlaygDetailsVC
        if let data = self.datasourceArray[indexPath.row] as? EntertainmentAndPlaygroundList {
            targetVC.id = String(describing:  data.EntertainmentPlaygroundId!)
            targetVC.banerImage = data.BannerImage!
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
    }
    
}
