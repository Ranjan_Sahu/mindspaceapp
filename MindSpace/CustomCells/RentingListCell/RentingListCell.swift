//
//  RentingListCell.swift
//  MindSpace
//
//  Created by webwerks on 10/16/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RentingListCell: UITableViewCell {

    @IBOutlet weak var rentBannerImgView: UIImageView!
    @IBOutlet weak var LBL_title: UILabel!
    @IBOutlet weak var LBL_subText: UILabel!
    @IBOutlet weak var LBL_description: UILabel!
    @IBOutlet weak var LBL_peopleCount: UILabel!
    @IBOutlet weak var LBL_price: UILabel!
    
    @IBOutlet weak var peopleCountSW: UIStackView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
