//
//  RURentingVC.swift
//  MindSpace
//
//  Created by webwerks on 10/16/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RURentingVC: UIViewController {
    
    
    // MARK:- Constants
    let cellIdentifier = "RentingListCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    
    // MARK:- Objects
    var catrgoryArray = [String]()
    var slides:[HomeScreenBannerView] = [];
    var bannerTimer:Timer? = nil
    var gymDetails : [GymDetails]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        
        // Banner Setup
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
        
   //     self.getGymData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Rent"
        
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
}
extension RURentingVC {
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        var bannerImgArray = ["bImg1", "bImg2", "bImg3", "bImg4"]
        
        for imgStr in bannerImgArray {
            let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
            slide.bannerImgView.image = UIImage(named: imgStr)
            bannerVwArray.append(slide)
        }
        return bannerVwArray
    }
    
    func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
    
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
  /*  // MARK:- Service Calls
    func getGymData() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "10"],
                         "entity" : ["GymId" : "4"],
                         "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetGymDetails, objectType: GymData.self, isHud: true, controller: self, success: { [weak self] gymData in
            self?.gymDetails = gymData.Data
            print(self?.gymDetails!)
            self?.tblView.reloadData()
            
        })*/
//        SVProgressHUD.show()
//        let webService = Service()
//        webService.callPostServiceWithName(serviceName: Constants.ServiceConstants.Service_GetGymDetails, paramDict: param, showHud: false, hudView: self.view, successBlock: { (data, response ) in
//
//            SVProgressHUD.dismiss()
//            print(response)
//            do{
//                let gymData = try JSONDecoder().decode(GymData.self, from: response) as GymData
//                print(gymData)
//                self.gymDetails = gymData.Data
//                print(self.gymDetails!)
//                self.tblView.reloadData()
//            }catch let error {
//                print("Error: \(error)")
//            }
//        }) { (error) in
//            SVProgressHUD.dismiss()
//            print(error)
//        }
//
    }

    // MARK: - TableView datsource and delegates
extension RURentingVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if let gList = self.gymDetails {
         //   return (gList.count)
        //}
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: RentingListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RentingListCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RentingListCell
        }
        
        cell.LBL_subText.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
extension RURentingVC : UIScrollViewDelegate{
    
    
    // MARK:- ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }
    
}
