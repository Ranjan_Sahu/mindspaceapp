//
//  RUBookACabVC.swift
//  MindSpace
//
//  Created by webwerks on 10/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUBookACabVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    
    // MARK:- Constants
    let cellIdentifier = "BookACabCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
//        self.tblView.rowHeight = UITableViewAutomaticDimension
//        self.tblView.estimatedRowHeight = 40
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Cab Booking"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tblView.frame.size.height/2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: BookACabCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BookACabCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BookACabCell
        }
        
        if (indexPath.row == 0) {
            cell.logo.image = UIImage(named: "logo_ola")
            cell.LBL_title.text = "OLA"
        }else if (indexPath.row == 1) {
            cell.logo.image = UIImage(named: "logo_uber")
            cell.LBL_title.text = "UBER"
        }
        
        self.addShadowInView(vw: cell.shadowView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var urlStr = ""
        
        if (indexPath.row == 0) {
            urlStr = UserDefaults.standard.string(forKey: "OlaIOSAppLink")!
        }else if (indexPath.row == 1) {
            urlStr = UserDefaults.standard.string(forKey: "UberIOSAppLink")!
        }
        
        var appUrl = URL(string: urlStr)
        let isAppInstalled = UIApplication.shared.canOpenURL(appUrl!)
        if isAppInstalled {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appUrl!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(appUrl!)
            }
        }else {
            if (indexPath.row == 0) {
                urlStr = UserDefaults.standard.string(forKey: "OlaIOSStoreLink")!
            }else if (indexPath.row == 1) {
                urlStr = UserDefaults.standard.string(forKey: "UberIOSStoreLink")!
            }
            appUrl = URL(string: urlStr)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appUrl!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(appUrl!)
            }
        }
        
    }
    
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    func addShadowInView(vw:UIView) {
        vw.layer.masksToBounds = false
        let sColor = Constants.AppColor.COLOR_Shadow
        vw.layer.shadowColor = sColor.cgColor
        vw.layer.shadowOpacity = 0.2
        vw.layer.shadowOffset = CGSize.zero
        vw.layer.shadowRadius = 8
    }
    
    
}
