//
//  RUEmergencyContactsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUEmergencyContactsVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    // MARK:- Constants
    let cellIdentifier = "ContactsListCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    var visitorReqList : [VisitorRequestList]? = nil
    var dummyImgArray : [String]? = nil
    var ambulanceNoStr = ""
    var hospitalNoStr = ""
    var fireBrigadeNoStr = ""
    var policeNoStr = ""
    var contactsArray : [Dictionary<String,Any>]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var hospitalNos = [String]()
        if hospitalNoStr != ""{
            hospitalNos = hospitalNoStr.components(separatedBy: ";")
        }
        var ambulanceNos = [String]()
        if ambulanceNoStr != ""{
            ambulanceNos = ambulanceNoStr.components(separatedBy: ";")
        }
        var fireBrigadeNos = [String]()
        if fireBrigadeNoStr != ""{
            fireBrigadeNos = fireBrigadeNoStr.components(separatedBy: ";")
        }
        var policeNos = [String]()
        if policeNoStr != ""{
            policeNos = policeNoStr.components(separatedBy: ";")
        }

        contactsArray = [
            [
                "title":"Ambulance :",
                "numberList": ambulanceNos
            ],
            [
                "title":"Hospital :",
                "numberList": hospitalNos
            ],
            [
                "title":"Fire Brigade :",
                "numberList": fireBrigadeNos
            ],[
                "title":"Police :",
                "numberList":policeNos
            ]
        ]
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Help Desk"
        
        self.tblView.reloadData()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
}
extension RUEmergencyContactsVC {
    // MARK: - TableView datsource and delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return (contactsArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arr = contactsArray![section]["numberList"] as! [String]
        return arr.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: 40))
        vw.backgroundColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 15, y: 15, width: self.tblView.frame.size.width-15, height: 25))
        label.backgroundColor = UIColor.clear
        label.text = contactsArray![section]["title"] as? String
        label.textColor = CommonHelper.hexStringToUIColor(hex: "323333")
        label.font  = UIFont(name: "Ubuntu", size: 15)
        
        vw.addSubview(label)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ContactsListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ContactsListCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ContactsListCell
        }
        
        let list = contactsArray![indexPath.section]["numberList"] as! [String]
        cell.LBL_ContactNo.text = list[indexPath.row] as String
        
        
        
        cell.seperatorLbl.isHidden = true
        if (indexPath.row == list.count-1) {
            cell.seperatorLbl.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let list = contactsArray![indexPath.section]["numberList"] as! [String]
        let phoneNo = list[indexPath.row] as String
    
        let numberToBeCalledArray =  phoneNo.components(separatedBy: "-")   //seaperated string withy - into array
        let numberToBeCalled = (numberToBeCalledArray[numberToBeCalledArray.count - 1]).trimmingCharacters(in: .whitespaces) // accessing array elemena and trimming spaces
        if let url = URL(string: "tel://\(numberToBeCalled	)"),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
   
    
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    func addShadowInView(vw:UIView) {
        vw.layer.masksToBounds = false
        let sColor = Constants.AppColor.COLOR_Shadow
        vw.layer.shadowColor = sColor.cgColor
        vw.layer.shadowOpacity = 0.2
        vw.layer.shadowOffset = CGSize.zero
        vw.layer.shadowRadius = 8
    }
    
}

