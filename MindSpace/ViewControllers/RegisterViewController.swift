//
//  RegisterViewController.swift
//  MindSpace
//
//  Created by Zeeshan  on 21/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    
    
    @IBOutlet weak var visitorViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var visitorView: ShadowView!
    @IBOutlet weak var employeeView: ShadowView!
    @IBOutlet weak var userTypeTxtField: DropDown!
    @IBOutlet weak var txtFirstName: RoundTextField!
    @IBOutlet weak var txtLastName: RoundTextField!
    @IBOutlet weak var txtPhoneNumber: RoundTextField!
    @IBOutlet weak var txtDocumentNumber: RoundTextField!
    @IBOutlet weak var txtPassword: RoundTextField!
    @IBOutlet weak var txtEmail: RoundTextField!
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var txtProfile: RoundTextField!
    @IBOutlet weak var uploadDocImageView: UIImageView!
    @IBOutlet weak var txtuploadDoc: RoundTextField!

    @IBOutlet weak var btnAadhar: UIButton!
    @IBOutlet weak var btnPanCard: UIButton!
    @IBOutlet weak var btnPassport: UIButton!
    
    
    @IBOutlet weak var BTN_Visitor: UIButton!
    @IBOutlet weak var BTN_Employee: UIButton!

    
    
    private var imagePicker = UIImagePickerController()
    private var imageType : Int?
    private var docType : Int?
    private var visitorRegistrationVc: VisitorRegistrationVC!
    
    private lazy var document : UIImage = {
        let document = UIImage()
        return UIImage()
    }()
    private lazy var profilePicture : UIImage = {
        let profilePicture = UIImage()
        return profilePicture
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.BTN_Visitor.isSelected = true
        self.BTN_Employee.isSelected = false
        self.setupController()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = "Registration"
        self.navigationController?.navigationBar.isHidden = false
    UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
    UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .highlighted)
    }    
}

extension RegisterViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visitorRegistrationSegue" {
            let RVC = segue.destination as? VisitorRegistrationVC
            visitorRegistrationVc = RVC
        }
    }
}

extension RegisterViewController {
    @IBAction func registerClicked(_ sender: UIButton) {
        
        if BTN_Visitor.isSelected {
            if self.visitorRegistrationVc.validate() {
               self.visitorRegistrationVc.submitRequest()
            }
        }
        else if BTN_Employee.isSelected {
            if(!CommonHelper.validateField(type: FieldTypes.Normal, val: txtFirstName.text!, controller: self,fieldName :FieldNames.FirstName.rawValue)){
                return;
            }
            if(!CommonHelper.validateField(type: FieldTypes.Normal, val: txtLastName.text!, controller: self,fieldName :FieldNames.LastName.rawValue)){
                return;
            }
            if(!CommonHelper.validateField(type: FieldTypes.Email, val: txtEmail.text!, controller: self)){
                return;
            }
            if(self.txtPhoneNumber.text?.count != 10){
                CommonHelper.showAlert(vc: self, title:"WARNING", msg: Warnings.Invalid_Phone)
                return;
            }
            self.callRegisterUser()
        }
    }
    @IBAction func uploadDocument(_ sender: UIButton) {
        self.imageType = 0
        present(imagePicker, animated: true, completion: nil)
  //      imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
        //var picker = UIImagePickerController()
    }
    @IBAction func uploadProfilePicture(_ sender: UIButton) {
        self.imageType = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func btnAadharAction(_ sender: UIButton) {
        self.btnAadhar.isSelected = true
        self.btnPanCard.isSelected = false
        self.btnPassport.isSelected = false
        self.docType = 1
    }
    
    @IBAction func btnPanCardAction(_ sender: UIButton) {
        self.btnAadhar.isSelected = false
        self.btnPanCard.isSelected = true
        self.btnPassport.isSelected = false
        self.docType = 2
    }
    
    @IBAction func btnPassportAction(_ sender: UIButton) {
        self.btnAadhar.isSelected = false
        self.btnPanCard.isSelected = false
        self.btnPassport.isSelected = true
        self.docType = 3
    }
    
    @IBAction func BTN_VisitorAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.BTN_Employee.isSelected = false
        self.visitorView.isHidden = false
        self.employeeView.isHidden = true
    }
    
    @IBAction func BTN_EmployeeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.BTN_Visitor.isSelected = false
        self.visitorView.isHidden = true
        self.employeeView.isHidden = false
    }
    
}

extension RegisterViewController {
    private func setupController(){
        self.setupPicker()
    }
    private func setupPicker(){
        self.imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType    = .photoLibrary
    }
    private func callRegisterUser(){

        let locationId = "\(UserDefaults.standard.value(forKey: "Selected_Location_ID") ?? 1)"
        let complexId = "\(UserDefaults.standard.value(forKey: "Selected_Complex_ID") ?? 1)"
        var parameters = ["FirstName":txtFirstName.text!,
                          "LastName":txtLastName.text!,
                          "EmailAddress" : txtEmail.text!,
                          "MobileNumber" : txtPhoneNumber.text!,
                          "LocationId" : locationId,
                          "ComplexId" : complexId,
        ]
        
        if let val = self.txtDocumentNumber.text {
            parameters["DocumentNumber"] = val
        }
        if let val = self.docType {
            parameters["DocumentType"] = "\(val)"
        }

        let images = ["Document" : self.document,
                      "ProfilePhoto" : self.profilePicture]
        Webservice.uploadFiles(imageArray: images, parameters: parameters, url: WebserviceURLs.register, objectType: UploadReqData.self, controller: self, success: { response in
            Constants.showAlertWithMessage(message: response.Message ?? "", presentingVC: self, okBlock: { _ in
                if response.Status == true{
                    self.navigationController?.popViewController(animated: true)
                }
            })
        })
    }
}

extension RegisterViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            switch self.imageType {
            case 0 : do {
                self.document = pickedImage
                self.uploadDocImageView.isHidden = false
                self.uploadDocImageView.image = pickedImage
                self.txtuploadDoc.isHidden = true
                }
            case 1 : do {
                self.profilePicture = pickedImage
                self.profilePicImageView.isHidden = false
                self.profilePicImageView.image = pickedImage
                self.txtProfile.isHidden = true
                }
            default :  break
            }
//            self.selectedKYCImage = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
