//
//  ListBaseVC.swift
//  MindSpace
//
//  Created by webwerks on 09/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class ListBaseVC: UIViewController {
    let refreshControl = UIRefreshControl()
    let cellIdentifier = "CrecheCell"
    var pageInfo = PageInfo()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    var datasourceArray : [ServicesListings] = [] {
        didSet {
            tableView.reloadData()
            if datasourceArray.count == 0{
                //tableView.tableFooterView = noDataView
                self.view.makeToast("No more data available", duration: 1.0, position: .center)

            }else {
                tableView.tableFooterView = nil
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil) , forCellReuseIdentifier: cellIdentifier)
        addRefreshControlToTableView()
        callWebService()
    }
    func callWebService()  {
        
    }
}

extension ListBaseVC {
    func addRefreshControlToTableView() {
        self.tableView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        callWebService()
        refreshControl.endRefreshing()
    }
}

extension ListBaseVC : UITableViewDelegate,UITableViewDataSource {
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: CrecheCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CrecheCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CrecheCell
        }
        let info = datasourceArray[indexPath.row]
        if (info.BannerImage) != nil {
            cell.eventBannerImgView.sd_setImage(with: URL(string: info.BannerImage ?? ""), placeholderImage: UIImage(named: "bannerPlaceholder"))
        }
        cell.LBL_eventTitle.text = info.Title ?? ""
        cell.LBL_eventLocation.text = info.BuildingName ?? ""
        cell.LBL_eventTime.text = info.Time ?? ""
        cell.LBL_subText?.text = "\(info.Capacity ?? "")"
        //        cell.eventBannerImgView.sd_setImage(with: URL(, placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .SDWebImageProgressiveDownload)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
}

extension ListBaseVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
        if (endScrolling >= Int(scrollView.contentSize.height)){
            pageInfo.pageIndex += 1
            callWebService()
        }
    }
}
