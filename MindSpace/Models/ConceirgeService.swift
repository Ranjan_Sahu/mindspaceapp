//
//  ConceirgeServiceModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  ConceirgeServiceListData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [ConceirgeServiceList]?
    let TotalRecords : Int?
}

struct  ConceirgeServiceList : Codable {
    let ConceirgeServiceId : Int?
    let Title : String?
    let ContactDetails : String?
    let BuildingName : String?
    let ComplexName : String?
    let ComplexId : Int?
    let IsActive : Bool?
    let CreatedOn : String?
    let StartTime : String?
    let EndTime : String?
    let Time : String?
    let ConceirgeServiceTypeTitle : String?
    let ImageURLs : [String]?
}

struct ConceirgeServiceDetailsData : Codable{
    let Data : ConceirgeServiceDetails?
    let Message : String?
    let Status : Bool?
}
struct ConceirgeServiceDetails : Codable{
    let Title : String?
    let Description : String?
    let BookingProcessDocument : String?
    let ContactDetails : String?
    let BannerImage : String?
    let BuildingId : Int?
    let BuildingName : String?
    let ComplexId : Int?
    let ComplexName : String?
    let ConceirgeServiceTypeId : Int?
    let ConceirgeServiceTypeTitle : String?
    let CategoryId : Int?
    let StartTime : String?
    let EndTime : String?
    let Time : String?
    let ImageUrls : [String]?
}
