//
//  Creche.swift
//  MindSpace
//
//  Created by webwerks on 29/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class  CrecheListData : Codable {
    var Status : Bool?
    var Message : String?
    var DataList : [CrecheList]?
    var TotalRecords : Int?
    var Data : CrecheDetails?
}

class CrecheList : Codable {
    var CrecheId : Int?
    var Title : String?
    var BuildingName : String?
    var BannerImage : String?
    var Time : String?
    var Capacity : String?
    var ComplexId : Int?
}

class CrecheDetails : Codable {
    var CrecheId : Int?
    var Title : String?
    var Description : String?
    var BookingProcessDocument : String?
    var ContactDetails : String?
    var BuildingId : Int?
    var ComplexId : Int?
    var CategoryId : Int?
    var Time : String?
    var Capacity : String?
    var BannerImage : String?
    var ImageUrls : [ImageList]?
}
//class  CrecheList : ServicesListings {
//    var CrecheId : Int? = 0
//
//    private enum CodingKeys : String, CodingKey {
//        case CrecheId
//    }
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.CrecheId = try container.decode(Int.self, forKey: .CrecheId)
//        try super.init(from: decoder)
//    }
//
//    override func encode(to encoder: Encoder) throws {
//        try super.encode(to: encoder)
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(CrecheId, forKey: .CrecheId)
//
//    }
//}
