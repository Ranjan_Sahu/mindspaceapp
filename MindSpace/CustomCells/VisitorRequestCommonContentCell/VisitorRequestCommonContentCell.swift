//
//  VisitorRequestCommonContentCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
protocol VisitorRequestCommonCellDelegate {
    func visitorDataChanged(common:VisitorRequestCommon)
    func departmentChanged(selectedDepartment : DepartmentList)
    func complexChanged(complex: ComplexList)
}
class VisitorRequestCommonContentCell: UITableViewCell {

    @IBOutlet weak var departmentTxtField: UITextField!
    @IBOutlet weak var complexTxtField: UITextField!
    @IBOutlet weak var buildingTextfield: RoundTextField!
    @IBOutlet weak var meetingTxtField: UITextField!
    @IBOutlet weak var startTimeTxtField: UITextField!
    @IBOutlet weak var endTimeTxtField: UITextField!
    @IBOutlet weak var dateTxtField: UITextField!

    @IBOutlet weak var scheduledBtn: UIButton!
    @IBOutlet weak var adhocBtn: UIButton!
    let dateFormatter = DateFormatter()
    var pickerView: UIPickerView? {
        didSet{
            pickerView?.delegate = self
            pickerView?.dataSource = self
        }
    }
    var datePickerView: UIDatePicker?
    var toolBar: UIToolbar?
    
    var pickerContent : [String] = ["Mindspace","Mindspace","Mindspace","Mindspace","Mindspace"]
    var commonFields = VisitorRequestCommon()
    var activeTextField :UITextField?
    var complexNDepartmentList : ComplexAndDepartmentDetails?
    var buildingList: [BuildingList]?
    var meetingPersonList : [MeetingPersonList] = []
    var delegate : VisitorRequestCommonCellDelegate?
    var scheduledBtnClicked: (() -> ())?
    var adhocBtnClicked: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
   
    @IBAction func optionBtnTapped(_ sender: UIButton) {
        if sender == adhocBtn {
            adhocBtn.isSelected = true
            scheduledBtn.isSelected = false
        }else {
            adhocBtn.isSelected = false
            scheduledBtn.isSelected = true
        }
        commonFields.isMeetingAdhoc =  adhocBtn.isSelected
        delegate?.visitorDataChanged(common: commonFields)
    }
}

extension VisitorRequestCommonContentCell : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        datePickerView?.autoresizingMask = .flexibleHeight
        pickerView?.autoresizingMask = .flexibleHeight
        activeTextField = textField
        if textField == startTimeTxtField || textField == endTimeTxtField{
            datePickerView?.datePickerMode = .time
            datePickerView?.minuteInterval = 30
            textField.inputView = datePickerView
        }else if textField == dateTxtField {
            datePickerView?.datePickerMode = .date
            datePickerView?.minimumDate = Date()
            textField.inputView = datePickerView
        }else {
            textField.inputView = pickerView
            pickerView?.reloadAllComponents()
        }
        
        textField.inputAccessoryView = toolBar
        //        textField.mo
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case startTimeTxtField,endTimeTxtField:
            if let datetime = datePickerView?.date {
                dateFormatter.dateFormat = "hh:mm a" //"dd-MM-yyyy HH:mm:ss"
                let time = dateFormatter.string(from: datetime)
                if textField == startTimeTxtField {
                    commonFields.meetingStartTime = time
                    startTimeTxtField.text = time
                }else {
                    commonFields.meetingEndTime = time
                    endTimeTxtField.text = time
                }
            }
        case dateTxtField:
            if let datetime = datePickerView?.date {
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let time = dateFormatter.string(from: datetime)
                commonFields.meetingDate = dateFormatter.string(from: (datePickerView?.date)!)
                dateTxtField.text = time
            }
        default:
            if let pickView = pickerView {
                selectRow(row: pickView.selectedRow(inComponent: 0))
//                pickerView(pickView, didSelectRow: pickView.selectedRow(inComponent: 0), inComponent: 0)
            }
            break
        }
        delegate?.visitorDataChanged(common: commonFields)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeTextField = nil
        return true
    }
}
extension VisitorRequestCommonContentCell : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if activeTextField == departmentTxtField {
            return complexNDepartmentList?.DepartmentList?.count ?? 0
        }else if activeTextField == complexTxtField {
            return complexNDepartmentList?.ComplexList?.count ?? 0
        } else if activeTextField == buildingTextfield {
            return buildingList?.count ?? 0
        } else {
            return meetingPersonList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if activeTextField == departmentTxtField {
            return complexNDepartmentList?.DepartmentList?[row].DepartmentName ?? ""
        }else if activeTextField == complexTxtField{
            return complexNDepartmentList?.ComplexList?[row].ComplexName ?? ""
        } else if activeTextField == buildingTextfield {
            return buildingList?[row].BuildingName ?? ""
        } else {
            return meetingPersonList[row].Name ?? ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
    }
//    {
//        if activeTextField == departmentTxtField {
//            if row < complexNDepartmentList?.DepartmentList?.count ?? 0,let info = complexNDepartmentList?.DepartmentList?[row] {
//                activeTextField?.text = info.DepartmentName ?? ""
//                commonFields.departmentId =  "\(complexNDepartmentList?.DepartmentList?[row].DepartmentId ?? 0)"
//                meetingTxtField.text = ""
//                delegate?.departmentChanged(selectedDepartment: info)
//            }else {
//                return
//            }
//
//        }else if activeTextField == complexTxtField {
//            if row < complexNDepartmentList?.ComplexList?.count ?? 0,  let info = complexNDepartmentList?.ComplexList?[row] {
//                activeTextField?.text = info.ComplexName ?? ""
//                commonFields.complexId = "\(info.ComplexId ?? 0)"
//            }else {
//                return
//            }
//
//        }else  if activeTextField == meetingTxtField{
//            if row < meetingPersonList.count{
//                let info = meetingPersonList[row]
//                activeTextField?.text = info.Name ?? ""
//                commonFields.meetingPersonId = "\(info.Id ?? 0)"
//            }else{
//                return
//            }
//
//        }
//        updateCommonDetails()
//    }
    func selectRow(row : Int) {
        if activeTextField == departmentTxtField {
            if row < complexNDepartmentList?.DepartmentList?.count ?? 0,let info = complexNDepartmentList?.DepartmentList?[row] {
                activeTextField?.text = info.DepartmentName ?? ""
                commonFields.departmentId =  "\(complexNDepartmentList?.DepartmentList?[row].DepartmentId ?? 0)"
                meetingTxtField.text = ""
                commonFields.meetingPersonId = ""
                commonFields.meetingPerson = ""
                delegate?.departmentChanged(selectedDepartment: info)
            }else {
                return
            }
            
        } else if activeTextField == complexTxtField {
            if row < complexNDepartmentList?.ComplexList?.count ?? 0,  let info = complexNDepartmentList?.ComplexList?[row] {
                activeTextField?.text = info.ComplexName ?? ""
                commonFields.complexId = "\(info.ComplexId ?? 0)"
                commonFields.buildingId = ""
                commonFields.building = ""
                commonFields.departmentId = ""
                commonFields.department = ""
                commonFields.meetingPersonId = ""
                commonFields.meetingPerson = ""
                delegate?.complexChanged(complex: info)
            } else {
                return
            }
            
        } else if activeTextField == buildingTextfield {
            if row < buildingList?.count ?? 0,  let info = buildingList?[row] {
                activeTextField?.text = info.BuildingName ?? ""
                commonFields.buildingId = "\(info.BuildingId ?? 0)"
                commonFields.departmentId = ""
                commonFields.department = ""
                commonFields.meetingPersonId = ""
                commonFields.meetingPerson = ""
            }else {
                return
            }
            
        } else  if activeTextField == meetingTxtField{
            if row < meetingPersonList.count{
                let info = meetingPersonList[row]
                activeTextField?.text = info.Name ?? ""
                commonFields.meetingPersonId = "\(info.Id ?? 0)"
            }else{
                return
            }
            
        }
        updateCommonDetails()
    }
}
//MARK: - Helper
extension VisitorRequestCommonContentCell {
    func updateCommonDetails() {
        if let txtField = activeTextField {
            switch txtField {
            case departmentTxtField:
                commonFields.department = txtField.text ?? ""
            case complexTxtField:
                commonFields.complex = txtField.text ?? ""
            case buildingTextfield:
                commonFields.building = txtField.text ?? ""
            case meetingTxtField:
                commonFields.meetingPerson = txtField.text ?? ""
            case startTimeTxtField:
                commonFields.meetingStartTime = txtField.text ?? ""
            case endTimeTxtField:
                commonFields.meetingEndTime = txtField.text ?? ""
            case dateTxtField:
                commonFields.meetingDate = txtField.text ?? ""
            default:
                break
            }
            delegate?.visitorDataChanged(common: commonFields)
        }
    }
}
