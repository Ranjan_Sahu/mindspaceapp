//
//  Extensions.swift
//  MindSpace
//
//  Created by Webwerks on 10/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

extension UIStoryboard {
    
    private enum Storyboard : String {
        case main = "Main"
        case advance = "Advance"
    }
    
    static var main: UIStoryboard { return UIStoryboard(.main) }
    static var advance: UIStoryboard { return UIStoryboard(.advance) }
    
    
    private convenience init(_ storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    private class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    
    func instantiate<T>(_ controller: T.Type) -> T where T : UIViewController {
        return viewController(with: String(describing: controller)) as! T
    }
    
    func viewController(with id: String) -> UIViewController {
        return instantiateViewController(withIdentifier: id) as UIViewController
    }
}
