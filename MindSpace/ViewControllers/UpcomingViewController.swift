//
//  UpcomingViewController.swift
//  MindSpace
//
//  Created by webwerks on 1/24/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit

class UpcomingViewController: UIViewController {
    let cellIdentifier = "HomeScreenCollectionViewCell1"
    var namesArray = [String]()
    var imgNameArray = [String]()
    let marketObject = RUMarketPlaceVC()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        namesArray = ["Visitor Request", "Visit Details", "Car Parking", "About Common Spaces","Creche","Book Cab","Entertainment & Playground","Gym"]
        //        imgNameArray = ["icon_visitor_request", "icon_visit_details", "icon_car_parking","icon_commonspaces","icon_creche","icon_cab","icon_playground","icon_gym"]
        namesArray = ["Book a Cab", "Gym","Concierge Services"]
        imgNameArray = ["icon_cab", "icon_gym","icon_concierge_service"]
        self.collectionView!.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Upcoming"
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
}

extension UpcomingViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return namesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeScreenCollectionViewCell
        cell.LBL_title?.text = namesArray[indexPath.item]
        cell.imgView?.image = UIImage(named: imgNameArray[indexPath.item])
        cell.alpha = 0.2         //assigning the animation
        cell.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 1.0, animations: {
            cell.alpha = 1
            cell.transform = .identity
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row == 0) {
            let targetVC = Storyboard.ruBookACabVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 1) {
            let targetVC = Storyboard.ruGymVC
            self.navigationController?.pushViewController(targetVC, animated: true)
        }else if (indexPath.row == 2) {
            self.navigationController?.pushViewController(Storyboard.ruConciergeServicesVC, animated: true)
        }
//        else if (indexPath.row == 3) {
//            let targetVC = Storyboard.entertainmentAndPlaygroundListVC
//            self.navigationController?.pushViewController(targetVC, animated: true)
//        }else if (indexPath.row == 4) {
//            let targetVC = Storyboard.upcomingViewController
//            self.navigationController?.pushViewController(targetVC, animated: true)
//        }
        //        else if (indexPath.row == 5) {
        //            let targetVC = Storyboard.ruBookACabVC
        //            self.navigationController?.pushViewController(targetVC, animated: true)
        //        }else if (indexPath.row == 6) {
        //            let targetVC = Storyboard.entertainmentAndPlaygroundListVC
        //            self.navigationController?.pushViewController(targetVC, animated: true)
        //        }else if (indexPath.row == 7) {
        //            let targetVC = Storyboard.ruGymVC
        //            self.navigationController?.pushViewController(targetVC, animated: true)
        //        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let w = (self.collectionView.bounds.size.width-6)/3
        //let h = (self.collectionView.bounds.size.height-6)/3
        let size = CGSize.init(width: w, height: w + 20)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(2, 2, 2, 2)
    }
    

   

}
