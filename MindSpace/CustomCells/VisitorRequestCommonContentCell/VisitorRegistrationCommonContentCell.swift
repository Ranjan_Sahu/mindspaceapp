//
//  VisitorRegistrationCommonContentCell.swift
//  MindSpace
//
//  Created by webwerks on 11/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
protocol VisitorRegistrationCommonCellDelegate {
    func visitorDataChanged(common:VisitorRegistrationCommon)
    func tenantChanged(selectedTenant : Tenant)
    func departmentChanged(selectedDepartment : DepartmentList)
}
class VisitorRegistrationCommonContentCell: UITableViewCell {

    @IBOutlet weak var departmentTxtField: UITextField!
    @IBOutlet weak var tenantTxtField: UITextField!
    @IBOutlet weak var buildingTextField: RoundTextField!
    @IBOutlet weak var meetingTxtField: UITextField!
    @IBOutlet weak var startTimeTxtField: UITextField!
    @IBOutlet weak var endTimeTxtField: UITextField!
    @IBOutlet weak var dateTxtField: UITextField!

    @IBOutlet weak var scheduledBtn: UIButton!
    @IBOutlet weak var adhocBtn: UIButton!
    let dateFormatter = DateFormatter()
    var pickerView: UIPickerView? {
        didSet{
            pickerView?.delegate = self
            pickerView?.dataSource = self
        }
    }
    var datePickerView: UIDatePicker?
    var toolBar: UIToolbar?
    
    var pickerContent : [String] = ["Mindspace","Mindspace","Mindspace","Mindspace","Mindspace"]
    var commonFields = VisitorRegistrationCommon()
    var activeTextField :UITextField?
    var tenantDepartmentList : [DepartmentList]?
    var meetingPersonList : [MeetingPersonList] = []
    var delegate : VisitorRegistrationCommonCellDelegate?
    var tenantList : [Tenant] = []
    var buildingList: [BuildingList]?
    var scheduledBtnClicked: (() -> ())?
    var adhocBtnClicked: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
   
    @IBAction func optionBtnTapped(_ sender: UIButton) {
        if sender == adhocBtn {
            adhocBtn.isSelected = true
            scheduledBtn.isSelected = false
        }else {
            adhocBtn.isSelected = false
            scheduledBtn.isSelected = true
        }
        commonFields.isMeetingAdhoc =  adhocBtn.isSelected
        delegate?.visitorDataChanged(common: commonFields)
    }
}

extension VisitorRegistrationCommonContentCell : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        datePickerView?.autoresizingMask = .flexibleHeight
        pickerView?.autoresizingMask = .flexibleHeight
        activeTextField = textField
        if textField == startTimeTxtField || textField == endTimeTxtField{
            datePickerView?.datePickerMode = .time
            datePickerView?.minuteInterval = 30
            textField.inputView = datePickerView
        }else if textField == dateTxtField {
            datePickerView?.datePickerMode = .date
            datePickerView?.minimumDate = Date()
            textField.inputView = datePickerView
        }else {
            textField.inputView = pickerView
            pickerView?.reloadAllComponents()
        }
        
        textField.inputAccessoryView = toolBar
        //        textField.mo
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case startTimeTxtField,endTimeTxtField:
            if let datetime = datePickerView?.date {
                dateFormatter.dateFormat = "hh:mm a" //"dd-MM-yyyy HH:mm:ss"
                let time = dateFormatter.string(from: datetime)
                if textField == startTimeTxtField {
                    commonFields.meetingStartTime = time
                    startTimeTxtField.text = time
                }else {
                    commonFields.meetingEndTime = time
                    endTimeTxtField.text = time
                }
            }
        case dateTxtField:
            if let datetime = datePickerView?.date {
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let time = dateFormatter.string(from: datetime)
                commonFields.meetingDate = dateFormatter.string(from: (datePickerView?.date)!)
                dateTxtField.text = time
            }
        default:
            if let pickView = pickerView {
                selectRow(row: pickView.selectedRow(inComponent: 0))
//                pickerView(pickView, didSelectRow: pickView.selectedRow(inComponent: 0), inComponent: 0)
            }
            break
        }
        delegate?.visitorDataChanged(common: commonFields)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeTextField = nil
        return true
    }
}
extension VisitorRegistrationCommonContentCell : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if activeTextField == departmentTxtField {
            return tenantDepartmentList?.count ?? 0
        }else if activeTextField == tenantTxtField {
            return tenantList.count
        } else if activeTextField == buildingTextField {
            return buildingList?.count ?? 0
        } else {
            return meetingPersonList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if activeTextField == departmentTxtField {
            return tenantDepartmentList?[row].DepartmentName ?? ""
        }else if activeTextField == tenantTxtField{
            return tenantList[row].TenantName
        } else if activeTextField == buildingTextField {
            return buildingList?[row].BuildingName ?? ""
        } else {
            return meetingPersonList[row].Name ?? ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
    }
//    {
//        if activeTextField == departmentTxtField {
//            if row < complexNDepartmentList?.DepartmentList?.count ?? 0,let info = complexNDepartmentList?.DepartmentList?[row] {
//                activeTextField?.text = info.DepartmentName ?? ""
//                commonFields.departmentId =  "\(complexNDepartmentList?.DepartmentList?[row].DepartmentId ?? 0)"
//                meetingTxtField.text = ""
//                delegate?.departmentChanged(selectedDepartment: info)
//            }else {
//                return
//            }
//
//        }else if activeTextField == complexTxtField {
//            if row < complexNDepartmentList?.ComplexList?.count ?? 0,  let info = complexNDepartmentList?.ComplexList?[row] {
//                activeTextField?.text = info.ComplexName ?? ""
//                commonFields.complexId = "\(info.ComplexId ?? 0)"
//            }else {
//                return
//            }
//
//        }else  if activeTextField == meetingTxtField{
//            if row < meetingPersonList.count{
//                let info = meetingPersonList[row]
//                activeTextField?.text = info.Name ?? ""
//                commonFields.meetingPersonId = "\(info.Id ?? 0)"
//            }else{
//                return
//            }
//
//        }
//        updateCommonDetails()
//    }
    func selectRow(row : Int) {
        if activeTextField == departmentTxtField {
            if row < tenantDepartmentList?.count ?? 0,let info = tenantDepartmentList?[row] {
                activeTextField?.text = info.DepartmentName ?? ""
                commonFields.departmentId =  "\(tenantDepartmentList?[row].DepartmentId ?? 0)"
                meetingTxtField.text = ""
                commonFields.meetingPersonId = ""
                commonFields.meetingPerson = ""
                delegate?.departmentChanged(selectedDepartment: info)
            }else {
                return
            }
            
        } else if activeTextField == tenantTxtField {
            if row < tenantList.count {
                let info = tenantList[row]
                activeTextField?.text = info.TenantName
                commonFields.tenantId = "\(info.TenantId ?? 0)"
                delegate?.tenantChanged(selectedTenant: info)
            }else {
                return
            }
            
        } else if activeTextField == buildingTextField {
            if row < buildingList?.count ?? 0, let info = buildingList?[row] {
                buildingTextField?.text = info.BuildingName
                commonFields.buildingId = "\(info.BuildingId ?? 0)"
            } else {
                return
            }
        } else if activeTextField == meetingTxtField{
            if row < meetingPersonList.count{
                let info = meetingPersonList[row]
                activeTextField?.text = info.Name ?? ""
                commonFields.meetingPersonId = "\(info.Id ?? 0)"
            }else{
                return
            }
            
        }
        updateCommonDetails()
    }
}
//MARK: - Helper
extension VisitorRegistrationCommonContentCell {
    func updateCommonDetails() {
        if let txtField = activeTextField {
            switch txtField {
            case departmentTxtField:
                commonFields.department = txtField.text ?? ""
            case tenantTxtField:
                commonFields.tenant = txtField.text ?? ""
            case buildingTextField:
                commonFields.building = txtField.text ?? ""
            case meetingTxtField:
                commonFields.meetingPerson = txtField.text ?? ""
            case startTimeTxtField:
                commonFields.meetingStartTime = txtField.text ?? ""
            case endTimeTxtField:
                commonFields.meetingEndTime = txtField.text ?? ""
            case dateTxtField:
                commonFields.meetingDate = txtField.text ?? ""
            default:
                break
            }
            delegate?.visitorDataChanged(common: commonFields)
        }
    }
}
