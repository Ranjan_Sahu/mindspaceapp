//
//  EventDetailsVC.swift
//  MindSpace
//
//  Created by webwerks on 08/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Lightbox

class EventDetailsVC: UIViewController {
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblEventName1: UILabel!
    @IBOutlet weak var lblEventName2: UILabel!
    @IBOutlet weak var lblEventDescription: UILabel!
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var lblEventLocation: UILabel!
    @IBOutlet weak var lblEventMonth: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var attendingBtn: UIButton!
    @IBOutlet weak var notAttendingBtn: UIButton!
    @IBOutlet weak var maybeBtn: UIButton!
    @IBOutlet weak var rsvpHeight: NSLayoutConstraint!
    @IBOutlet weak var rsvpStack: UIStackView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var clockIcon: UIImageView!
    @IBOutlet weak var attendingCountLabel: UILabel!
    @IBOutlet weak var lblMaybeAttending: UILabel!
    @IBOutlet weak var lblNotAttending: UILabel!
    
    public var id : Int?
    private var eventDetailsObj:EventDetails?
    private var eventImages : [ImageURLS]?
    private var imageList : [LightboxImage] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //  navigationItem.title = "Event Details"
        self.setupScreen()
    }
}
extension EventDetailsVC {
    //MARK:- Class Methods
    private func setupScreen() {
        self.getEventDetails()
    }
    
    private func setupData(evenDetails : EventDetails) {
        if (evenDetails.BannerImage) != nil {
            self.imgBanner.sd_setImage(with: URL.init(string: evenDetails.BannerImage ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        //self.lblEventName1.text = evenDetails.EventNewsTitle
        self.navigationItem.title = evenDetails.EventNewsTitle
        self.lblEventName2.text = evenDetails.EventNewsTitle
        self.lblEventDescription.text = evenDetails.EventNewsDesc?.html2String
        self.lblEventLocation.text = evenDetails.EventLocation
        //self.lblEventTime.text = evenDetails.EventTime
        if evenDetails.EventNewsType != 2 {
            self.lblEventTime.text = evenDetails.EventTime
        } else {
            self.lblEventTime.isHidden = true
            self.clockIcon.isHidden = true
        }
        let date = evenDetails.FromDate!.getDateArray()
        self.lblEventMonth.text = date[1]
        self.lblEventDate.text = date[0]
        self.eventImages = evenDetails.ImageURLs
        
        self.imageList = self.eventImages?.map({ LightboxImage(imageURL: URL.init(string: $0.ImageName ?? "")!) }) ?? []
        
        if evenDetails.RSVP!{
            
            let attendingCount:Int = evenDetails.RSVPData?.AttendingCount ?? 0
            let strAttendingCount = String(attendingCount)
            let notAttendingCount:Int = evenDetails.RSVPData?.NotAttendingCount ?? 0
            let strNotAttendingCount = String(notAttendingCount)
            let maybeAttendingCount:Int = evenDetails.RSVPData?.MayBeCount ?? 0
            let strMaybeAttendingCount = String(maybeAttendingCount)
            
            self.attendingCountLabel.text = "(\(strAttendingCount) people attending)"
            self.lblNotAttending.text = "(\(strNotAttendingCount) people not attending)"
            self.lblMaybeAttending.text = "(\(strMaybeAttendingCount) people maybe attending)"
            
            let rvspVal:Int = evenDetails.RSVPData?.RSVP ?? 0
            
            switch rvspVal {
            case 1:
                self.buttonSelections(attendingBtn: true, notAttendingBtn: false, maybeBtn: false)
                self.disableButtonInteraction()
                break
            case 2:
                self.buttonSelections(attendingBtn: false, notAttendingBtn: true, maybeBtn: false)
                 self.disableButtonInteraction()
                break
            case 3:
                self.buttonSelections(attendingBtn: false, notAttendingBtn: false, maybeBtn: true)
                 self.disableButtonInteraction()
                break
            default:
                self.buttonSelections(attendingBtn: false, notAttendingBtn: false, maybeBtn: false)
                 self.enableButtonInteraction()
                break
            }
        }
        self.collectionView.reloadData()
    }
    
    //MARK:- Webservice call
    private func getEventDetails() {
        let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["EventNewsId" : id ?? 0],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getEventDetails, objectType: EventData.self, isHud: true, controller: self, success: { [weak self] eventData in
            
            guard let data = eventData.Data else {
                let alert = UIAlertController.init(title: "Error", message: Messages.noData, preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
                    self?.navigationController?.popViewController(animated: true)
                }
                alert.addAction(okAction)
                self?.present(alert, animated: true, completion: nil)
                self?.view.isHidden = true
                return
            }
            
            
        //    guard let data =  eventData.Data else {return}
            if data.RSVP! {
            }
            else {
                self?.rsvpHeight.constant = 0
                self?.rsvpStack.isHidden = true
            }
            self!.eventDetailsObj = data
            self?.setupData(evenDetails: self!.eventDetailsObj!)
            
        })
        
        
    }

    private func setRSVP(rsvp : String?){
        
        let parameters = [ "entity" : ["UserId" : CommonHelper.getUserID(),
                                       "EventNewsId" : id ?? "",
                                       "RSVP":rsvp ?? ""],
                           "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.setRSVP, objectType: RSVPData.self, isHud: true, controller: self, success: { rsvpData in
            
            guard rsvpData.Data != nil else {return}
            
            let rsvpDataObj:RSVP = rsvpData.Data!
            self.eventDetailsObj!.RSVPData = rsvpDataObj
            print(rsvpData)
            self.setupData(evenDetails: self.eventDetailsObj!)
            //self.getEventDetails()
        })
    }
    private func setRSVPData(rsvp: RSVP?){
        switch rsvp?.RSVP {
        case 0 : self.buttonSelections(attendingBtn: false, notAttendingBtn: false, maybeBtn: false)
        case 1 : self.buttonSelections(attendingBtn: true, notAttendingBtn: false, maybeBtn: false)
        case 2 : self.buttonSelections(attendingBtn: false, notAttendingBtn: true, maybeBtn: false)
        case 3 : self.buttonSelections(attendingBtn: false, notAttendingBtn: false, maybeBtn: true)
        default: break
        }
    }
}

//MARK:- Actions
extension EventDetailsVC {
    @IBAction func selectOptionClicked(_ sender: UIButton) {
        if sender == attendingBtn {
            self.buttonSelections(attendingBtn: true, notAttendingBtn: false, maybeBtn: false)
            self.setRSVP(rsvp: "1")

        }else if sender == notAttendingBtn{
            self.buttonSelections(attendingBtn: false, notAttendingBtn: true, maybeBtn: false)
            self.setRSVP(rsvp: "2")

        }else {
            self.setRSVP(rsvp: "3")
            self.buttonSelections(attendingBtn: false, notAttendingBtn: false, maybeBtn: true)
            self.setRSVP(rsvp: "2")

        }
    }
    
    private func enableButtonInteraction(){
        self.attendingBtn.isUserInteractionEnabled = true
        self.notAttendingBtn.isUserInteractionEnabled = true
        self.maybeBtn.isUserInteractionEnabled = true
    }
    
    private func disableButtonInteraction(){
        self.attendingBtn.isUserInteractionEnabled = false
        self.notAttendingBtn.isUserInteractionEnabled = false
        self.maybeBtn.isUserInteractionEnabled = false
    }
    
    private func buttonSelections(attendingBtn : Bool, notAttendingBtn : Bool, maybeBtn : Bool){
        DispatchQueue.main.async {
            self.attendingBtn.isSelected = attendingBtn
            self.notAttendingBtn.isSelected = notAttendingBtn
            self.maybeBtn.isSelected = maybeBtn
        }
    }
}


extension EventDetailsVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.eventImages?.count else { return 0 }
        return count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        if let image = self.eventImages?[indexPath.row].thumbnail {
            cell.imgView.sd_setImage(with: URL.init(string: image), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        return cell
    }
}
extension EventDetailsVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard imageList.count != 0 else {return}
        let controller = LightboxController(images: imageList, startIndex: indexPath.row)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
    
}
extension EventDetailsVC : LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}
extension EventDetailsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
}
