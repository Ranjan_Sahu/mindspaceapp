//
//  Webservices.swift
//  MindSpace
//
//  Created by Webwerks on 10/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation
import Alamofire

struct WebserviceURLs {
    //ServiceName Constants
    //static let baseUrl = "http://10.0.101.59/api/"
//   static let baseUrl = "http://mindspace.aspnetdevelopment.in/api/"
    static let baseUrl = "http://portaldev.mindspaceindia.co.in/api/" // Current

//    http://portaldev.mindspaceindia.co.in/
    static let Service_AcccountLogin = "Account/Login"
    static let Service_GetGalleryList = "Album/GetGalleryList"
    static let Service_GetGalleryDetails = "Album/GetGalleryDetails"
    static let Service_GetEntertainmentPlaygroundList = "EntertainmentPlayground/GetEntertainmentPlayGroundList"
    static let Service_GetEntertainmentPlaygroundDetails = "EntertainmentPlayground/GetEntertainmentPlaygroundDetails"
    static let Service_GetFNBOutletList = "FNBOutlet/GetFNBOutletList"
    static let Service_GetFNBDetails = "FNBOutlet/GetFNBDetails"
    static let Service_GetBookingSpaceList = "BookingSpace/GetBookingSpaceList"
    static let Service_GetBookingSpaceDetails = "BookingSpace/GetBookingSpaceDetails"
    static let getGymList = "Gym/GetGymList"
    static let Service_GetGymDetails = "Gym/GetGymDetails"
    static let Service_GetConceirgeServiceList = "ConceirgeService/GetConceirgeServiceList"
    static let getConceirgeServiceDetails = "ConceirgeService/GetConceirgeServiceDetails"
    static let Service_GetAnnouncementList = "Announcement/GetAnnouncementList"
    static let getAnouncementDetails = "Announcement/GetAnnouncementDetails"
    static let getOfferList = "Offer/GetOfferList"
    static let getOfferDetails = "Offer/GetOfferDetails"
    static let Service_GetEventList = "Event/GetEventList"
    static let getEventDetails = "Event/GetEventDetails"
    static let Service_CrecheList = "Creche/GetCrecheList"
    static let getCrecheDetails = "Creche/GetCrecheDetails"
    static let Service_GetLocationList = "Location/GetLocationList"
    static let getParkingDetails = "Complex/GetCarParkingDetails"
    static let Service_GetComplexList = "Complex/GetComplexList"
    static let Service_GetFAQList = "FAQ/GetFAQList"
    static let getPollByComplex = "Poll/GetPollByComplex"
    static let getPollByBuilding = "Poll/GetPollByBuilding"
    static let getHelpDeskList = "HelpDesk/GetHelpDeskList"
    static let Service_GetSystemConfiguration = "SystemConfiguration/GetSystemConfiguration"
    static let getTenantList = "Tenant/GetTenantList"
    static let addVisitorRequest = "VisitorRequest/AddVisitorRequest"
    static let Service_GetVisitorRequestHistory = "VisitorRequest/GetVisitorRequestHistory"
    static let Service_GetBannerList = "Banner/GetBannerList"
    static let setRSVP = "Event/SetRSVP"
    static let getRSVP = "Event/GetRSVP"
    static let getVisitorInfo = "Visitor/GetVisitorInfo"
    static let uploadKYC = "Visitor/UploadKYCDocuments"
    static let suppotList = "ComplaintTicket/GetComplaintTicketList"
    static let Service_GetComplexNDepartmentList = "VisitorRequest/GetComplexNDepartmentList"
    static let Service_GetMeetingPersonList = "VisitorRequest/GetMeetingPersonList"
    static let Service_AddVisitorRequest = "VisitorRequest/AddVisitorRequest"
    static let register = "Account/RegisterUser"
    static let register_visitor = "Account/RegisterVisitor"
    static let Service_GetProductList = "MarketPlace/GetProductList"
    static let Service_GetProductDetails = "MarketPlace/GetProductDetails"
    static let Service_GetMarketPlaceBannerList = "MarketPlace/GetMarketPlaceBanner"
    static let Service_GetProductCategoryList = "MarketPlace/GetProductCategoryList"
    static let Service_AddProduct = "MarketPlace/AddProduct"
    static let Service_SetInterested = "MarketPlace/SetInterested"
    static let ticketTypeListData = "ComplaintTicket/GetComplaintTypeList"
    static let raiseTicket = "ComplaintTicket/RaiseTicket"
    static let Service_GetSetPollList = "Poll/SetPoll"
    static let Service_GetPollList = "Poll/GetPollByComplex"
    static let Service_GetVisitorHistory = "Visitor/GetVisitorHistory"
    static let Service_GetTenantList = "Tenant/GetTenantListByComplexId"
    static let Service_GetTenantDepartmentList = "Department/GetDepartmentByTenantId"
    static let Service_GetVisitorMeetingPersonList = "Visitor/GetMeetingPersonList"
    static let Service_GetVisitorBanner = "Visitor/GetVisitorBanner"
    static let Service_GetComplaintTicket = "ComplaintTicket/GetComplaintTicket"
    static let Service_GetAccountStatus = "Account/CheckUserStatus"
    static let getATMList = "Atm/GetAtmList"
    static let getAmbulanceList = "Ambulance/GetAmbulanceList"
    static let getBuildingList = "/Building/GetBuildingList"
    static let getBuildingListForTenant = "Building/GetBuildingListForTenant"
    

}

class Webservice {
    
    static let header = ["content-type": "application/json",
                         "AuthorizeToken" : authValue]
    
    static func callPostWebservice<T:Codable>(parameters : [String:Any]? = nil, url : String, objectType : T.Type, isHud : Bool,controller : UIViewController, success : @escaping (_ responseData : T) -> Void){
        if CommonHelper.checkReachability() == false {
            CommonHelper.showAlert(vc: controller, title: "Network Issue", msg: "Please check your internet connection !")
            return
        }
        if isHud {
            UIApplication.shared.beginIgnoringInteractionEvents()
            SVProgressHUD.show()
        }
        Alamofire.request(WebserviceURLs.baseUrl + url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers : Webservice.header).responseData { response in
            SVProgressHUD.dismiss()
            UIApplication.shared.endIgnoringInteractionEvents()
            switch response.result {
            case .success:
                do {
                    //let backToString = String(data: response.data!, encoding: String.Encoding.utf8) as String!
                   // print(backToString)
                    let sysData = try JSONDecoder().decode(T.self, from: response.data!) as T
//                    print(sysData)
                    DispatchQueue.main.async {
                        success(sysData)
                    }
                }
                catch {
                    print(error.localizedDescription)
                    CommonHelper.showAlert(vc: controller, title: "Mindspace", msg: Warnings.errorMessage)
                }
                print("Validation Successful")
            case .failure(let error):
                print(error)
                CommonHelper.showAlert(vc: controller, title: "Mindspace", msg: error.localizedDescription)
            }
        }
    }
    
    static func callGetWebservice<T : Codable>(parameters : [String:Any], url : String, objectType : T.Type, success : @escaping (_ responseData : T) -> Void){
        Alamofire.request(WebserviceURLs.baseUrl + url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers : Webservice.header).responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                do {
                    _ = try JSONDecoder().decode(T.self, from: response.data!) as T
                    //success(sysData)
                }
                catch {
                    print(error.localizedDescription)
                }
                print("Validation Successful")
            case .failure(let error):
                print(error)
            }
        }
    }
    static func uploadFiles<T : Codable>(imageArray : [String:UIImage], parameters : [String:String], url : String, objectType : T.Type, controller : UIViewController, success : @escaping (_ responseData : T) -> Void){
        
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            SVProgressHUD.show()
            
        }
        let serviceURL = WebserviceURLs.baseUrl + url
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key,value) in imageArray {
                let image:UIImage?
                image = value
                if (image != nil) {
                    let jpegImage = UIImageJPEGRepresentation(image!, 1)
                    if(jpegImage != nil){
                        multipartFormData.append(UIImageJPEGRepresentation(image!, 1)!, withName: key, fileName: "\(key).jpeg", mimeType: "image/jpeg")
                    }else{
                        print("empty image")
                    }
                }else{
                    print("empty image")
                }
            }
            
            for (key,value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: serviceURL , headers: Webservice.header )
        { (result) in
           
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                
                upload.responseJSON(completionHandler: { (response) in
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        SVProgressHUD.dismiss()
                    }
                    
                    switch response.result {
                    case .success:
                        do {
                            _ = String(data: response.data!, encoding: String.Encoding.utf8) as String!
//                            print(backToString)
                            let sysData = try JSONDecoder().decode(T.self, from: response.data!) as T
                            //print(sysData)
                            DispatchQueue.main.async {
                                success(sysData)
                            }
                        }
                        catch {
                            print(error.localizedDescription)
                        }
                        print("Validation Successful")
                    case .failure(let error):
                        print(error)
                        CommonHelper.showAlert(vc: controller, title: "Mindspace", msg: error.localizedDescription)
                    }                })
//                upload.responseJSON { response in
//                    print(response.response)
////                    print(response.result)
//                    success()
//                }
                
            case .failure(let encodingError): print(encodingError.localizedDescription)
                
            }
        }
    }
    static func showAlertWithMessage(message:String, presentingVC:UIViewController){
        let alertVC = UIAlertController(title: "Mind Space", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
        }
        alertVC.addAction(ok)
        presentingVC.present(alertVC, animated: true, completion: nil)
    }
}
