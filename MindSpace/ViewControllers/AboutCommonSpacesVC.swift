//
//  AboutCommonSpacesVC.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class AboutCommonSpacesVC: ListBaseVC {

    var totalRecords : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "About Common Spaces"
    }
    override func callWebService() {
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)"],
                          "entity" : ["ComplexId" : "1" ],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetBookingSpaceList, objectType: CommonSpacesListData.self, isHud: true, controller: self, success: {
            response in
            self.totalRecords = response.TotalRecords
            
            if self.totalRecords == 0{
                self.view.makeToast("No more data available", duration: 1.0, position: .center)
                return;
            }
            guard let list = response.DataList else {return}
            if self.pageInfo.pageIndex == 1  {
                self.datasourceArray = list
            }else {
                self.datasourceArray.append(contentsOf: list)
            }
            self.tableView.reloadData()
        })
    }
}
extension AboutCommonSpacesVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasourceArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! CrecheCell
        if let info = datasourceArray[indexPath.row] as? CommonSpacesList {
            cell.LBL_eventTitle.text        = info.BookingSpaceName ?? ""
            cell.LBL_subText.text           = info.BookingSpaceCapacity ?? ""
            cell.LBL_eventTime.text         = info.Time ?? ""
            cell.LBL_eventLocation.text     = info.BuildingName ?? ""
          //  cell.eventBannerImgView.sd_setImage(with: URL.init(string: info.ban ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
            cell.eventBannerImgView.sd_setImage(with: URL(string: info.ImageUrls?.first ?? ""), placeholderImage: UIImage(named: "bannerPlaceholder"))

        }
        return cell
    }
}
extension AboutCommonSpacesVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.ruAboutCommanSpaceDetailsVC
        if let info = datasourceArray[indexPath.row] as? CommonSpacesList {
            targetVC.bookingSpaceId = String(describing: info.BookingSpaceId ?? 0)
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
    }
}

