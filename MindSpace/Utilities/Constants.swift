//
//  Constants.swift
//  MindSpace
//
//  Created by webwerks on 9/24/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let SOS_NUMBER: String = "9879879870"
    
    // App delegate Constant
    struct AppDelegate {
        static let APP_DELEGATE = UIApplication.shared.delegate
    }
    
    struct StoryBoard {
        static let MAIN: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        static let ADVANCE: UIStoryboard = UIStoryboard(name: "Advance", bundle: nil)
    }
    
    // iPhone Device Constants
    struct Device {
        static let IS_IPHONE_4 : Bool =  (UIScreen.main.bounds.size.height == 480.0)
        static let IS_IPHONE_5 : Bool =  (UIScreen.main.bounds.size.height == 568.0)
        static let IS_IPHONE_6 : Bool =  (UIScreen.main.bounds.size.height == 667.0)
        static let IS_IPHONE_6P : Bool =  (UIScreen.main.bounds.size.height == 736.0)
        static let IS_IPHONE_7 : Bool =  (UIScreen.main.bounds.size.height == 667.0)
        static let IS_IPHONE_7P : Bool =  (UIScreen.main.bounds.size.height == 736.0)
    }
    
    // Device Type Constants
    struct DeviceType {
        static let IS_IPAD: Bool =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        static let IS_IPHONE: Bool =  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
        static let IS_RETINA: Bool =  (UIScreen.main.scale >= 2.0)
    }
    
    // Screen Size Constants
    struct ScreenSize {
        static let SCREEN_WIDTH: CGFloat  = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT: CGFloat  = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGHT: CGFloat = max(SCREEN_WIDTH, SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGHT: CGFloat = min(SCREEN_WIDTH, SCREEN_HEIGHT)
    }
    
    // Color constants
    struct AppColor {
        static let COLOR_Theme: UIColor = UIColor(red: 53.0/255.0, green: 126.0/255.0, blue: 167.0/255.0, alpha: 1.0)
        static let COLOR_Black: UIColor = UIColor.black
        static let COLOR_White: UIColor = UIColor.white
        static let COLOR_Blue: UIColor = UIColor.blue
        static let COLOR_Gray: UIColor = UIColor.gray
        static let COLOR_Shadow: UIColor = CommonHelper.hexStringToUIColor(hex: "267aa0")
    }
    
    // Font constants
    struct AppFont {
        static func APP_FONT(size: CGFloat) -> UIFont{
            return UIFont(name: "SourceSansPro-Regular", size: size)!
        }
        static func APP_FONT_LIGHT(size: CGFloat) -> UIFont{
            return UIFont(name: "SourceSansPro-Light", size: size)!
        }
        static func APP_FONT_SEMIBOLD(size: CGFloat) -> UIFont{
            return UIFont(name: "SourceSansPro-Semibold", size: size)!
        }
        static func APP_FONT_BOLD(size: CGFloat) -> UIFont{
            return UIFont(name: "SourceSansPro-bold", size: size)!
        }
    }
    
    //Font Size constants
    struct FontSizes {
        static let SIZE_TITLE:CGFloat = 14.0
        static let SIZE_SUB_TITLE:CGFloat = 12.0
        static let SIZE_DATE:CGFloat = 10.0
        static let SIZE_HEADER:CGFloat = 18.0
        static let SIZE_SUB_HEADER:CGFloat = 16.0
    }
    
    // OS Version constant
    struct DeviceInfo {
        static let IOS_VERSION: String  = UIDevice.current.systemVersion
        static let SYSTEM_NAME: String  = UIDevice.current.systemName
    }
    
    //WebService Constants
//    struct ServiceConstants {
//        //ServiceName Constants
//        static let Service_GetComplexList = "Complex/GetComplexList"
//        static let Service_AcccountLogin = "Account/Login"
//        static let Service_GetLocationList = "Location/GetLocationList"
//        static let Service_GetEventList = "Event/GetEventList"
//        static let Service_GetFNBOutletList = "FNBOutlet/GetFNBOutletList"
//        static let Service_GetGalleryList = "Album/GetGalleryList"
//        static let Service_GetVisitorRequestHistory = "VisitorRequest/GetVisitorRequestHistory"
//        static let Service_GetFAQList = "FAQ/GetFAQList"
//        static let Service_GetGymDetails = "Gym/GetGymDetails"
//        static let Service_GetEntertainmentPlaygroundDetails = "EntertainmentPlayground/GetEntertainmentPlaygroundDetails"
//        static let Service_GetBookingSpaceDetails = "BookingSpace/GetBookingSpaceDetails"
//        static let Service_GetAnnouncementList = "Announcement/GetAnnouncementList"
//        static let Service_GetSystemConfiguration = "SystemConfiguration/GetSystemConfiguration"
//        static let Service_GetConceirgeServiceList = "ConceirgeService/GetConceirgeServiceList"
//        static let Service_GetBannerList = "Banner/GetBannerList"
//    }
    
    // Identifiers constants
    struct IdentifierKeys {
    }
    
    // NotificationKeys constants
    struct NotificationKeys {
    }
    static func showAlertWithMessage(message:String, presentingVC:UIViewController,okBlock:@escaping (_ msg:String)->Void){
        let alertVC = UIAlertController(title: "Mindspace", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            okBlock("")
        }
        alertVC.addAction(ok)
        presentingVC.present(alertVC, animated: true, completion: nil)
    }
}
