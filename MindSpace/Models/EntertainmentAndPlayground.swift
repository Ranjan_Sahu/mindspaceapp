//
//  EntertainmentAndPlayground.swift
//  MindSpace
//
//  Created by webwerks on 29/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

class  ServicesListings:Codable  {
    var Title : String?
    var ContactDetails : String?
    var BuildingName : String?
    var ComplexId : Int?
    var Capacity : String?
    var ComplexName : String?
    var Time : String?
    var ImageUrls : [String]?
    var BannerImage : String?
}

class  EntertainmentAndPlaygroundData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [EntertainmentAndPlaygroundList]?
    let TotalRecords : Int?
}

class  EntertainmentAndPlaygroundList : ServicesListings {
    
    var EntertainmentPlaygroundId : Int? = 0
    var Description : String? = ""
    var BookingProcessDocument : String? = ""
    
    private enum CodingKeys : String, CodingKey {
        case EntertainmentPlaygroundId
        case Description
        case BookingProcessDocument
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.EntertainmentPlaygroundId = try container.decode(Int.self, forKey: .EntertainmentPlaygroundId)
        self.Description = try container.decode(String.self, forKey: .Description)
        self.BookingProcessDocument = try container.decode(String.self, forKey: .BookingProcessDocument)
        
        try super.init(from: decoder)
    }
    
    override func encode(to encoder: Encoder) throws {
        try super.encode(to: encoder)
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(EntertainmentPlaygroundId, forKey: .EntertainmentPlaygroundId)
        try container.encode(Description, forKey: .Description)
        try container.encode(BookingProcessDocument, forKey: .BookingProcessDocument)
        
    }
}

// MARK :-  Details
class  EntertainmentAndPlaygroundDetailsData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : [EntertainmentAndPlaygroundList]?
}
