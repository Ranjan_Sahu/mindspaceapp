//
//  GalleryCollectionViewCell.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: ScalingCarouselCell {
    @IBOutlet weak var imgView: UIImageView!

}
