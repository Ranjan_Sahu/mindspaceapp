//
//  NotificationsVC.swift
//  MindSpace
//
//  Created by webwerks on 10/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
    
class NotificationsVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    // MARK:- Constants
    let cellIdentifier = "NotificationListCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        
        self.getNotificationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Notifications"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    // MARK:- Service Calls
    func getNotificationList() {
        
    }
    
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: NotificationListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? NotificationListCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? NotificationListCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
}
