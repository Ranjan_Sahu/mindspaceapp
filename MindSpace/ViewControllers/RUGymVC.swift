//
//  RUGymVC.swift
//  MindSpace
//
//  Created by webwerks on 10/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUGymVC: UIViewController {
    
    
    // MARK:- Constants
    let cellIdentifier = "CrecheCell"
    var totalRecords : Int?

    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var noDataView: UIView!

    // MARK:- Objects
    var catrgoryArray = [String]()
    public var imageURL : [String]?
    let refreshControl = UIRefreshControl()
    private var pageInfo = PageInfo()
    private var gymData : GymList?

    var gymDetails : [GymListing] = []{
        didSet {
            tblView.reloadData()
            if gymDetails.count == 0{
                self.view.makeToast("No more data available", duration: 1.0, position: .center)
            }else {
                tblView.tableFooterView = nil
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //tblview setup
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.addRefreshControlToTableView()
        self.view.layoutIfNeeded()
        // Banner Setup
        self.getGymData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Gyms"

//        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
//        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //  self.bannerTimer?.invalidate()
    }
}
extension RUGymVC {
    // MARK:- Service Calls
    private func getGymData() {
        let parameters = ["pageInfo" : ["PageIndex" : 1,
                                        "PageSize" : 20],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getGymList, objectType: GymList.self, isHud: true, controller: self, success: { [weak self] response in
            self?.totalRecords = response.TotalRecords
            
            if self?.totalRecords == 0{
                self?.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            
            self?.gymData = response
            if self?.pageInfo.pageIndex == 1{
                self?.gymDetails = response.DataList ?? []
            } else {
                if let gymDetails = response.DataList{
                    self?.gymDetails.append(contentsOf: gymDetails)
                }
            }
            self?.tblView.reloadData()
            
        })
    }
    
}
extension RUGymVC : UITableViewDataSource, UITableViewDelegate {
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.gymDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CrecheCell", for: indexPath) as! CrecheCell
        let gymData = self.gymDetails[indexPath.row]
        cell.LBL_eventTitle?.text       = gymData.Title
        cell.LBL_eventLocation?.text    = gymData.BuildingName
        cell.LBL_eventTime?.text        = gymData.Time
        cell.LBL_subText?.text          = "\(gymData.Capacity ?? 0)"
         if (gymData.BannerImage) != nil {
           cell.eventBannerImgView.sd_setImage(with: URL.init(string: gymData.BannerImage ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Constants.StoryBoard.MAIN.instantiateViewController(withIdentifier: "GymDetailsViewController") as! GymDetailsViewController
        if let data = self.gymDetails[indexPath.row] as? GymListing {
            targetVC.id = String(describing:  data.GymId!)
            self.navigationController?.pushViewController(targetVC, animated: true)
            
        }
    }
}

extension RUGymVC {
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
}

extension RUGymVC {
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func handleRefresh()  {
        pageInfo.pageIndex = 1
        getGymData()
        refreshControl.endRefreshing()
    }
}


