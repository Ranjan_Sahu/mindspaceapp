//
//  RUSettingVC.swift
//  MindSpace
//
//  Created by webwerks on 9/26/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUSettingVC: UIViewController {
    
    // Constants
    
    // Outlets
    @IBOutlet weak var BTN_aboutUs: UIButton!
    @IBOutlet weak var BTN_PrivacyPolicy: UIButton!
    @IBOutlet weak var view_PrivacyPolicy: UIView!
    @IBOutlet weak var view_AboutUs: UIView!
    @IBOutlet weak var view_logout: UIView!
    
    // Objects
    
    // Controllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftMenuButton()
        
        self.view_AboutUs.isHidden = false
        self.view_PrivacyPolicy.isHidden = true
        
        self.addShadowInView(vw: self.view_AboutUs)
        self.addShadowInView(vw: self.view_PrivacyPolicy)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Settings"
    }
    
    // MARK:- IBActions
    @IBAction func bellAction(_ sender: UIButton) {
    }
    
    @IBAction func aboutUsAction(_ sender: UIButton) {
        self.BTN_PrivacyPolicy.isSelected = false
        self.BTN_aboutUs.isSelected = true

        self.view_AboutUs.isHidden = false
        self.view_PrivacyPolicy.isHidden = true
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        self.BTN_aboutUs.isSelected = false
        self.BTN_PrivacyPolicy.isSelected = true

        self.view_AboutUs.isHidden = true
        self.view_PrivacyPolicy.isHidden = false
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
    }
    
    // MARK:- Custom methods
    func addShadowInView(vw:UIView) {
        vw.layer.masksToBounds = false
        let sColor = Constants.AppColor.COLOR_Shadow
        vw.layer.shadowColor = sColor.cgColor
        vw.layer.shadowOpacity = 0.3
        vw.layer.shadowOffset = CGSize.zero
        vw.layer.shadowRadius = 6
    }
    
}
