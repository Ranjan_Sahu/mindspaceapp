//
//  UserTabBarController.swift
//  MindSpace
//
//  Created by webwerks on 9/25/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class UserTabBarController: UITabBarController {
    
    let TABBAR_HEIGHT = 50.0
    var currentIndex = 0
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTabBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
 
        
    override var preferredStatusBarStyle : UIStatusBarStyle {
        
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
}

// MARK: - Custom Methods
extension UserTabBarController {
    private func callSOSNumber(phoneNumber: String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            if UIApplication.shared.canOpenURL(phoneCallURL) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            } else {
                Constants.showAlertWithMessage(message: "Device does not support calling.", presentingVC: self, okBlock: { (str) in })
            }
        }
    }
}

//MARK:- Custom Delegate
extension UserTabBarController {
    private func setupTabBar(){
        self.delegate = self
        self.selectedIndex = 0
        
        
        let tabBarItem1 = self.tabBar.items![0]
        let tabBarItem2 = self.tabBar.items![1]
        let tabBarItem3 = self.tabBar.items![2]
        let tabBarItem4 = self.tabBar.items![3]
        let tabBarItem5 = self.tabBar.items![4]
        
        let selectedFont:UIFont = UIFont.boldSystemFont(ofSize: 11)
        let unSelectedFont:UIFont = UIFont.systemFont(ofSize: 11)
        let selectedColor:UIColor = UIColor.white
        let unSelectedColor:UIColor = CommonHelper.hexStringToUIColor(hex: "B6D2DD")
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : unSelectedColor, NSAttributedStringKey.font:unSelectedFont], for: UIControlState.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : selectedColor, NSAttributedStringKey.font:selectedFont], for: UIControlState.selected)
        //UITabBar.appearance().backgroundColor = CommonHelper.hexStringToUIColor(hex: "1C6290")
//        let layerGradient = CAGradientLayer()
//        layerGradient.colors = [CommonHelper.hexStringToUIColor(hex: "1C669a").cgColor, CommonHelper.hexStringToUIColor(hex: "5fe8c0").cgColor]
//        layerGradient.startPoint = CGPoint(x: 0, y: 0)
//        layerGradient.endPoint = CGPoint(x: 1, y: 1)
//        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
//        self.tabBar.layer.addSublayer(layerGradient)
        self.tabBar.backgroundColor = CommonHelper.hexStringToUIColor(hex: "1c5ab0")
        
        tabBarItem1.title = "Home"
        tabBarItem2.title = "Social"
        tabBarItem3.title = "Announcements"
        tabBarItem4.title = "Offers"
        tabBarItem5.title = "My Cart"
        
        tabBarItem1.image = UIImage(named: "tab_icon_home_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem2.image = UIImage(named: "tab_icon_social_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem3.image = UIImage(named: "tab_icon_announcement_default")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem4.image = UIImage(named: "tab_icon_offers_dafault")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem5.image = UIImage(named: "cart_Unselected_SM")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        tabBarItem1.selectedImage = UIImage(named: "tab_icon_home")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem2.selectedImage = UIImage(named: "tab_icon_social")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem3.selectedImage = UIImage(named: "tab_icon_announcement")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem4.selectedImage = UIImage(named: "tab_icon_offers")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarItem5.selectedImage = UIImage(named: "cart_Selected_SM")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.view.layoutIfNeeded()
    }
}
//MARK:- TabBar Delegate
extension UserTabBarController : UITabBarControllerDelegate{
    // TabBar delegates
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        currentIndex = self.selectedIndex
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        // If tapped Announcement button, call SOS.
        // Disable for now.
        /*if let navigationController = viewController as? UINavigationController, let controller = navigationController.viewControllers.first, controller is RUAnnouncementsVC {
            callSOSNumber(phoneNumber: Constants.SOS_NUMBER)
            return false
        }*/
        return true
    }
}
