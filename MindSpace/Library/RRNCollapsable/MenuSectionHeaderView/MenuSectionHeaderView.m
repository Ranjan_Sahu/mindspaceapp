//
//  MenuSectionHeaderView.m
//  Example
//
//  Created by Robert Nash on 08/09/2015.
//  Copyright (c) 2015 Robert Nash. All rights reserved.
//

#import "MenuSectionHeaderView.h"

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

@interface MenuSectionHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation MenuSectionHeaderView {
    BOOL isRotating;
}

@synthesize interactionDelegate = _interactionDelegate;

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.interactionDelegate userTapped:self];
}

-(void)closeAnimated:(BOOL)animated {
    
//    self.imageView.image = [UIImage imageNamed:@"minimize"];

    if (animated && !isRotating) {
        
        isRotating = YES;
        
        [UIView animateWithDuration:0.3 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionCurveLinear animations:^{
            self.imageView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            isRotating = NO;
        }];
        
    } else {
        [self.layer removeAllAnimations];
        self.imageView.transform = CGAffineTransformIdentity;
        
        isRotating = NO;
    }
}

-(void)openAnimated:(BOOL)animated {
    
//    self.imageView.image = [UIImage imageNamed:@"expand"];

    if (animated && !isRotating) {
        
        isRotating = YES;
        
        [UIView animateWithDuration:0.3 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionCurveLinear animations:^{
            self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.0f));
        } completion:^(BOOL finished) {
            isRotating = NO;
        }];
        
    } else {
        [self.layer removeAllAnimations];
        self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.0f));
        isRotating = NO;
    }
}

@end
