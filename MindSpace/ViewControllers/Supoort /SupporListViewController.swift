//
//  SupporListViewController.swift
//  MindSpace
//
//  Created by webwerks on 12/18/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class SupportPageInfo {
    var pageIndex = 1
    var pageSize = 50
}

class SupporListViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    
    let supportCell = "SupportListTableViewCell"
    private var pageInfo = SupportPageInfo()
    var supportData = [SupportModel]()
    var listingData = [SupportDataListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Tickets Raised"
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        self.navigationController?.navigationBar.isHidden = false
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .highlighted)
        let rightBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addSupportDetailVC))
        rightBtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = rightBtn
        
        self.view.layoutIfNeeded()
        setUpTableView()
        getListingData()
        
    }
}

extension SupporListViewController {
    func setUpTableView() {
        listTableView.dataSource = self
        listTableView.delegate = self
     
        self.listTableView.register(UINib.init(nibName: supportCell, bundle: nil) , forCellReuseIdentifier: supportCell)
        listTableView.separatorStyle = .none
    }
    
    func getListingData() {
        
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)"],
                          "entity" : ["TenantId" : "1"],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.suppotList, objectType: SupportModel.self, isHud: true, controller: self, success: { [weak self] listingData in
            self?.supportData = [listingData]
            if self?.pageInfo.pageIndex == 1  {
                self?.listingData = listingData.DateList ?? []
            }else {
                if let listingData = listingData.DateList {
                    self?.listingData.append(contentsOf: listingData)
                }
            }
            self?.listTableView.reloadData()
        })
    }
}
extension SupporListViewController {
    @objc func addSupportDetailVC() {
        let vc = Storyboard.supportDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension SupporListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SupportListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: supportCell) as? SupportListTableViewCell
        let list = listingData[indexPath.row]
        cell.ticketTypeTextLabel.text = list.TicketType
        cell.subjectTestLabel.text = list.Subject
        cell.priorityTextLabel.text = list.Priority
        cell.statusLabel.text = list.Status
        cell.addedClockLabel.text = "Added \(list.CreatedOn ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Storyboard.viewTicketViewController
        targetVC.complaintTicketsId = listingData[indexPath.row].ComplaintTicketsId
       self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
