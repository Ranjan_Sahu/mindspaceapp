//
//  RUConciergeServicesVC.swift
//  MindSpace
//
//  Created by webwerks on 10/17/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUConciergeServicesVC: UIViewController {
    
    
    // MARK:- Constants
    let cellIdentifier = "ConciergeListCell"
    
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    var cServiceList : [ConceirgeServiceList]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        
        self.getConciergeServiceList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Concierge Services"
    }
}
extension RUConciergeServicesVC {
// MARK:- Custom methods
    private func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    // MARK:- Service Calls
    private func getConciergeServiceList() {
        let parameter = ["pageInfo" : ["PageIndex" : "1", "PageSize" : "20"],
                         "entity" : ["ComplexId" : "1"],
                         "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameter, url: WebserviceURLs.Service_GetConceirgeServiceList, objectType: ConceirgeServiceListData.self, isHud: true, controller: self, success: { [weak self] csdata in
            self?.cServiceList = csdata.DataList
            print(self?.cServiceList!)
            self?.tblView.reloadData()
            
        })
    }
}
    // MARK: - TableView datsource and delegates
extension RUConciergeServicesVC : UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let csList = self.cServiceList {
           return (csList.count)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ConciergeListCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ConciergeListCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ConciergeListCell
        }
        let service = self.cServiceList![indexPath.row]
        cell.LBL_title.text = service.Title?.capitalized
        cell.LBL_description.text = service.ConceirgeServiceTypeTitle?.capitalized
        if service.ImageURLs?.count != 0 {
            cell.serviceBannerImgView?.sd_setImage(with: URL.init(string: service.ImageURLs?[0] ?? ""), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: SDWebImageOptions.highPriority, completed: nil)
            cell.serviceBannerImgView.layer.cornerRadius = 4.0
        }
        cell.LBL_time.text = service.Time
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let targetVC = Constants.StoryBoard.ADVANCE.instantiateViewController(withIdentifier: "ConceirgeDetailsVC") as! ConceirgeDetailsVC
        targetVC.navTitle = cServiceList?[indexPath.row].Title
        targetVC.outletId = self.cServiceList?[indexPath.row].ConceirgeServiceId
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
}
