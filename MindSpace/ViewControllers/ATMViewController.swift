//
//  ATMViewController.swift
//  MindSpace
//
//  Created by webwerks on 2/8/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import UIKit

class ATMViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier  = "ATMTableViewCell"
    var atmList = [ATMlist]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "ATMs"
        self.setupTableView()
    }
    
    func setupTableView(){
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 180
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.separatorColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAtmList()
    }
    
    func getAtmList(){
        
        let parameters = ["pageInfo" : ["PageIndex" : 1,
                                        "PageSize" : 30],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.getATMList, objectType: GetATMLists.self, isHud: true, controller: self, success: { [weak self] response in
            if response.TotalRecords == 0{
                self?.view.makeToast("No data available", duration: 1.0, position: .center)
                return;
            }
            self?.atmList = response.DataList ?? []
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
       })
        
    }
}

extension ATMViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return atmList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ATMTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ATMTableViewCell
        let atmInfo = self.atmList[indexPath.row]
            cell.backgroundColor = .clear
            cell.separatorInset = .zero
        cell.bannerImageView.image = UIImage(named: "atm_38")
            cell.LBL_Name_ATM.text = atmInfo.AtmName
            cell.lblAtmDesc.text = atmInfo.ComplexName
            cell.LBL_Buiolding_Name.text = "\(atmInfo.BuildingName ?? ""), \(atmInfo.LocationName ?? "")"
        cell.lmgLocation.image = UIImage(named: "icon_greenLocation")
        return cell
    }
}

