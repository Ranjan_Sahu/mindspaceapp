//
//  GalleryModels.swift
//  MindSpace
//
//  Created by Webwerks on 10/29/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct  GalleryData : Codable {
    var Status : Bool?
    var Message : String?
    var DataList : [GalleryList]?
    var BannerList : [BannerListing]?
    var TotalRecords : Int?
    var Data : GalleryDetails?
}

struct  GalleryList : Codable {
    var AlbumId : Int?
    var AlbumName : String?
    var CreatedOn : String?
    var ComplexName : String?
    var LocationName : String?
    var ComplexId : Int?
    var BannerImage : String?
    var Date : String?
//    var ImageURLs : [String]?
}
struct Gallery : Codable {
    
    var Status : Bool?
    var Message : String?
}

struct GalleryDetails : Codable {
    var AlbumId : Int?
    var AlbumName : String?
    var ComplexName : String?
    var LocationName : String?
    var Description : String?
    var Date : String?
    var ComplexId : Int?
    var ImageUrls : [ImageList]?
}

struct ImageList: Codable {
    var ImageName : String?
    var thumbnail : String?
    var ImageTitle : String?
}
