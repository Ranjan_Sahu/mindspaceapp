//
//  RSVPData.swift
//  MindSpace
//
//  Created by Zeeshan  on 15/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

struct RSVPData : Codable {
    var Data : RSVP?
    var Messgae  :String?
    var Status : Bool?
}

struct RSVP : Codable {
    var RSVP : Int?
    var RSVPName : String?
    var AttendingCount : Int?
    var NotAttendingCount : Int?
    var MayBeCount : Int?
}
