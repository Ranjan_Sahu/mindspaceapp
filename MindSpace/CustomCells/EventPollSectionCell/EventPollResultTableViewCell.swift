//
//  EventPollResultTableViewCell.swift
//  MindSpace
//
//  Created by webwerks on 12/21/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class EventPollResultTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var resultForthSeasonView: UIView!
    @IBOutlet weak var resultFifthSeasonView: UIView!
    @IBOutlet weak var resultSummerView: UIView!
    @IBOutlet weak var resultWinterView: UIView!
    @IBOutlet weak var resultRainyView: UIView!
    @IBOutlet weak var pollTitleLabel: UILabel!
    @IBOutlet weak var pollQuestionLabel: UILabel!
    @IBOutlet weak var resultStackView: UIStackView!
    @IBOutlet weak var summerProgress: UIProgressView!
    @IBOutlet weak var summerCountLabel: UILabel!
    @IBOutlet weak var winterProgress: UIProgressView!
    @IBOutlet weak var winterCountLabel: UILabel!
    @IBOutlet weak var rainyProgress: UIProgressView!
    @IBOutlet weak var rainyCountLabel: UILabel!
    @IBOutlet weak var season1progress: UIProgressView!
    @IBOutlet weak var season1CountLabel: UILabel!
    @IBOutlet weak var season2Progress: UIProgressView!
    @IBOutlet weak var season2CountLabel: UILabel!
    @IBOutlet weak var season2TitleLabel: UILabel!
    @IBOutlet weak var season1TitleLabel: UILabel!
    @IBOutlet weak var summerTextLabel: UILabel!
    @IBOutlet weak var winterTextLabel: UILabel!
    @IBOutlet weak var rainyTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCellUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func setCellUI() {
        selectionStyle = .none
        bgView.layer.cornerRadius = 10
    }
}
