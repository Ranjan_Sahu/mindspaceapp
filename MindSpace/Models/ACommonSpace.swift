//
//  ACommonSpaceModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct  ACommonSpaceDetailsData : Codable {
    let Status : Bool?
    let Message : String?
    let Data : [ACommonSpaceDetails]?
}

struct  ACommonSpaceDetails : Codable {
    let BookingSpaceName : String?
    let BookingSpaceDesc : String?
    let BookingSpaceCapacity : String?
    let StartTime : String?
    let EndTime : String?
    let FloorId : Int?
    let FloorNumber : String?
    let BuildingId : Int?
    let BuildingName : String?
    let ComplexId : Int?
    let ComplexName : String?
    let BookingProcessDocument : String?
    let ImageURLs : [String]?
}
