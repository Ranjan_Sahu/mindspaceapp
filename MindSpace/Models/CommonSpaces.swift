//
//  CommonSpaces.swift
//  MindSpace
//
//  Created by webwerks on 30/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

// MARK :-  List
class  CommonSpacesListData : Codable {
    let Status : Bool?
    let Message : String?
    let DataList : [CommonSpacesList]?
    let TotalRecords : Int?
}

class  CommonSpacesList : ServicesListings {
    var BookingSpaceId : Int?
    var BookingSpaceName : String?
    var BookingSpaceCapacity : String?
    var BookingProcessDocument : String?
    var BookingSpaceDesc : String?
    
    private enum CodingKeys : String, CodingKey {
        case BookingSpaceId
        case BookingSpaceName
        case BookingSpaceCapacity
        case BookingProcessDocument
        case BookingSpaceDesc
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.BookingSpaceId = try container.decode(Int.self, forKey: .BookingSpaceId)
        self.BookingSpaceName = try container.decode(String.self, forKey: .BookingSpaceName)
        self.BookingSpaceCapacity = try container.decode(String.self, forKey: .BookingSpaceCapacity)
        self.BookingProcessDocument = try container.decode(String.self, forKey: .BookingProcessDocument)
        self.BookingSpaceDesc = try container.decode(String.self, forKey: .BookingSpaceDesc)

        try super.init(from: decoder)
    }
}

// MARK :-  Details
class  CommonSpacesDetailsData : Codable {
    var Status : Bool?
    var Message : String?
    var Data : CommonSpacesDetails?
} //CommonSpacesList

class  CommonSpacesDetails : Codable {

    var BookingSpaceName : String?
    var BookingSpaceCapacity : String?
    var BookingSpaceDesc : String?
    var Time : String?
    var FloorId : Int?
    var FloorNumber : String?
    var BuildingId : Int?
    var BuildingName : String?
    var ComplexId : Int?
    var ComplexName : String?
    var ContactDetails : String?
    var BookingProcessDocument : String?
    var ImageUrls : [String]?
//
//    private enum CodingKeys : String, CodingKey {
//        case FloorId
//        case FloorNumber
//        case BuildingId
//    }
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.FloorId = try container.decode(Int.self, forKey: .FloorId)
//        self.FloorNumber = try container.decode(String.self, forKey: .FloorNumber)
//        self.BuildingId = try container.decode(Int.self, forKey: .BuildingId)
//        try super.init(from: decoder)
//    }
}
