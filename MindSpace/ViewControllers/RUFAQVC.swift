//
//  RUFAQVC.swift
//  MindSpace
//
//  Created by webwerks on 10/8/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class RUFAQVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- Constants
    let cellIdentifier = "FAQCell"
    var selectedIndexPath : IndexPath?
    var lastSelectedIndexPath : IndexPath?
//    var titleStr : String
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Objects
    var faqList : [FAQListData]? = nil
    var dummyImgArray : [String]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tblview setup
        self.registerNibWithIdentifier(identifier: cellIdentifier)
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 40
        self.getFAQList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "FAQs"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func getFAQList() {
        let parameters = ["deviceInfo" : CommonHelper.getDeviceInfo()]
        Webservice.callPostWebservice(parameters: parameters,  url: WebserviceURLs.Service_GetFAQList, objectType: FAQData.self, isHud: true, controller: self, success: { [weak self] faqData in
            self?.faqList = faqData.DataList
            self?.tblView.reloadData()
        })
    }
    
    
    // MARK:- Custom methods
    func registerNibWithIdentifier(identifier: String) -> Void {
        self.tblView.register(UINib.init(nibName: identifier, bundle: nil) , forCellReuseIdentifier: identifier)
    }
    
    // MARK:- Collapsable Tableview implementations
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let fList = self.faqList {
            return (fList.count)
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: FAQCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FAQCell
        if cell == nil {
            tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FAQCell
        }
        cell.selectionStyle = .none
        
        cell.quesLabel.text = self.faqList![indexPath.row].QuestionText
        cell.ansLabel.text = self.faqList![indexPath.row].AnswerText
        
        if let indPath = selectedIndexPath, indPath.row == indexPath.row{
            cell.ansLabel.isHidden = false
        }else {
            cell.ansLabel.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexPath == indexPath {
            let lastSelectedCell = tableView.cellForRow(at: indexPath) as! FAQCell
            lastSelectedCell.ansLabel.isHidden = true
            selectedIndexPath = nil
            lastSelectedIndexPath = nil
            tblView.beginUpdates()
            tblView.endUpdates()
            return
        }
        
        lastSelectedIndexPath = selectedIndexPath
        if let indPath = lastSelectedIndexPath {
            let lastSelectedCell = tableView.cellForRow(at: indPath) as! FAQCell
            lastSelectedCell.ansLabel.isHidden = true
        }
        selectedIndexPath = indexPath
        let cell = tableView.cellForRow(at: indexPath) as! FAQCell
        cell.ansLabel.isHidden = false
        tblView.beginUpdates()
        tblView.endUpdates()
        
    }
    
}
