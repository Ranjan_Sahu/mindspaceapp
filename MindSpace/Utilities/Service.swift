
//
//  Service.swift
//  MindSpace
//
//  Created by webwerks on 9/24/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

let baseUrl:String = "http://mindspace.aspnetdevelopment.in/api/"
//let baseUrl:String = "http://10.0.101.59/api/"

let authValue = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1"

// for delegate
protocol Serviec_Delegate : NSObjectProtocol {
    func Service_Success(_responseString : String) -> Void
    func Service_Error(_error : AnyObject) -> Void
    func No_Response_Function()
}

class Service: NSObject {
    // Variables
    weak var delegate : Serviec_Delegate?
    
    // Methods using delegate
    func requestingURLString(URLString : String, withServiceName ServiceName : String, withParameters Param :  [String: AnyObject]) -> Void
    {
        let reqParameters = Param
        let jsonError : NSError
        var jsonData : Data?
        let jsonString : String
        
        // Create JSON of req param
        do{
            jsonData = try JSONSerialization.data(withJSONObject: reqParameters, options: JSONSerialization.WritingOptions.prettyPrinted) as Data
            print(jsonData!)
        }catch let error as NSError{
            jsonError = error
            print(jsonError.debugDescription)
        }
        
        if (jsonData != nil){
            jsonString = String.init(data: jsonData! as Data, encoding: String.Encoding.utf8)!
            print(jsonString)
        }
        
        // get req url
        let finalUrl : String =  URLString.appending(ServiceName)
        var postData : Data?
        
        do{
            postData = try JSONSerialization.data(withJSONObject: reqParameters, options: JSONSerialization.WritingOptions(rawValue: 0))
            print(postData!)
        }catch let error as NSError{
            print(error.debugDescription)
        }
        
        var headers : [String : String] = [:]
        headers["content-type"] = "application/json"
        
        var request : URLRequest = URLRequest.init(url: URL.init(string: finalUrl)! , cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData!
        
        let session : URLSession = URLSession.shared
        let dataTask : URLSessionDataTask = session.dataTask(with: request, completionHandler : { (data : Data?, response : URLResponse?, error : Error?) in
            
            let  errorData = error as NSError?
            if errorData != nil{
                if errorData?.code == -1009{
                    DispatchQueue.main.sync {
                        self.delegate?.No_Response_Function()
                    }
                }else{
                    DispatchQueue.main.sync {
                        self.delegate?.Service_Error(_error: errorData as AnyObject)
                    }
                }
            }
            else{
                DispatchQueue.main.sync{
                    let serverResponse : String = String.init(data: data!, encoding: String.Encoding.utf8)!
                    self.delegate?.Service_Success(_responseString: serverResponse)
                }
            }
        })
        
        dataTask.resume()
    }
    
    //  GET & POST Methods using block
    // GET Method
    func callGetServiceWithName(serviceName:String, paramDict:Dictionary<String, Any>, showHud:Bool, hudView:UIView, successBlock:@escaping (_ response:Any,_ responseStr: Dictionary<String, Any>)->Void, errorBlock:@escaping (_ error:Any)->Void)  {
        
        var urlString = String()
        urlString.append(baseUrl)
        urlString.append(serviceName)
        
        // create request url
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let serverUrl: URL = URL(string: (encodedUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!)!
        
        // set headers
        var headers : [String : String] = [:]
        headers["content-type"] = "application/json"
        
        // create request
        var request = URLRequest(url: serverUrl)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let dataTask = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let httpResponse = response as? HTTPURLResponse {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                    DispatchQueue.main.async {
                        successBlock(response!,json)
                    }
                } catch {
                    DispatchQueue.main.async {
                        errorBlock(error.localizedDescription)
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    errorBlock("")
                }
            }
        }
        
        dataTask.resume()
    }
    
    // POST Method
    func callPostServiceWithName(serviceName:String, paramDict:Dictionary<String, Any>, showHud:Bool, hudView: UIView, successBlock:@escaping (_ response:Any, _ responseStr: Data )->Void, errorBlock:@escaping (_ error:Any)->Void)  -> Void
    {
        var urlString = String()
        urlString.append(baseUrl)
        urlString.append(serviceName)
        
        // create request url
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let serverUrl: URL = URL(string: (encodedUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!)!
        
        // set headers
        var headers : [String : String] = [:]
        headers["content-type"] = "application/json"
        headers["AuthorizeToken"] = authValue
        
        var jsonData : Data?
        let jsonString : String
        let jsonError : NSError
        var postData : Data?
        
        // Create JSON Data from request param
        do{
            jsonData = try JSONSerialization.data(withJSONObject: paramDict, options: JSONSerialization.WritingOptions.prettyPrinted) as Data
            print(jsonData!)
        }catch let error as NSError{
            jsonError = error
            print(jsonError.debugDescription)
        }
        // Create JSON String from json data
        if (jsonData != nil){
            jsonString = String.init(data: jsonData! as Data, encoding: String.Encoding.utf8)!
            print(jsonString)
        }
        
        // Create Post Data from request param
        do{
            postData = try JSONSerialization.data(withJSONObject: paramDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            print(postData!)
        }catch let error as NSError{
            print(error.debugDescription)
        }
        
        // create request
        var request : URLRequest = URLRequest(url: serverUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
         request.httpBody = postData!
        
        let postDataTask = URLSession.shared.dataTask(with: request) { data, response, error in
            
            //data, response, error in
            if data != nil && error == nil{
                
                //let res = String(data: data!, encoding: .utf8)
                //print(res)
                //let dict = self.convertToDictionary(text: res!)
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if (httpResponse.statusCode == 200)
                    {
                        DispatchQueue.main.async {
                            successBlock (response!,data!)
                        }
                    }
                    else
                    {
                        if (error?.localizedDescription) != nil{
                            errorBlock((error?.localizedDescription)! as String)
                        }else{
                            errorBlock("")
                        }
                    }
                }
                else
                {
                    errorBlock((error?.localizedDescription)! as String)
                }
            }
            else{
                if let httpResponse = error as? HTTPURLResponse {
                    print("error \(httpResponse.statusCode)")
                }
                errorBlock((error?.localizedDescription)! as String)
            }
        }
        
        postDataTask.resume()
        
    }
    
    // custom methods
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return ["":""]
    }
    
}
