//
//  RoundedViews.swift
//  MindSpace
//
//  Created by webwerks on 05/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import Foundation

@IBDesignable public class RoundView: UIView {
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 1.0
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 4.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        
    }
}
@IBDesignable public class RoundTableView: UITableView {
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 1.0
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 4.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
    }
}
@IBDesignable public class RoundImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 4.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
            clipsToBounds = true
            
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
    }
}
@IBDesignable public class RoundCornerButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 4.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
    }
}
@IBDesignable public class RoundButton: UIButton {
    @IBInspectable var borderColor: UIColor = UIColor.white{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.height / 2.0
        clipsToBounds = true
    }
}

@IBDesignable public class VisitorRoundButton: UIButton {
    @IBInspectable var borderColor: UIColor = UIColor.white{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
        clipsToBounds = true
    }
}
@IBDesignable public class ShadowView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 4.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = false
        layer.masksToBounds = false
        let sColor = Constants.AppColor.COLOR_Shadow
        layer.shadowColor = sColor.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 8
        
    }
}

@IBDesignable  open  class CustomTextField: UITextField {
    @IBInspectable var rightImage: UIImage? = nil{
        didSet {
            if  let image = rightImage {
                let rightImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: frame.height))
                rightImageView.contentMode = .center
                rightImageView.image = image
                rightView = rightImageView
                rightViewMode = .always
            }
        }
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
}
@IBDesignable public class RoundTextField: CustomTextField {
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: frame.height/2.0, height: frame.height))
        leftView = leftPaddingView
        leftViewMode = .always
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
   //     layer.cornerRadius = frame.size.height / 2.0
          layer.cornerRadius = 10.0
        clipsToBounds = true
        
        
    }
}

@IBDesignable public class CustomProgressView: UIProgressView {
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.height / 2.0
        clipsToBounds = true
    }
}


