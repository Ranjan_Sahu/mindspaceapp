//
//  ProductModels.swift
//  MindSpace
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Neo. All rights reserved.
//

import Foundation

struct ProductBuyListData : Codable{
    var Status : Bool?
    var Message : String?
    var DateList : [ProductBuyList]?
    var TotalRecords : Int?
}
struct ProductBuyList :Codable{
    var ClassifiedId : Int
    var Category : String
    var Title : String
    var Price : Int
    var Description : String
    var ImageName : String
}

struct ProductDetailsData : Codable{
    var Status : Bool?
    var Message : String?
    var Data : ProductDetails?
}

struct ProductDetails :Codable{
    var ClassifiedId : Int?
    var Title : String?
    var Price : Int?
    var Description : String?
    var CreatedOn : String?
    var ExpiryDate : String?
    var Author : String?
    var Interested : String?
    var PostedOn : String?
    var ImageUrls : [ImageList]?
}
