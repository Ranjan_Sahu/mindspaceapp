//
//  ViewTicketViewController.swift
//  
//
//  Created by webwerks on 1/14/19.
//

import UIKit
import Lightbox

class ViewTicketViewController: UIViewController {
    let cellIdentifier = "SupportDetailTableViewCell"
    var ticketDataModel : GetTicketData?
    var ticketDetailsModel : GetTicketDetails?
    var complaintTicketsId : Int?
    var menuCardURLArray : [LightboxImage] = []
    var imageList: [LightboxImage] = []

    @IBOutlet weak var tableView: RoundTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil) , forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = 750
        tableView.keyboardDismissMode = .interactive
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationItem.title = "Ticket Details"
        callWebService()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.title = "Ticket Details"
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
        }
}

extension ViewTicketViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SupportDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SupportDetailTableViewCell
        if ticketDetailsModel != nil {
            cell.LBL_CategoryName.text =  ticketDetailsModel?.ComplaintTypeName
            cell.LBL_tickcetSubject.text = ticketDetailsModel?.Subject
            cell.LBL_priority.text = ticketDetailsModel?.Priority
            if ticketDetailsModel?.Description != ""{
                cell.descriptionTextView.text = ticketDetailsModel?.Description
            } else {
                cell.descriptionTextView.text = "-"
            }
            if ticketDetailsModel?.UserComments != ""{
                cell.commentsTextView.text = ticketDetailsModel?.UserComments
            } else {
                cell.commentsTextView.text = "-"
            }
            cell.LBL_status.text = ticketDetailsModel?.StatusType
            cell.viewAttcehmentButton.addTarget(self, action:#selector(showMenuCard), for: .touchUpInside)
            
        }
        return cell

    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension ViewTicketViewController{
    func callWebService(){
        var cti:Int? = 0
        cti = complaintTicketsId
        print("************")
        print("\(cti ?? 0)")
        let complaintId = "\(cti ?? 0)"
         let parameters = ["pageInfo" : ["PageIndex" : "1",
                                        "PageSize" : "20"],
                          "entity" : ["ComplaintTicketsId" :complaintId],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()] as [String:Any]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetComplaintTicket, objectType: GetTicketData.self, isHud: true, controller: self, success:
            { [weak self] ticketData in
                self?.ticketDetailsModel = ticketData.Data
                print(self?.ticketDetailsModel?.ComplaintTicketNo!)
                print("*************")
                
                if (self?.ticketDetailsModel) != nil {
                    self?.tableView.reloadData()
                }
            }
        )
    }
   
}
extension ViewTicketViewController {
    @objc func showMenuCard() {
        if let menuCardURL = ticketDetailsModel?.Attachment {
            let extensionTypeString = menuCardURL.components(separatedBy: ".")
            let extensionType = extensionTypeString[extensionTypeString.count - 1]
            if extensionType == "jpg" || extensionType == "png" || extensionType == "jpeg" {
                let lightBoxImageObject = LightboxImage(imageURL: URL.init(string: menuCardURL)!)
                menuCardURLArray.append(lightBoxImageObject)
                let controller = LightboxController(images: menuCardURLArray, startIndex: 0)
                controller.pageDelegate = self
                controller.dismissalDelegate = self
                controller.dynamicBackground = true
                present(controller, animated: true, completion: nil)
            }
        }
    }
}

extension ViewTicketViewController : LightboxControllerPageDelegate , LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
    }
}
