//
//  ContactViewController.swift
//  MindSpace
//
//  Created by Zeeshan  on 09/11/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import FRHyperLabel


class ContactViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: FRHyperLabel!
    var email : String? = "cssdhjbjhfd@dknkf.com"
    var contactUs : String?
    var titleStr : String?
    var searchElementArr = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //        setupScreen()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.lblDescription.text = email
        self.lblTitle.text = ""
        self.title = self.titleStr ?? ""
        onClickPhnNumber()
    }
}
extension ContactViewController {
    private func setupScreen(){
        setupDetailLabel()
    }
    private func setupDetailLabel(){
        let email = NSAttributedString(string: "Email us at ", attributes: nil)
        let emailID = NSAttributedString(string: self.email ?? "", attributes: [NSAttributedStringKey.font : "Helvetica-Bold"])
        let str = NSMutableAttributedString()
        str.append(email)
        str.append(emailID)
        //self.lblDescription.attributedText = str
        //        let emailID = NSAttributedString()
        
    }
    
    func onClickPhnNumber() {
        
        let finalStringsArray = getModifiedString()
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                          NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        
        lblDescription?.attributedText = NSAttributedString(string: email!, attributes: attributes)
        let handlr = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            if (substring?.contains("@"))! {
                //Open email box
                let mailUrl = URL(string: "mailto:\(substring ?? "")")
                if mailUrl != nil {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(mailUrl!)
                    } else {
                        UIApplication.shared.openURL(mailUrl!)
                    }
                }
            } else {
                if let url = URL(string: "tel://\(substring ?? "")"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }else{
                    print("cant open url")
                }
            }
        }
        
        lblDescription?.setLinksForSubstrings(finalStringsArray, withLinkHandler: handlr)
    }
}
extension ContactViewController {
    func getModifiedString() -> [String] {
        
        if titleStr == "Visitor Assistance" {
            searchForEmails(searchStr: email!)
            searchForPhoneNumbers(searchStr: email!)
            
        } else {
            searchForEmails(searchStr: email!)
            searchForPhoneNumbers(searchStr: email!)
        }
        print(searchElementArr)
        return searchElementArr
    }
    
    func searchForPhoneNumbers(searchStr:String){
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: email!, range: NSRange(email!.startIndex..., in: email!))
            for match in matches{
                if match.resultType == .phoneNumber, let number = match.phoneNumber {
                    print(number)
                    searchElementArr.append(number)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func searchForEmails(searchStr:String){
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let nsText = email as! NSString
        do {
            let regExp = try NSRegularExpression(pattern: emailRegex, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, email!.characters.count)
            let matches = regExp.matches(in: email!, options: .reportProgress, range: range)
            
            for match in matches {
                let matchRange = match.range
                searchElementArr.append(nsText.substring(with: matchRange))
            }
            
        } catch _ {
        }
    }
}
    
