//
//  PollAnsCell.swift
//  MindSpace
//
//  Created by webwerks on 16/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class PollAnsCell: UITableViewCell {
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var ansLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
