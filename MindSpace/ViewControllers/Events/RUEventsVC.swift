//
//  RUEventsVC.swift
//  MindSpace
//
//  Created by webwerks on 9/28/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit
import Toast_Swift

class PageInfo {
    var pageIndex = 1
    var pageSize = 20
}
class RUEventsVC: UIViewController {
    // MARK:- Constants
    let eventCell = "EventListTableViewCell"
    let pollCell  = "PollTableViewCell"
    let cellIdentifer = "EventListTableViewCell"
    
    let eventPollCell = "EventPollTableViewCell"
    let eventPollResultCell = "EventPollResultTableViewCell"
    
    var monthArray = ["All","January","February","March","April","May","June","July","August","    September","October","November","December"]
    // MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var pollTableView: UITableView!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var eventBannerView: UIView!

    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var janBtn: UIButton!
    @IBOutlet weak var calenderBtn: UIButton!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var calenderTextField: RoundTextField!
    @IBOutlet weak var toolBar: UIToolbar!
    
    let refreshControl = UIRefreshControl()
    let pollRefreshControl = UIRefreshControl()
    private var bannerView: UIView!
    // MARK:- Objects
    var catrgoryArray = [String]()
    var slides:[HomeScreenBannerView] = [];
    var bannerTimer:Timer? = nil
    var eventList : [EventList] = []
    private var eventData : EventData?
    private var pageInfo = PageInfo()
    private var pollData: PollData?
    var pollDataList = [PollList]()
    var setPollList = PollList()
    var selectedCellId:Int = 0
    var selectedMonth = "All"
    var totalEvents:Int = 0
    
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var testCalendar = Calendar(identifier: Calendar.Identifier.iso8601)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentDate = Date()
        janBtn.setTitle("Poll", for: .normal)
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.view.layoutIfNeeded()
        self.pollTableView.register(UINib.init(nibName: eventPollCell, bundle: nil) , forCellReuseIdentifier: eventPollCell)
        
        self.pollTableView.register(UINib.init(nibName: eventPollResultCell, bundle: nil) , forCellReuseIdentifier: eventPollResultCell)
        
        self.tblView.register(UINib.init(nibName: eventCell, bundle: nil) , forCellReuseIdentifier: eventCell)
        self.addRefreshControlToTableView()
        self.getEventList()
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 266
        pollTableView.rowHeight = UITableViewAutomaticDimension
        pollTableView.estimatedRowHeight = 570
        self.addTapToScroll()
        pollTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Events"
        
        let customFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        customFooterView.backgroundColor = .clear
        self.tblView.tableFooterView = customFooterView
        bannerView = eventBannerView
        let slideTimeInterveal = UserDefaults.standard.value(forKey: "BannerSlide_Time") as! Int
        if((self.bannerTimer) != nil){
            self.bannerTimer?.invalidate()
            self.bannerTimer = nil
        }
        self.bannerTimer = Timer.scheduledTimer(timeInterval: Double(slideTimeInterveal) , target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    @IBAction func calendearClicked(_ sender: Any) {
        self.buttonSelection(allBtn: false, janBtn: false, febBtn: true, calenderbtn: false)
    }
    private func addTapToScroll(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        self.bannerScrollView.addGestureRecognizer(tap)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.bannerTimer?.invalidate()
    }
    @objc func scrollViewTapped(){
        let currentBanner = self.pgControl.currentPage
        let bannerID = self.eventData?.BannerList?[currentBanner].Id
        let vc = Storyboard.eventDetailsVC
        vc.id = bannerID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension RUEventsVC {
    
    // MARK:- Service Calls
    private func getEventList() {
        let parameters = ["pageInfo" : ["PageIndex" : "\(pageInfo.pageIndex)",
            "PageSize" : "\(pageInfo.pageSize)","Month" : selectedMonth],
                          "entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfo()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetEventList, objectType: EventData.self, isHud: true, controller: self, success: { [weak self] eventData in
            UIApplication.shared.endIgnoringInteractionEvents()
            self?.eventData = eventData
            DispatchQueue.main.async {
                self?.hideBanner()
            }
            
            guard eventData.DataList != nil else {

                if (self?.eventList.isEmpty)! {
                    self?.eventList.removeAll()
                }
                if (self?.pageInfo.pageIndex)! > 1 {
                    self?.pageInfo.pageIndex = (self?.pageInfo.pageIndex)! - 1
                }
                
                DispatchQueue.main.async {
                    self?.tblView.reloadData()
                }
                
                self?.view.makeToast("No data available", duration: 1.0, position: .center)
                return
            }
            
            if self?.pageInfo.pageIndex == 1  {
                self?.eventList = eventData.DataList ?? []
                if(self?.eventList.count == 0){
                    self?.view.makeToast("No data available", duration: 1.0, position: .center)
                    return;
                }
                self!.totalEvents = self?.eventData?.TotalRecords ?? 0
            }else {
                if let eventList = eventData.DataList {
                    self?.eventList.append(contentsOf: eventList)
                }
            }
            if (self!.eventData?.BannerList?.count)! > 0{
                DispatchQueue.main.async {
                 self?.showBanner()
                 self?.setupScroll()
                }
            }else{
                DispatchQueue.main.async {
                    self?.hideBanner()
                }
            }
            
            DispatchQueue.main.async {
                self?.tblView.reloadData()
            }
        })
    }
    
    func hideBanner(){
        self.tblView.tableHeaderView = UIView()
    }
    
    func showBanner(){
        self.tblView.tableHeaderView = bannerView
        self.bannerScrollView.isHidden = false
        self.pgControl.isHidden = false
    }
    
    //MARK:- Class Methods
    private func buttonSelection(allBtn : Bool, janBtn : Bool, febBtn: Bool, calenderbtn  : Bool) {
        self.allBtn.isSelected = allBtn
        self.janBtn.isSelected = janBtn
        setTableViewOnClick()
    }
    private func createSlides() -> [HomeScreenBannerView] {
        var bannerVwArray = [HomeScreenBannerView]()
        if let bannerImgArray = self.eventData?.BannerList?.map( { $0.BannerImage ?? "" } ) {
            
            for imgStr in bannerImgArray {
                let slide:HomeScreenBannerView = Bundle.main.loadNibNamed("HomeScreenBannerView", owner: self, options: nil)?.first as! HomeScreenBannerView
                slide.bannerImgView.sd_setImage(with: URL.init(string: imgStr), placeholderImage: #imageLiteral(resourceName: "bannerPlaceholder"), options: .highPriority, completed: nil)
                bannerVwArray.append(slide)
            }
            return bannerVwArray
        }
        return []
    }
    
    private func setupSlideScrollView(slides : [HomeScreenBannerView]) {
        self.bannerScrollView.contentSize = CGSize(width: self.bannerScrollView.frame.width * CGFloat(slides.count), height: self.bannerScrollView.frame.height)
        self.bannerScrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.bannerScrollView.frame.width * CGFloat(i), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
            self.bannerScrollView.addSubview(slides[i])
        }
    }
    private func setupScroll(){
        self.slides = self.createSlides()
        self.setupSlideScrollView(slides: self.slides)
        self.pgControl.numberOfPages = self.slides.count
        self.pgControl.currentPage = 0
        self.bannerScrollView.delegate = self
    }
    private func setDate() {
        let month = testCalendar.dateComponents([.month], from: currentDate).month!
        let weekday = testCalendar.component(.weekday, from: currentDate)
        _ = DateFormatter().monthSymbols[(month-1) % 12] //
        _ = DateFormatter().shortWeekdaySymbols[weekday-1]
        _ = testCalendar.component(.day, from: currentDate)
        //        dateLabel.text = "\(week), " + monthName + " " + String(day)
    }
    @objc func moveToNextPage() {
        let pageWidth:CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        if  ((contentOffset + pageWidth) == maxWidth){
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.bannerScrollView.frame.height), animated: true)
    }
    
    @objc func handleRefresh()  {
        UIApplication.shared.beginIgnoringInteractionEvents()
        pageInfo.pageIndex = 1
        self.eventList.removeAll()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.getEventList()
        }
        
    }
    @objc func handlePollRefresh()  {
        UIApplication.shared.beginIgnoringInteractionEvents()
        pageInfo.pageIndex = 1
        self.pollDataList.removeAll()
        pollRefreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.getPollList()
        }
    }
}
extension RUEventsVC {
    func addRefreshControlToTableView() {
        self.tblView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.gray;
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        
        self.pollTableView.alwaysBounceVertical = true
        pollRefreshControl.tintColor = UIColor.gray;
        pollRefreshControl.addTarget(self, action: #selector(handlePollRefresh), for: .valueChanged)
        self.pollTableView.addSubview(pollRefreshControl)
    }
}

// MARK: - Button Action
extension RUEventsVC {
    @IBAction func allBtnAction(_ sender: Any) {
        self.calenderBtn.alpha = 1.0
        self.calenderBtn.isUserInteractionEnabled = true
        self.calenderTextField.isUserInteractionEnabled = true
        self.buttonSelection(allBtn: true, janBtn: false, febBtn: false, calenderbtn: false)
    }
    @IBAction func janBtnAction(_ sender: Any) {
        self.calenderBtn.alpha = 0.3
        self.calenderBtn.isUserInteractionEnabled = false
        self.calenderTextField.isUserInteractionEnabled = false
        self.buttonSelection(allBtn: false, janBtn: true, febBtn: false, calenderbtn: false)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let selRow = self.pickerView.selectedRow(inComponent: 0)
        selectedMonth = self.monthArray[selRow]
        self.handleRefresh()
    }
    
}

extension RUEventsVC : UITableViewDataSource, UITableViewDelegate{
    
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allBtn.isSelected {
            return eventList.count
        } else {
            return pollDataList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if allBtn.isSelected {
            var cell: EventListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: eventCell) as? EventListTableViewCell
            if cell == nil {
                tableView.register(UINib(nibName: cellIdentifer, bundle: nil), forCellReuseIdentifier: cellIdentifer)
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer) as? EventListTableViewCell
            }
            let event = self.eventList[indexPath.row]
            cell.LBL_eventTitle.text = event.EventNewsTitle
            cell.LBL_eventLocation.text = event.EventLocation
            if event.EventNewsType != 2 {
                cell.LBL_eventTime.isHidden = false
                cell.icon_time.isHidden = false
                cell.LBL_eventTime.text = event.EventTime
            } else {
                cell.LBL_eventTime.isHidden = true
                cell.icon_time.isHidden = true
            }
            // setup Date
            let date = event.FromDate!.getDateArray()
            cell.LBL_month.text = date[1]
            cell.LBL_date.text = date[0]
            if (event.BannerImage) != nil {
                cell.eventBannerImgView.sd_setImage(with: URL.init(string : event.BannerImage ?? ""), placeholderImage: UIImage(named: "bannerPlaceholder"))
            }
            return cell
        }
        else {
            
            let pollDetail = pollDataList[indexPath.row]
            if !pollDetail.AnswerGiven! {
                let cell: AnnouncementPollCell! = tableView.dequeueReusableCell(withIdentifier: "AnnouncementPollCell") as? AnnouncementPollCell
                cell.topicLabel.text = pollDetail.PollQuestion
                cell.pollCategory.text = pollDetail.PollTitle
                cell.cellNumber = indexPath.row
                let answerLists = pollDetail.AnswerList
                cell.dataSource = answerLists!
                cell.pollDelegate = self
                cell.tablHeightConstraint.constant = CGFloat(45 * cell.dataSource.count)
                cell.selectionStyle = .none
                return cell
            }
            else{
                
                let cell: AnnouncementResultCell! = tableView.dequeueReusableCell(withIdentifier: "AnnouncementResultCell") as? AnnouncementResultCell
                cell.selectedAnswerID = pollDetail.PollAnswerId
                cell.topicLabel.text = pollDetail.PollQuestion
                cell.pollCategory.text = pollDetail.PollTitle
                let answerLists = pollDetail.AnswerList
                cell.dataSource = answerLists!
                cell.tablHeightConstraint.constant = CGFloat(63 * cell.dataSource.count)
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if allBtn.isSelected {
            let targetVC = Storyboard.eventDetailsVC
            targetVC.id = eventList[indexPath.row].EventNewsId ?? 0
            self.navigationController?.pushViewController(targetVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK:- ScrollView Delegate
extension RUEventsVC : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == self.bannerScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            self.pgControl.currentPage = Int(pageIndex)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            if(totalEvents == self.eventList.count){
                return;
            }
            let endScrolling = Int(scrollView.contentOffset.y + scrollView.frame.size.height)
            if (endScrolling >= Int(scrollView.contentSize.height)){
                pageInfo.pageIndex += 1
                getEventList()
            }
        }
    }
}

extension RUEventsVC: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return monthArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return monthArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selRow = self.pickerView.selectedRow(inComponent: 0)
        selectedMonth = self.monthArray[selRow]
    }
}

extension RUEventsVC: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView?.autoresizingMask = .flexibleHeight
        textField.inputView = pickerView
        pickerView?.reloadAllComponents()
        textField.inputAccessoryView = toolBar
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
extension RUEventsVC {
    func setTableViewOnClick() {
        if allBtn.isSelected {
            pollTableView.isHidden = true
            tblView.isHidden = false
            tblView.contentOffset = CGPoint(x: 0, y: 0)
            getEventList()
        } else if janBtn.isSelected {
            pollTableView.separatorStyle  = .none
            pollTableView.isHidden = false
            pollTableView.contentOffset = CGPoint(x: 0, y: 0)
            tblView.isHidden = true
            getPollList()
        }
    }
    func getPollList() {
        let parameters = ["entity" : ["ComplexId" : CommonHelper.getComplexID()],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetPollList, objectType: PollData.self, isHud: true, controller: self, success: { [weak self] pollData in
            UIApplication.shared.endIgnoringInteractionEvents()
            self?.pollData = pollData
            if self?.pageInfo.pageIndex == 1  {
                self?.pollDataList = pollData.DataList ?? []
                if(self?.pollDataList.count == 0){
                    DispatchQueue.main.async{
                    self?.view.makeToast("No data available", duration: 1.0, position: .center)
                    }
                }
            }else {
                if let pollList = pollData.DataList {
                    self?.pollDataList.append(contentsOf: pollList)
                }
            }
            //self?.setupScroll()
            self?.pollTableView.reloadData()
        })
    }
    
    @objc func onClickRadioButton(sender: UIButton) {
        guard let cell = sender.superview?.superview?.superview?.superview?.superview as? EventPollTableViewCell else {
            return
        }
        let indexPath = pollTableView.indexPath(for: cell)
        let polldetails = pollDataList[indexPath!.row].AnswerList
        let pollAnsData = polldetails![sender.tag]
        let pollAnsId = pollAnsData.PollAnswerId
        let pollAnsStr = "\(pollAnsId)"
        getSetPollData(ansID: pollAnsStr)
    }
    
    func getSetPollData(ansID: String) {
        
        let parameters = ["entity" : ["UserId" : CommonHelper.getUserID(),"PollAnswerId": ansID],
                          "deviceInfo" : CommonHelper.getDeviceInfoWithUser()]
        
        Webservice.callPostWebservice(parameters: parameters, url: WebserviceURLs.Service_GetSetPollList, objectType: SetPoll.self, isHud: true, controller: self, success: { [weak self] setpollData in
            
            guard setpollData.Data != nil else{
                return
            }
            self?.setPollList = setpollData.Data!
            self?.pollDataList[self!.selectedCellId] = (self?.setPollList)!
            let indexPath = IndexPath(item: (self?.selectedCellId)!, section: 0)
            DispatchQueue.main.async {
                self?.pollTableView.reloadRows(at: [indexPath], with: .fade)
            }
            print(self!.setPollList)
            
        })
    }
    
}

extension RUEventsVC:PollQuesDelegates{
    func didSelectAnswer(selectedAns: PollAnswerList,selectedCellNo:Int) {
        print(selectedAns)
        selectedCellId = selectedCellNo
        let selectedAnsId = "\(selectedAns.PollAnswerId)"
        getSetPollData(ansID: selectedAnsId)
    }
    
}


