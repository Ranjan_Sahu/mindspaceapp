//
//  PollDetailVC.swift
//  MindSpace
//
//  Created by webwerks on 16/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class PollDetailVC: UIViewController {
    let cellIdentifier = "PollAnsCell"
    var optionArray : [String] = []
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var queLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var currentSelectedIndexPath : IndexPath?
    @IBOutlet weak var tablHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Poll Detail"
        optionArray = ["Kavita", "Aishwarya","Zeeshan","Ranjan"]
        tablHeightConstraint.constant = CGFloat(35 * optionArray.count)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension PollDetailVC : UITableViewDelegate,UITableViewDataSource {
    // MARK: - TableView datsource and delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PollAnsCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PollAnsCell
        cell.ansLabel.text = "Option \(indexPath.row + 1)"
        
        cell.selectionStyle = .none
        cell.radioBtn.isSelected = false
        
        if let indPath = currentSelectedIndexPath, indPath.row == indexPath.row{
            cell.radioBtn.isSelected = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentSelectedIndexPath == indexPath {
            return
        }
        //        lastSelectedIndexPath = currentSelectedIndexPath
        if let indPath = currentSelectedIndexPath {
            let lastSelectedCell = tableView.cellForRow(at: indPath) as! PollAnsCell
            lastSelectedCell.radioBtn.isSelected = false
            tableView.beginUpdates()
            tableView.endUpdates()
        }
        currentSelectedIndexPath = indexPath
        let cell = tableView.cellForRow(at: indexPath) as! PollAnsCell
        cell.radioBtn.isSelected = true
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    
}
