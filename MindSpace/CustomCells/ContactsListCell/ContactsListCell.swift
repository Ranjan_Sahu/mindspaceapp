//
//  ContactsListCell.swift
//  MindSpace
//
//  Created by webwerks on 10/10/18.
//  Copyright © 2018 Neo. All rights reserved.
//

import UIKit

class ContactsListCell: UITableViewCell {

    @IBOutlet weak var LBL_ContactNo: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var seperatorLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
